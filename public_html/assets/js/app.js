
var Qms = null; //'summernote',
(function() {
    Qms = angular.module('qms', [
        'ui.router', 'ngResource', 'angularFileUpload', 'ui.bootstrap',"ngTouch", "angucomplete-alt","ngTableToCsv", 
        'tld.csvDownload',"ui.bootstrap",'datatables', 'ngSanitize', 'ui.select', 'datatables.buttons',"ngDialog",
        'textAngular', 'localytics.directives','ngMap','google.places','ui.tinymce','ngFileUpload'
    ]);
})();

Qms.controller('ServiceQualifyCtrl',function($scope, $rootScope,$state,FlashMessage,NgMap,GeoCoder,FrontierQualification,ngDialog,$filter,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder , $http){
    $scope.vm = this;
    $scope.advancedShown = false;
    $scope.xmlResponse = "";
    $scope.address = {};
    $scope.fnn = {};
    $scope.autoCompleteOptions = {
        componentRestrictions : {country: 'au'},
        types : 'geocode'
    }
    $scope.locationId = "";

    $scope.sort = {       
            sortingOrder : 'locationId',
            reverse : false
        };
    
    $scope.gap = 10;
    
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 5;
    $scope.pagedItems = [];
    $scope.currentPage = 0;

    NgMap.getMap().then(function(evtMap) {
        //console.log(evtMap);
        $scope.vm.map = evtMap;
        var ll = evtMap.markers[0].position;
        GeoCoder.geocode({'latLng': ll}).then(function(result){
            //console.log(result);
            $scope.deleteMarkers();
            $scope.vm.positions.push({lat:ll.lat(),lng:ll.lng()});
            $scope.locationReference = result[0];
        });
    });

    $scope.getCurrentLocation = function(){
        var position;
        if (navigator.geolocation){
            navigator.geolocation.getCurrentPosition(
                function(position){
                    position = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                }
            );
        } else {
            position = {lat:-33.867926,lng:151.205392};
        }
        if (angular.isUndefined(position)){
            position = {lat:-33.867926,lng:151.205392};   
        }
        //console.log(position);
        return position;
    }

    $scope.vm.positions = [{
                            lat: $scope.getCurrentLocation().lat,
                            lng: $scope.getCurrentLocation().lng
                        }];

    $scope.loadAddress = function(){
        var address = $('#locationReference').val();
        GeoCoder.geocode({'address': address}).then(function(result){
            $scope.locationReference = result[0];
            $('#locationReference').val($scope.locationReference.formatted_address);
        });
    }

    $scope.deleteMarkers = function(){
        $scope.vm.positions = [];
    }

    $scope.placeChanged = function(){
        $scope.vm.place = this.getPlace();
        var location = $scope.vm.place.geometry.location();
        $scope.deleteMarkers();
        $scope.vm.positions.push({lat : location.lat(),lng: location.lng()});
    }

    $scope.addMarker = function(event){
        //nsole.log(event);
        var ll = event.latLng;
        GeoCoder.geocode({'latLng': ll}).then(function(result){
            //console.log(result);
            $scope.deleteMarkers();
            $scope.vm.positions.push({lat:ll.lat(),lng:ll.lng()});
            $scope.locationReference = result[0];
            $('#locationReference').val($scope.locationReference.formatted_address);
        });
    }

    $scope.showMarkers = function(){
        for(var key in $scope.vm.markers){
            vm.map.markers[key].setMap($scope.vm.map);
        }
    }

    $scope.searchAddress = function(){
        // $scope.clearMarkers();
        var location = $scope.locationReference.geometry.location;
        $scope.deleteMarkers();
        //console.log(location);
        $scope.vm.positions.push({lat : location.lat(),lng: location.lng()});
    }

    $scope.toggleAdvanced = function(){
        $scope.advancedShown = !$scope.advancedShown;
    }

    $scope.sqCsv = function() {
        $scope.sqCsv = !$scope.sqCsv;
        FrontierQualification.sq_Csv({addresses:$scope.addresses}, function(response) {
            // console.log($scope.addresses);
            if(response.success){
                $scope.csvFile = response.filename;
                $('#sqCSV').click();
            }
        });

    }
    
    $scope.response = null;    
    $scope.locationId = {};
    
    $scope.qualifyAddress = function(){
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;
        //console.log($scope.locationReference);
        var address = {
            'streetNumber' : $scope.locationReference.address_components[0].short_name,
            'streetName' : $scope.locationReference.address_components[1].long_name,
            'suburb' : $scope.locationReference.address_components[2].short_name,
            'state' : $scope.locationReference.address_components[4].short_name,
            'postcode' : $scope.locationReference.address_components[6].short_name
        };

        FrontierQualification.get_addresses(address, function(data) {
        $scope.loading = false;        
            $scope.addresses = data.response;
            // console.log($scope.items);
            if (data.success){
                $scope.template = "templates/catalog/frontier/sq_address.html";
                ngDialog.open({
                     template: $scope.template,
                     plan: true,
                     controller : 'ServiceQualifyCtrl',
                     width: 1080,
                     scope: $scope,
                     className: 'ngdialog-theme-default',
                     showClose: true,
                     closeByEscape: true,
                     closeByDocument: true
                 });
                $scope.vms = {};
                $scope.vms.dtInstance = {};   
                // $scope.vms.dtColumnDefs = [DTColumnDefBuilder.newColumnDef(2).notSortable()];
                $scope.vms.dtOptions = DTOptionsBuilder.newOptions()
                                  .withOption('paging', true)
                                  .withOption('searching', true)
                                  .withOption('info', true)
                                  .withOption('lengthMenu',[5,10,15,20,50,100])
            }
        });
    }
    $scope.qualifyServiceNumber = function(){
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;
        FrontierQualification.qualify({'serviceNumber' : $scope.fnn.serviceNumber, 'serviceProvider' : ''}, function(data){
            $scope.loading = false;
            $scope.items = data.response;
            if (data.success){
                $scope.template = "templates/catalog/frontier/sq_response.html";
                ngDialog.open({
                     template: $scope.template,
                     plan: true,
                     controller : 'ServiceQualifyCtrl',
                     width: 1080,
                     scope: $scope,
                     className: 'ngdialog-theme-default',
                     showClose: true,
                     closeByEscape: true,
                     closeByDocument: true
                 });
                $scope.vms = {};
                $scope.vms.dtInstance = {};   
                // $scope.vms.dtColumnDefs = [DTColumnDefBuilder.newColumnDef(2).notSortable()];
                $scope.vms.dtOptions = DTOptionsBuilder.newOptions()
                                  .withOption('paging', true)
                                  .withOption('searching', true)
                                  .withOption('info', true)
                                  .withOption('lengthMenu',[5,10,15,20,50,100])
            }
        });

    }


    $scope.qualifyAdvance = function(){
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;
        // console.log($scope.address);
        var advance = {
            'streetNumber' : $scope.address.streetNumber,
            'streetName' : $scope.address.streetName,
            'suburb' : $scope.address.suburb,
            'state' : $scope.address.state,
            'postcode' : $scope.address.postcode
        };

        FrontierQualification.get_advance(advance, function(data) {
            $scope.loading = false;
            console.log(advance);
            $scope.addresses = data.response;
            if (data.success){
                $scope.template = "templates/catalog/frontier/sq_address.html";
                ngDialog.open({
                     template: $scope.template,
                     plan: true,
                     controller : 'ServiceQualifyCtrl',
                     width: 1080,
                     scope: $scope,
                     className: 'ngdialog-theme-default',
                     showClose: true,
                     closeByEscape: true,
                     closeByDocument: true
                     // preCloseCallback : function(value) {
                     //     var signData = $('img.imported').attr('src');
                     //     if ((!signData) && (!$rootScope.isDraftMode)) {
                     //         if (alert("You have not signed yet. Please sign to start filling out data. You can also select Draft Mode to be able to start.")) {
                     //             return true;
                     //         }
                     //         return false;
                     //         $timeout(angular.noop());
                     //     }
                     // }
                 });
                $scope.vms = {};
                $scope.vms.dtInstance = {};   
                // $scope.vms.dtColumnDefs = [DTColumnDefBuilder.newColumnDef(2).notSortable()];
                $scope.vms.dtOptions = DTOptionsBuilder.newOptions()
                                  .withOption('paging', true)
                                  .withOption('searching', true)
                                  .withOption('info', true)
                                  .withOption('lengthMenu',[5,10,15,20,50,100])
            }
        });
    }

    $scope.showClicked = function(item){
        // alert('Clicked');
        //console.log(item);
        $scope.location  = item;
    };

    $scope.qualifyLocationId = function(){
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;
        if ($scope.location!=undefined){
            $scope.msg = ' ';
            //console.log($scope.location);
            // $scope.location = {
            //     'locationId' : '5554545454',
            //     'serviceProvider' : 'nbn'
            // }    
        // console.log($scope.location);
            $scope.vma = {};
            $scope.vma.dtInstance = {};   
            // $scope.vma.dtColumnDefs = [DTColumnDefBuilder.newColumnDef(2).notSortable()];
            $scope.vma.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('lengthMenu',[5,10,15,20,50,100])

            FrontierQualification.qualify($scope.location, function(data) {
                $scope.loading = false;
                $scope.items = data.response;
                if (data.success){
                    $scope.template = "templates/catalog/frontier/sq_response.html";
                    ngDialog.open({
                         template: $scope.template,
                         plan: true,
                         controller : 'ServiceQualifyCtrl',
                         width: 1080,
                         scope: $scope,
                         className: 'ngdialog-theme-default',
                         showClose: true,
                         closeByEscape: true,
                         closeByDocument: true,
                         sableAnimation: false
                         // preCloseCallback : function(value) {
                         //     var signData = $('img.imported').attr('src');
                         //     if ((!signData) && (!$rootScope.isDraftMode)) {
                         //         if (alert("You have not signed yet. Please sign to start filling out data. You can also select Draft Mode to be able to start.")) {
                         //             return true;
                         //         }
                         //         return false;
                         //         $timeout(angular.noop());
                         //     }
                         // }

                     });

                }

                    
            });
             
            
        }

        else{       
            $scope.loading = false;   
            alert('Please choose One Row!');
            // $scope.msg = 'Please choose One Row!';
        }
        
    }
});

Qms.controller('DefaultCtrl', function($scope, Scopes, $rootScope, $http, FlashMessage,$locale) {
    $scope.flashMessage = FlashMessage;
    $scope.currentYear = new Date().getFullYear();
    $scope.currentMonth = new Date().getMonth() + 1;
    $scope.months = $locale.DATETIME_FORMATS.MONTH;
    $rootScope.version = _version;
    //alert($scope.currentYear + '-' + $scope.months);
    $http.get("profile").success(function(response) {
        $scope.profile = response;
        $rootScope.profile = response;
        $rootScope.sign = response.sign;
        Scopes.store('DefaultCtrl', $scope);
    });
})


Qms.controller('RatesCtrl',function($scope, $location, $state,
$stateParams, $filter, Form, $location,Scopes, ScheduleGoods, $rootScope, FlashMessage,
$http, ngDialog,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder,$timeout,
$interval,Category,Upload, $timeout, Rates){
  
  //console.log($stateParams.id);
  if ($stateParams.id) {
    Rates.get({
                id: $stateParams.id
                }, function(data) {
                  $scope.rateForm = {};
                  $scope.rateForm.id = data.id;
                  $scope.rateForm.name = data.name;
                  $scope.rateForm.term = data.term;
                  $scope.rateForm.type = data.type;
                  $scope.rateForm.effective_at = new Date(data.effective_at); 
                  $scope.rateForm.rate = [];
                  angular.forEach(data.rates,function (item,index){
                    $scope.rateForm.rate.push({
                        id : item.id,
                        rate : item.rate,
                        lowerLimit : item.type,
                        lowerLimit : item.lowerLimit,
                        upperLimit : item.upperLimit
                      })
                  })
             });
  } else {
    $scope.rateForm={};
    $scope.rateForm.name ='';
    $scope.rateForm.term ='';
    $scope.rateForm.type ='';
    $scope.rateForm.rate = [{id: '1', rate:'', lowerLimit:'',  upperLimit:''}];
    $scope.rateForm.effective_at = new Date();
  }

  $scope.addRates = function() {
    var newItemNo = $scope.rateForm.rate.length+1;
    $scope.rateForm.rate.push({'id':+newItemNo});
  };

  $scope.removeRates = function(index) {
    $scope.rateForm.rate.splice(index,1);
  };

  $scope.saveRate = function() {
   if (angular.isDefined($scope.rateForm.id)) {
            Rates.update({
                id: $scope.rateForm.id
            }, $scope.rateForm, function(data) {
                FlashMessage.setMessage(data);
                if (data.success)
                    $location.path("/rates");
            });
            console.log($scope.rateForm);
        }
    else{
      Rates.save($scope.rateForm, 
                  function(data) {
                      $scope.editId = data.editId;
                       if (data.success)
                      $location.path("/rates");
                  });    
        }
    }
})

Qms.controller('RatesListCtrl',function($scope,$stateParams,$rootScope,$filter,$location,$http,$state,ScheduleGoods,FileUploader,FlashMessage,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder, Rates){
    $scope.vmrates = {};
            $scope.vmrates.dtInstance = {};   
            $scope.vmrates.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('order', [[8, 'desc']])
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500]);
    $scope.rateForm = Rates.query();

    $scope.edit = function(row_id) {
           $state.go('edit_rates', {
              id: row_id
            });
           console.log($scope.rateForm);
    }
    $scope.delete = function(row_id) {
        Rates.delete({
            id: row_id
        }, function(data) {
            FlashMessage.setMessage(data);
            if (data.success == true)
                $scope.rateForm = Rates.query();

        });
    }

});


Qms.controller('ScheduleGoodsCtrl',function($scope, $location, $state,
$stateParams, $filter, Form, Scopes, ScheduleGoods, $rootScope, FlashMessage,
$http, ngDialog,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder,$timeout,
$interval,Category,FileUploader, $timeout){
   
    $scope.categories = Category.query();

    Form.getScheduleGoods(function(data) {
        $scope.scheduleGoodsList = data.goods;
        $scope.scheduleGoodsList.unshift({
            item: ""
        });
    });
     
    if ($stateParams.id) {
        $scope.sg_form = ScheduleGoods.get({
            id: $stateParams.id
        }, function(data) {

        });
    }
    else {
        $scope.sg_form ={};
        $scope.sg_form.descriptions = '';
        $scope.sg_form.features = '';
        $scope.sg_form.hardware_solutions = '';
        $scope.sg_form.img1 = '';
        $scope.sg_form.img2 = '';
        $scope.sg_form.item = '';
        $scope.sg_form.rrp = 0;
        $scope.sg_form.model = 0;
    }


    var uploaders = $scope.uploaders = new FileUploader({
        url: '/schedule_goods/upload',
        formData: [{
            name: null
        }]
    });

    var dataPath;

    $scope.hiders = [{
        id: 1,
        title: "No"
    }, {
        id: 2,
        title: "Yes"
    }]

    uploaders.onSuccessItem = function(fileItem, response, status, headers) {
        dataPath = response.FilePath;
        $scope.sg_form.img2 = dataPath;
    };

    var uploader = $scope.uploader = new FileUploader({
        url: '/schedule_goods/upload',
        formData: [{
            name: null
        }]
    });

    $scope.hiders = [{
        id: 1,
        title: "No"
    }, {
        id: 2,
        title: "Yes"
    }]

    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        dataPath = response.FilePath;
        $scope.sg_form.img1 = dataPath;
    };

    $scope.save = function() {
          if (!$("#sg_form").parsley('validate')) return false;
          console.log($scope.sg_form);
          if (angular.isDefined($scope.sg_form.id)) {
              console.log($scope.sg_form);
              ScheduleGoods.update({
                  id: $scope.sg_form.id
              }, $scope.sg_form, function(data) {
                  FlashMessage.setMessage(data);
                  if (data.success)
                      $location.path("/schedule_goods");
              });
          } else {
              ScheduleGoods.save($scope.sg_form, function(data) {
                  FlashMessage.setMessage(data);
                  if (data.success)
                      $location.path("/schedule_goods");
              });
          }
    }

  $scope.tinymceOptions = {
    selector: "textarea",
    theme: "modern",
    paste_data_images: true,
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "print preview media | forecolor backcolor emoticons "
  };
  
})

Qms.controller('ScheduleGoodsListCtrl',function($scope,$filter,$http,$state,ScheduleGoods,FileUploader,FlashMessage,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder, Category){
    $scope.vmsc = {};
            $scope.vmsc.dtInstance = {};   
            $scope.vmsc.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('order', [[5, 'desc']])
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500]);

    $scope.sched_goods = ScheduleGoods.query();
    $scope.categories = Category.query();
    $scope.edit = function(row_id) {
        var x=window.confirm("Do you want to Modify this?")
        if (x)
        $state.go('edit_sg', {
            id: row_id
        });
    }
    $scope.delete = function(row_id) 
    {
        var x=window.confirm("Do you want to Delete this?")
        if (x)
        ScheduleGoods.delete({
            id: row_id
        }, function(data) {
            FlashMessage.setMessage(data);
            if (data.success == true)
                $scope.sched_goods = ScheduleGoods.query();

        });
    }
});



Qms.controller('ProductListCtrl', function($scope, $filter, $http, $state, Product, FileUploader, FlashMessage,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder) {
    
    $scope.vmp = {};
            $scope.vmp.dtInstance = {};   
            // $scope.vmp.dtColumnDefs = [DTColumnDefBuilder.newColumnDef(2).notSortable()];
            $scope.vmp.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500])
    $scope.products = Product.query();

    $scope.edit = function(row_id) {
        $state.go('edit_product', {
            id: row_id
        });
    }

    $scope.delete = function(row_id) {
        Product.delete({
            product_id: row_id
        }, function(data) {
            FlashMessage.setMessage(data);
            if (data.success == true)
                $scope.products = Product.query();

        });
    }
});

Qms.controller('ProductCtrl', function($scope, $http, $state, $location, $stateParams, Product, Category, FlashMessage, FileUploader) {
    var uploader = $scope.uploader = new FileUploader({
        url: 'product/upload',
        formData: [{
            name: null
        }]
    });

    var dataPath;

    console.log(dataPath);
    $scope.hiders = [{
        id: 1,
        title: "No"
    }, {
        id: 2,
        title: "Yes"
    }]

    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        dataPath = response.FilePath
    };

    if ($stateParams.id)
        $scope.product = Product.get({
            product_id: $stateParams.id
        });
    else
        $scope.product = {};

    
    $scope.products = Product.query();


    $scope.save = function() {

        if (!$("#products_form").parsley('validate'))
            return;

        if (angular.isDefined($scope.product.id)) {
            $scope.product.image = dataPath;
            Product.update({
                product_id: $scope.product.id
            }, $scope.product, function(data) {
                FlashMessage.setMessage(data);

                if (data.success)
                    $location.path("/products");
            })
        } else {
            Product.save($scope.product, function(data) {
                FlashMessage.setMessage(data);

                if (data.success)
                    $location.path("/products");

            })

        }
    }

});



Qms.controller('CategoryListCtrl', function($scope, $http, $state, $location, $stateParams, Category, FlashMessage,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder) {
    $scope.vmc = {};
            $scope.vmc.dtInstance = {};   
            // $scope.vmc.dtColumnDefs = [DTColumnDefBuilder.newColumnDef(2).notSortable()];
            $scope.vmc.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500])
    $scope.categories = Category.query();
    $scope.edit = function(row_id) {
        $state.go('edit_category', {
            id: row_id
        });
    }

    $scope.delete = function(row_id) {
        Category.delete({
            category_id: row_id
        }, function(data) {
            FlashMessage.setMessage(data);
            if (data.success == true)
                $scope.categories = Category.query();
        });
    }

});


Qms.controller('CategoryCtrl', function($scope, $http, $state, $location, $stateParams, Category, FinanceType, Typet, FlashMessage) {
    $scope.categories = Category.query();

    if ($stateParams.id)
        $scope.category = Category.get({
            category_id: $stateParams.id
        });
    else
        $scope.category = {};

    $scope.financetypes = FinanceType.query();
    $scope.typets = Typet.query();

    $scope.save = function() {
        if (!$("#cat_form").parsley('validate')) return false;

        if (angular.isDefined($scope.category.id)) {
            Category.update({
                category_id: $scope.category.id
            }, $scope.category, function(data) {
                FlashMessage.setMessage(data);

                if (data.success)
                    $location.path("/categories");
            });

        } else {


            Category.save($scope.category, function(data) {
                FlashMessage.setMessage(data);
                if (data.success)
                    $location.path("/categories");
            });
        }
    }

});

Qms.controller('UserListCtrl', function($scope, $http, $state, $location, $stateParams, User, FlashMessage,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder) {
    $scope.vmu = {};
            $scope.vmu.dtInstance = {};   
            $scope.vmu.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500])
    

    $scope.users = User.query();
    
    $scope.edit = function(row_id) {
        $state.go('edit_user', {
            id: row_id
        });
    }

    $scope.delete = function(row_id) {
        User.delete({
            user_id: row_id
        }, function(data) {
            FlashMessage.setMessage(data);
            if (data.success == true)
                $scope.users = User.query();

        });
        
    }
});


Qms.controller('UserCtrl', function($scope, $http, $state, $location, $stateParams, User, FlashMessage, Scopes, $rootScope) {
    $scope.users = User.query();
    $scope.noSign = true;

    if ($stateParams.id) {
        $scope.user = User.get({
            user_id: $stateParams.id
        }, function(data) {
          if (data.sign && data.sign_exists) {
              $scope.noSign = false;
          }
        });
    }
    else {
        $scope.user = {};
    }

    $scope.profile = [];
    $scope.readonly = true;
    
    $http.get("profile").success(function(response) {
            $scope.profile = response;
            if (response.type == 'admin') {
                $scope.readonly = false;
            }
        if ($state.current.url == "/user/settings") {
            $state.go('edit_user', {
            id: $scope.profile.id
        });

    }
        });
    $scope.$watch('profile');
    $scope.$watch('readonly');
    $scope.$watch('user');


    $('#userSign').jSignature({color:"#000000",lineWidth:1,
                                   width :350, height :70,
                                   cssclass : "signature-canvas",
                                  "decor-color": 'transparent'
                                 });

    $scope.clearSg = function() {
        $scope.noSign = true;
        $("#userSign").jSignature("reset");
    }


    $scope.save = function() {
        var pInstance = $("#user_form").parsley();

        if ( !pInstance.validate(true) ) {
            return false;
        }

        //get the signature

        if ($("#userSign").jSignature('getData', 'native').length != 0) {
            var signData = $("#userSign").jSignature('getData');
            $scope.user.signData = signData;
        } else {
            $scope.user.signData = "";
        }

        $scope.user.financeonly = $('input[name=financeonly]').prop('checked');

        if (angular.isDefined($scope.user.id)) {
            User.update({
                user_id: $scope.user.id
            }, $scope.user, function(data) {

                FlashMessage.setMessage(data);
                if (data.success && !$scope.readonly) {
                    $location.path("/users");
                    $http.get("profile").success(function(response) {
                        $rootScope.sign = response.sign;
                    });
                } else {
                   $(window).scrollTop(0);
                }
            });

        } else {

            User.save($scope.user, function(data) {
                FlashMessage.setMessage(data);
                if (data.success) {
                    $location.path("/users");
                    $http.get("profile").success(function(response) {
                        $rootScope.sign = response.sign;
                    });
                }
            })
        }
    }
});




Qms.controller('CompanyListCtrl', function($scope, $http, $state, $location, $stateParams, Company, FlashMessage) {

    $scope.companies = Company.query();


    $scope.edit = function(row_id) {
        $state.go('edit_company', {
            id: row_id
        });
    }

    $scope.delete = function(row_id) {
        Company.delete({
            company_id: row_id
        }, function(data) {
            FlashMessage.setMessage(data);
            if (data.success == true)
                $scope.companies = Company.query();

        });
    }

});


Qms.controller('CompanyCtrl', function($scope, $http, $state, $location, $stateParams, Company, FlashMessage) {
    $scope.companies = Company.query();

    if ($stateParams.id)
        $scope.company = Company.get({
            company_id: $stateParams.id
        });
    else
        $scope.company = {};


    $scope.save = function() {
        if (!$("#company_form").parsley('validate')) return;

        if (angular.isDefined($scope.company.id)) {

            Company.update({
                company_id: $scope.company.id
            }, $scope.company, function(data) {
                FlashMessage.setMessage(data);

                if (data.success) $location.path("/companies");
            })

        } else {
            Company.save($scope.company, function(data) {
                FlashMessage.setMessage(data);

                if (data.success) $location.path("/companies");
            })

        }
    }

});


Qms.controller('SalesRepListCtrl', function($scope, $http, $state, $location, $stateParams, Salesrep, FlashMessage) {
    $scope.salesreps = Salesrep.query();


    $scope.edit = function(row_id) {
        $state.go('edit_salesrep', {
            id: row_id
        });
    }

    $scope.delete = function(row_id) {
        Salesrep.delete({
            salesrep_id: row_id
        }, function(data) {
            FlashMessage.setMessage(data);
            if (data.success == true)
                $scope.salesreps = Salesrep.query();


        });
    }

});


Qms.controller('SalesRepCtrl', function($scope, $http, $state, $location, $stateParams, Salesrep, FlashMessage) {

    $scope.salesreps = Salesrep.query();

    if ($stateParams.id)
        $scope.salesrep = Salesrep.get({
            salesrep_id: $stateParams.id
        });
    else
        $scope.salesrep = {};


    $scope.save = function() {

        if (!$("#salesrep_form").parsley('validate')) return;

        if (angular.isDefined($scope.salesrep.id)) {

            Salesrep.update({
                salesrep_id: $scope.salesrep.id
            }, $scope.salesrep, function(data) {
                FlashMessage.setMessage(data);

                if (data.success) $location.path("/salesreps");
            })

        } else {
            Salesrep.save($scope.salesrep, function(data) {
                FlashMessage.setMessage(data);

                if (data.success) $location.path("/salesreps");
            })

        }


    }
});




Qms.controller('TelemarketerListCtrl', function($scope, $http, $state, $location, $stateParams, Telemarketer, FlashMessage) {

    $scope.telemarketers = Telemarketer.query();

    $scope.edit = function(row_id) {
        $state.go('edit_telemarketer', {
            id: row_id
        });
    }

    $scope.delete = function(row_id) {
        Telemarketer.delete({
            telemarketer_id: row_id
        }, function(data) {
            FlashMessage.setMessage(data);
            if (data.success == true)
                $scope.telemarketers = Telemarketer.query();

        });
    }
});


Qms.controller('TelemarketerCtrl', function($scope, $http, $state, $location, $stateParams, Telemarketer, FlashMessage) {
    $scope.telemarketers = Telemarketer.query();

    if ($stateParams.id)
        $scope.telemarketer = Telemarketer.get({
            telemarketer_id: $stateParams.id
        });
    else
        $scope.telemarketer = {};


    $scope.save = function() {

        if (!$("#telemarketer_form").parsley('validate')) return;

        if (angular.isDefined($scope.telemarketer.id)) {

            Telemarketer.update({
                telemarketer_id: $scope.telemarketer.id
            }, $scope.telemarketer, function(data) {
                FlashMessage.setMessage(data);

                if (data.success) $location.path("/telemarketers");
            })

        } else {
            Telemarketer.save($scope.telemarketer, function(data) {
                FlashMessage.setMessage(data);

                if (data.success) $location.path("/s");
            })

        }


    }
});


Qms.controller('WinBackListCtrl', function($scope, $http, $state, $location, $stateParams, Winback, FlashMessage) {

    $scope.winbacks = Winback.query();

    $scope.edit = function(row_id) {
        $state.go('edit_winback', {
            id: row_id
        });
    }

    $scope.delete = function(row_id) {
        Winback.delete({
            winback_id: row_id
        }, function(data) {
            FlashMessage.setMessage(data);
            if (data.success == true)
                $scope.winbacks = Winback.query();

        });
    }
});


Qms.controller('WinBackCtrl', function($scope, $http, $state, $location, $stateParams, Winback, FlashMessage) {
    $scope.winbacks = Winback.query();

    if ($stateParams.id)
        $scope.winback = Winback.get({
            winback_id: $stateParams.id
        });
    else
        $scope.winback = {};


    $scope.save = function() {

        if (!$("#winback_form").parsley('validate')) return;

        if (angular.isDefined($scope.winback.id)) {

            Winback.update({
                winback_id: $scope.winback.id
            }, $scope.winback, function(data) {
                FlashMessage.setMessage(data);

                if (data.success) $location.path("/winbacks");
            })

        } else {
            Winback.save($scope.winback, function(data) {
                FlashMessage.setMessage(data);

                if (data.success) $location.path("/winbacks");
            })

        }


    }

});



Qms.controller('LeadSourceListCtrl', function($scope, $http, $state, $location, $stateParams, Leadsource, FlashMessage) {
    $scope.leadsources = Leadsource.query();

    $scope.edit = function(row_id) {
        $state.go('edit_leadsource', {
            id: row_id
        });
    }

    $scope.delete = function(row_id) {
        Leadsource.delete({
            leadsource_id: row_id
        }, function(data) {
            FlashMessage.setMessage(data);
            if (data.success == true) {
                $scope.leadsources = Leadsource.query();

            }
        });
    }
});


Qms.controller('LeadSourceCtrl', function($scope, $http, $state, $location, $stateParams, Leadsource, FlashMessage) {
    $scope.leadsources = Leadsource.query();

    if ($stateParams.id)
        $scope.leadsource = Leadsource.get({
            leadsource_id: $stateParams.id
        });
    else
        $scope.leadsource = {};


    $scope.save = function() {

        if (!$("#leadsource_form").parsley('validate')) return;

        if (angular.isDefined($scope.leadsource.id)) {

            Leadsource.update({
                leadsource_id: $scope.leadsource.id
            }, $scope.leadsource, function(data) {
                FlashMessage.setMessage(data);

                if (data.success) $location.path("/leadsources");
            })

        } else {
            Leadsource.save($scope.leadsource, function(data) {
                FlashMessage.setMessage(data);

                if (data.success) $location.path("/leadsources");
            })

        }

    }

});




Qms.controller('ConfigCtrl', function($scope, $http, $state, $location, $stateParams, Config, FlashMessage) {
    $scope.config = Config.get();



    $scope.save = function() {

        if (!$("#config_form").parsley('validate')) return;

        //console.log($scope.config);
        Config.save($scope.config, function(data) {
            FlashMessage.setMessage(data);

            if (data.success) $location.path("/config");
        })


    }

});



Qms.controller('FinanceRatesCtrl', function($scope, $http, $state, $location, $stateParams, SellPrice, RentalPrice, FlashMessage) {
    $scope.sellprice = SellPrice.get();
    $scope.rentalprice = RentalPrice.get();


    $scope.saveSellPrice = function() {

        SellPrice.save($scope.sellprice, function(data) {
            FlashMessage.setMessage(data);

            if (data.success) $location.path("/finance_rates");
        })

    }

    $scope.saveRentalPrice = function() {

        RentalPrice.save($scope.rentalprice, function(data) {
            FlashMessage.setMessage(data);

            if (data.success) $location.path("/finance_rates");
        })

    }

});




Qms.controller('FlexiRentCtrl', function($scope, $http, $state, $location, $stateParams, FlexiRent, FlashMessage) {
    $scope.fr = FlexiRent.query(function(fr) {

    });


    $scope.remRow = function(i, type) {
        $scope.fr["fr_" + type].splice(i, 1);
    }

    $scope.addRow = function(type) {
        //console.log(type);
        $scope.fr["fr_" + type].push({});
    }


    $scope.saveFREGST = function() {

        FlexiRent.save($scope.fr, function(data) {
            FlashMessage.setMessage(data);

            if (data.success) $location.path("/flexi_rent");
        })

    }

    $scope.saveRentalPrice = function() {

        RentalPrice.save($scope.rentalprice, function(data) {
            FlashMessage.setMessage(data);

            if (data.success) $location.path("/flexi_rent");
        })

    }

});

Qms.controller('ProductViewCtrl', function($scope, Products, $stateParams){
    var items = [];
        $scope.products = Products.query({id :$stateParams.id}, function() {
           var itemObj = $scope.products.items;
           $.each(itemObj, function(){
               var obj = $(this)[0];

               var item = [obj.name,obj.category,obj.description,obj.qty,obj.rrp_tax,obj.sell_price,obj.tax];

               items.push(item);

           });



        });

       function exportToCsv(filename, rows) {
        var processRow = function (row) {
            var finalVal = '';
            for (var j = 0; j < row.length; j++) {
                var innerValue = row[j] == null ? '' : row[j].toString();
                if (row[j] instanceof Date) {
                    innerValue = row[j].toLocaleString();
                };
                var result = innerValue.replace(/"/g, '""');
                if (result.search(/("|,|\n)/g) >= 0)
                    result = '"' + result + '"';
                if (j > 0)
                    finalVal += ',';
                finalVal += result;
            }
            return finalVal + '\n';

        };
        var header ='Name,Category,Description,Quantity,Tax,RRP Tax,Sell Price\n';
        var csvFile = header;
        for (var i = 0; i < rows.length; i++) {
            csvFile += processRow(rows[i]);
        }

        var blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
        if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(blob, filename);
        } else {
            var link = document.createElement("a");
            if (link.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                var url = URL.createObjectURL(blob);
                link.setAttribute("href", url);
                link.setAttribute("download", filename);
                link.style = "visibility:hidden";
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
    }
    $("#downloadCsv").on("click", function() {
           exportToCsv("proposal-"+$stateParams.id+".csv",items);
    });

});

Qms.controller('FeedbacksListCtrl',function($scope, $rootScope,$filter,$http,$state,FileUploader,FlashMessage,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder, Feedbacks){
   $scope.feedbackForm = Feedbacks.query();

   $scope.edit = function(row_id) {
        $state.go('edit_feedbacks', {
            id: row_id
        });
    }
    $scope.delete = function(row_id) {
        Feedbacks.delete({
            id: row_id
        }, function(data) {
            FlashMessage.setMessage(data);
            if (data.success == true)
                $scope.feedbackForm = Feedbacks.query();

        });
    }
   $scope.vmfb = {};
            $scope.vmfb.dtInstance = {};   
            $scope.vmfb.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('order', [[4, 'desc']])
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500]);
});


Qms.controller('FeedbacksCtrl',function($scope,$filter, $rootScope,$stateParams,FileUploader,$location,$http,$state,FlashMessage,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder, Feedbacks){
  
  $scope.feedbackForm = Feedbacks.query();

  var uploader = $scope.uploader = new FileUploader({
        url: 'feedbacks/upload',
        formData: [{
            name: null
        }]
    });

    var dataPath;
    // console.log(dataPath);
    $scope.hiders = [{
        id: 1,
        title: "No"
    }, {
        id: 2,
        title: "Yes"
    }]

    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        dataPath = response.FilePath
    };
  if ($stateParams.id) {
        $scope.feedbackForm = Feedbacks.get({
            id: $stateParams.id,
            logo : $scope.feedbackForm.logo
        }, function(data) {

        });
    }
  $scope.savefb = function() {
        if (!$("#feedbackForm").parsley('validate')) return false;
        // console.log(dataPath);
        if (angular.isDefined($scope.feedbackForm.id)) {
          // $scope.feedbackForm.logo = dataPath;
          if(angular.isDefined(dataPath)){
            $scope.feedbackForm.logo = dataPath;
              Feedbacks.update({
                id: $scope.feedbackForm.id,
                logo : $scope.feedbackForm.logo
              }, $scope.feedbackForm, function(data) {
                  FlashMessage.setMessage(data);
                  if (data.success)
                      $location.path("/feedbacks");
              });
          }
          else{
            
             Feedbacks.update({
                id: $scope.feedbackForm.id,
                logo : $scope.feedbackForm.logo
              }, $scope.feedbackForm, function(data) {
                  FlashMessage.setMessage(data);
                  if (data.success)
                      $location.path("/feedbacks");
              });
          }
        } else {
            $scope.feedbackForm.logo = dataPath;
            Feedbacks.save({fb:$scope.feedbackForm.fb,
                            logo:$scope.feedbackForm.logo}, function(data) {
                FlashMessage.setMessage(data);
                if (data.success)
                    $location.path("/feedbacks");
            });
        }
    }


   $scope.tinymceOption = {
    selector: "textarea",
    theme: "modern",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "print preview media | forecolor backcolor emoticons "
  };
});


Qms.controller('ProposalCtrl',function($scope, $http, $state, $location, $stateParams, FlashMessage, Scopes, $rootScope,Proposals,ngDialog,Form,Rates,ScheduleGoods,$anchorScroll,FileUploader,Feedbacks,ProposalActions,$timeout) {
    Form.getScheduleGoods(function(data) {
        $scope.scheduleGoodsList = data.goods;
        $scope.scheduleGoodsList.unshift({
            item: ""
        });
    });

    $scope.scheduleCount = 0;
    $scope.locationCount = 0;
    $scope.accountLocation = 0;
    $scope.addTelCount = 0;
    $scope.addMobPortCount = 0;
    $scope.htmlData = {};
    $scope.discountArr = {};
    $scope.companyLocations = [];
    $rootScope.draftMode = false;

    $scope.locationAvailableTemps = [
        'nbf_cap',
        'nbf_std',
        'voice_comp',
        'voice_solution_untimed',
        'voice_essentials_cap',
        'voice_solution_standard',
        'data_adsl',
        'ip_midband',
        'ip_midbandunli',
        'nbnUnlimited',
        'nbn',
        'mobile_mega',
        'mobile_ut',
        'mobile_wireless',
        '1300',
        '1300_discounted',
        'fibre',
        'ethernet',
        'telstraUntimed',
        'telstraWireless',
        'printer'
    ];

    $scope.items = {
        'nbf_cap'               : [0],
        'nbf_std'               : [0],
        'voice_comp'            : [0],
        'voice_solution_untimed'  : [0],
        'voice_essentials_cap'    : [0],
        'voice_solution_standard' : [0],
        'data_adsl'               : [0],
        'ip_midband'              : [0],
        'ip_midbandunli'          : [0],
        'nbn'                     : [0],
        'nbnUnlimited'            : [0],
        'mobile_mega'             : [0],
        'mobile_ut'               : [0],
        'mobile_wireless'         : [0],
        '1300'                    : [0],
        '1300_discounted'         : [0],
        'fibre'                   : [0],
        'ethernet'                : [0],
        'telstraUntimed'          : [0],
        'telstraWireless'         : [0],
        'printer'                 : [0]
    };

    $scope.recurring_services = [
        {'description' : 'Monitoring Centre connect 24 hours','price' : 59.00},
        {'description' : "Monitoring Centre connect 24 hours (Including NBN/3G Wireless Unit)",'price' : 89}
    ];

    $scope.ethernet_plans = [
        {'description' : 'Up to 10Mbps/10Mbps* Unlimited','price':499},
        {'description' : 'Up to 20Mbps/20Mbps* Unlimited','price':599},
    ];

    $scope.nbnUnlimited_plans = [
        {'description' : 'NBN Standard 25','inclusion' : 'Download speeds Between 5Mbps and 25Mbps*<br/>Upload speeds Between 1Mbps and 5Mbps*','price':159},
        {'description' : 'NBN Fast 50','inclusion' : 'Download speeds Between 12Mbps and 50Mbps*<br/>Upload speeds Between 1Mbps and 20Mbps*','price':179},
        {'description' : 'NBN Ultra fast 100','inclusion' : 'Download speeds Between 12Mbps and 100Mbps*<br/>Upload speeds Between 1Mbps and 40Mbps*','price':189}
    ];

    $scope.mobile_mega_plans = [
        {'description' : 'Cap 39','inclusions' : '$250 inc. calls 500MB','price':39},
        {'description' : 'Cap 49','inclusions' : '$2500 inc. calls 2GB','price':49},
        {'description' : 'Cap 59','inclusions' : '$2500 inc. calls 3GB','price':59},
    ];

    $scope.data_bolt_on_plans = [
        {'description' : 'Data Bolt-on 1GB','memory': '1GB','price':9.89,'itemIndex':0},
        {'description' : 'Data Bolt-on 5GB','memory': '5GB','price':31.99,'itemIndex':1}
    ];
    
    $scope.telstra_rate_plans = [
        {'plan' : 'Select a Mobile Plan','inclusions':''},
        {'group':'Telstra 4G','plan':'Telstra 4G Lite','inclusions':'$1000 Calls 1GB Data','price':49,'itemIndex':0},
        {'group':'Telstra 4G','plan':'Telstra 4G Business Executive','inclusions':'Unlimited Calls 3GB Data','price':69,'itemIndex':1},
        {'group':'Telstra 4G','plan':'Telstra 4G Business Professional','inclusions':'Unlimited Calls 7GB Data','price':79,'itemIndex':2},
        {'group':'Telstra 4G','plan':'Telstra 4G Business Maximum','inclusions':'Unlimited Calls 10GB Data','price':89,'itemIndex':3}
    ];

    $scope.untimed_rate_plans = [
        {'plan' : 'Select a Mobile Plan','inclusions':''},
        {'group':'Mobile 4G Untimed','plan':'Moblie 4G Untimed 49','inclusions':'Includes 700MB','price':49,'itemIndex':0},
        {'group':'Mobile 4G Untimed','plan':'Moblie 4G Untimed 59','inclusions':'Includes 2GB','price':59,'itemIndex':1},
        {'group':'Mobile 4G Untimed','plan':'Moblie 4G Untimed 79','inclusions':'Includes 4GB and 300 INTERNATIONAL MINS','price':79,'itemIndex':2},
        {'group':'Mobile 4G Untimed','plan':'Moblie 4G Untimed 89','inclusions':'Includes 6GB and 300 INTERNATIONAL MINS','price':89,'itemIndex':3}
    ];
    // $scope.mobile_mega_rate_plans = [
    //     {'plan' : 'Select a Mobile Plan','inclusions':''},
    //     {'group':'Mobile Mega','plan':'Mobile Mega Cap 39','inclusions':'$250 inc. calls 500Mb','price':39,'itemIndex':0},
    //     {'group':'Mobile Mega','plan':'Mobile Mega Cap 49','inclusions':'$2500 inc. calls 2GB','price':49,'itemIndex':1},
    //     {'group':'Mobile Mega','plan':'Mobile Mega Cap 59','inclusions':'$2500 inc. calls 3GB','price':59,'itemIndex':2}
    // ];

    $scope.configMobPort = [
        {'confAccHolder':'','confMobNo':'','confProvider':'','confAccNo':'','confPatPlan':$scope.telstra_rate_plans[0],'boltOn':{}}
    ];

    $scope.adsl_plans = [
        {'description' : 'ADSL 2 + LITE','details' : '(Unlimited Lite (no static IP))','price':79},
        {'description' : 'ADSL 2 + Unlimited','details' : 'Business Grade (ON NET)','price':109},
        {'description' : 'ADSL 2 + 100GB','details' : 'Business Grade (OFF NET)','price':109},
        {'description' : 'ADSL 2 + 200GB','details' : 'Business Grade (OFF NET)','price':129},
        {'description' : 'ADSL 2 + 300GB','details' : 'Business Grade (OFF NET)','price':139},
        {'description' : 'ADSL 2 + 500GB','details' : 'Business Grade (OFF NET)','price':149},
        {'description' : 'ADSL 2 + 1TB','details' : 'Business Grade (OFF NET)','price':169},
    ];

    $scope.telstraUntimed_plans = [
        {'description' : 'Lite','inclusions' : 'INCLUDES $1000 National Calls,INCLUDES 1GB,UNLIMITED NATIONAL SMS,$0.69 PER MMS','price':49},
        {'description' : 'Business Executive','inclusions' : 'UNLIMITED NATIONAL CALLS**,INCLUDES 3GB,UNLIMITED NATIONAL SMS,UNLIMITED NATIONAL MMS,100 INTERNATIONAL MINS*','price':69},
        {'description' : 'Business Professional','inclusions' : 'UNLIMITED NATIONAL CALLS**,INCLUDES 10GB,UNLIMITED NATIONAL SMS,UNLIMITED NATIONAL MMS,300 INTERNATIONAL MINS*','price':79},
        {'description' : 'Business Maximum','inclusions' : 'UNLIMITED NATIONAL CALLS**,INCLUDES 20GB,UNLIMITED NATIONAL SMS,UNLIMITED NATIONAL MMS,300 INTERNATIONAL MINS*','price':89},
    ];


    $scope.telstraWireless_plans = [
        {'description' : 'Broadband Wireless 3GB','price':43},
        {'description' : 'Broadband Wireless 6GB','price':64},
        {'description' : 'Broadband Wireless 10GB','price':93}
    ];

    $scope.ip_midbandunli_plans = [
        {'description' : '10Mbps/10Mbps*','price':549},
        {'description' : '20Mbps/20Mbps*','price':699},
        {'description' : '40Mbps/40Mbps*','price':999}
    ];

    $scope.ip_midband_plans = [
        {'description' : 'None','price':0},
        {'description' : '6Mbps/6Mbps*','price':319},
        {'description' : '8Mbps/8Mbps*','price':349},
        {'description' : '18Mbps/18Mbps*','price':399},
        {'description' : '20Mbps/20Mbps*','price':449},
        {'description' : '30Mbps/30Mbps*','price':799},
        {'description' : '40Mbps/40Mbps*','price':849}
    ];

    $scope.professional_install = [
        {'description' : 'ASDL 2+ 20/1Mbps','price':399},
        {'description' : 'ASDL 2+ 20/3 to 40Mbps Midband','price':599},
    ];

    $scope.download_plans = [
        {'description' : 'None','price':0},
        {'description' : '50 GB','price':19},
        {'description' : '100 GB','price':35},
        {'description' : '200 GB','price':69},
        {'description' : '300 GB','price':99},
        {'description' : '500 GB','price':149},
        {'description' : '1000 GB','price':289}
    ];

    $scope.mobile_4G_Untimed_Calls = [
        {'description' : '4G Untimed 49','inclusions' : 'Includes 700MB','price':49},
        {'description' : '4G Untimed 59','inclusions' : 'Includes 2GB','price':59},
        {'description' : '4G Untimed 79','inclusions' : 'Includes 4GB,300 INTERNATIONAL MINS*','price':79},
        {'description' : '4G Untimed 89','inclusions' : 'Includes 6GB,300 INTERNATIONAL MINS*','price':89}
    ];

    $scope.wireless_cap_plans = [
        {'description' : 'Wireless 15 - 500MB','price':15},
        {'description' : 'Wireless 19 - 1GB','price':19},
        {'description' : 'Wireless 29 - 2GB','price':29},
        {'description' : 'Wireless 39 - 3GB','price':39},  
        {'description' : 'Wireless 44 - 4GB','price':44},
        {'description' : 'Wireless 49 - 6GB','price':49},
        {'description' : 'Wireless 59 - 8GB','price':59},
        {'description' : 'Wireless 119 - 12GB','price':119}
    ];

    $scope.fibre_unli_plans = [
        {'description' : '10Mbps / 10Mbps Unlimited - 36 month term','price':599},
        {'description' : '20Mbps / 20Mbps Unlimited - 36 month term','price':699},
        {'description' : '100Mbps / 100Mbps Unlimited - 36 month term','price':1199},
        {'description' : '400Mbps / 400Mbps Unlimited - 36 month term','price':758},
        {'description' : '500Mbps / 500Mbps Unlimited - 36 month term','price':1599},
        {'description' : '1000Mbps / 1000Mbps Unlimited - 24 month term','price':1899},
        {'description' : '1000Mbps / 1000Mbps Unlimited - 36 month term','price':1699},
        {'description' : '1000Mbps / 1000Mbps Unlimited - 48 month term','price':1299},
    ];

    $scope.addCompanyLocation = function(form_type) {
        if ($scope.items[form_type].length!=0){
            $scope.locationCount = $scope.items[form_type].length-1;
        }
        $scope.locationCount++;
        if (form_type=='voice_solution_standard'){
            $scope.proposalData.voiceSolutionChannel[$scope.locationCount] = 149;
            $scope.proposalData.voiceSolutionDID[$scope.locationCount] = 10;
            $scope.proposalData.bFSolutionDiscount[$scope.locationCount] = 0;
            $scope.proposalData.bFSolutionCallDiscount[$scope.locationCount] = 0;
        }
        if (form_type=='voice_solution_untimed'){
            $scope.proposalData.voiceUntimedChannel[$scope.locationCount] = 3;
            $scope.proposalData.voiceUntimedDID[$scope.locationCount] = 10;
            $scope.proposalData.bFUntimedDiscount[$scope.locationCount] = 0;
        }
        if (form_type=='voice_comp'){
            $scope.proposalData.voiceCompAnalouge[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompBri[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompPri[$scope.locationCount] = 299.85;
            $scope.proposalData.voiceCompDID[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompBriDis[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompPriDis[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompCallDis[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompAnalougeDisDSL[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompAnalougeDis[$scope.locationCount] = 0;
        }
        if (form_type=='nbf_std'){
            $scope.proposalData.voiceStdChannel[$scope.locationCount] = 5;
            $scope.proposalData.voiceStdDID[$scope.locationCount] = 10;
            $scope.proposalData.bFStdDiscount[$scope.locationCount] = 0;
            $scope.proposalData.bFCallDiscount[$scope.locationCount] = 0;
        }
        if (form_type=='nbf_cap'){
            $scope.proposalData.voiceStd[$scope.locationCount] = 495;
            $scope.proposalData.dID[$scope.locationCount] = 9.99;
            $scope.proposalData.bFCapDiscount[$scope.locationCount] = 0;
        }
        if (form_type=='voice_essentials_cap'){
            $scope.proposalData.ipMidbandPlansVoice[$scope.locationCount] = $scope.ip_midband_plans[0];
            $scope.proposalData.ipMidbandDownloadVoice[$scope.locationCount] = $scope.download_plans[0];
            $scope.proposalData.voiceEssentialChannel[$scope.locationCount] = 4;
            $scope.proposalData.voiceEssentialDID[$scope.locationCount] = 10;
            $scope.proposalData.bECapDiscount[$scope.locationCount] = 0;
            $scope.proposalData.ipMidbandDis[$scope.locationCount] = 0;
            // $scope.proposalData.ipMidbandDownloadVoice[$scope.locationCount] = 0;            
        } 
        if (form_type=='data_adsl'){
            // $scope.proposalData.adsl2Plans[$scope.locationCount] = 79;
            $scope.proposalData.adsl2Plans[$scope.locationCount] = $scope.adsl_plans[0];
            $scope.proposalData.adsl2Dis[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompAnalougeDSL[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompAnalougeDisDSL[$scope.locationCount] = 0;
        }
        if (form_type=='ip_midband'){
            $scope.proposalData.ipMidbandDownload[$scope.locationCount] = $scope.download_plans[1];
            $scope.proposalData.ipMidbandPlans[$scope.locationCount] = $scope.ip_midband_plans[1];
            $scope.proposalData.ipMidbandDis[$scope.locationCount] = 0;
        }
        if (form_type=='ip_midbandunli'){
            $scope.proposalData.ipMidbandUnliPlans[$scope.locationCount] = $scope.ip_midbandunli_plans[0];
            $scope.proposalData.ipMidbandUnliProf[$scope.locationCount] = $scope.professional_install[0];
            $scope.proposalData.ipMidbandUnliDis[$scope.locationCount] = 0;
        }
        if (form_type=='1300_discounted'){
            $scope.proposalData.rate131300Dis.qt1800[$scope.locationCount] = 0;
            $scope.proposalData.rate131300Dis.qt1300[$scope.locationCount] = 0;
            $scope.proposalData.rateDis131300.discount[$scope.locationCount] = 0;
        }
        if (form_type=='1300'){
            $scope.proposalData.rate131300.qt1800[$scope.locationCount] = 0;
            $scope.proposalData.rate131300.qt1300[$scope.locationCount] = 0;
            $scope.proposalData.rate131300.qt13[$scope.locationCount] = 0;
        }
        if (form_type=='mobile_wireless'){
            $scope.proposalData.mobileWirelessPlans[$scope.locationCount] = $scope.wireless_cap_plans[0];
            // $scope.proposalData.mobileWirelessPlans[$scope.locationCount] = 15;
            $scope.proposalData.mobileWirelessDis[$scope.locationCount] = 0;
        }        
        if (form_type=='fibre'){
            $scope.proposalData.fibreUtPlans[$scope.locationCount] = $scope.fibre_unli_plans[0];
            $scope.proposalData.fibreDis[$scope.locationCount] = 0;
        }
        // if (form_type=='adsl'){
        //     $scope.proposalData.adsl2Plans[$scope.locationCount] = $scope.adsl_plans[0];
        // }
        if (form_type=='ethernet'){
            $scope.proposalData.ethernetPlans[$scope.locationCount] = $scope.ethernet_plans[0];
            $scope.proposalData.ethernetDis[$scope.locationCount] = 0;
        }
        if (form_type=='nbnUnlimited'){
            $scope.proposalData.UnlimitedPlans[$scope.locationCount] = $scope.nbnUnlimited_plans[0];
            $scope.proposalData.nbnUnlimitedDis[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompAnalougeDisNBNUnli[$scope.locationCount] = 0;
            $scope.proposalData.voiceCompAnalougeNBNUnli[$scope.locationCount] =0;
        }
        if (form_type=='telstraUntimed'){
            $scope.proposalData.telstraUntimedPlans[$scope.locationCount] = $scope.telstraUntimed_plans[0];
        }
        if (form_type=='telstraWireless'){
            $scope.proposalData.telstraWirelessPlans[$scope.locationCount] = $scope.telstraWireless_plans[0];
            $scope.proposalData.telstraWirelessDis[$scope.locationCount] = 0;
        }
        $scope.items[form_type].push($scope.locationCount);
    };

    $scope.reset = function(){
        $scope.proposalData={};
        $scope.proposalData.tradingName = "";
        $scope.proposalData.contactPerson = "";
        $scope.proposalData.businessName = "";
        $scope.proposalData.emailAddress = "";
        $scope.proposalData.schQty = [];
        $scope.proposalData.schOption = [];
        $scope.proposalData.untimed = [];
        $scope.proposalData.megaRate = [];
        $scope.proposalData.telstra = [];
        $scope.proposalData.term = 60;
        $scope.proposalData.billingType = 'rental';
        $scope.schItemCount = 0;  
        $scope.proposalData.schItems = [{count:$scope.schItemCount}];
        $scope.proposalData.schOptions = [{count:$scope.schItemCount}];
        $scope.proposalData.dID = [];
        $scope.proposalData.dID[0] = 0;
        $scope.proposalData.voiceStd = [];
        $scope.proposalData.voiceStd[0] = 0;
        $scope.proposalData.voiceStdDID = [];
        $scope.proposalData.voiceStdDID[0] = 0;
        $scope.proposalData.voiceStdChannel = [];
        $scope.proposalData.voiceStdChannel[0] = 0;
        $scope.proposalData.bFCapDiscount = [];
        $scope.proposalData.bFCapDiscount[0] = 0;
        $scope.proposalData.bFStdDiscount = [];
        $scope.proposalData.bFStdDiscount[0] = 0;
        $scope.proposalData.bFCallDiscount = [];
        $scope.proposalData.bFCallDiscount[0] = 0;
        $scope.proposalData.voiceUntimedChannel = [];
        $scope.proposalData.voiceUntimedChannel[0] = 0;
        $scope.proposalData.voiceUntimedDID = [];
        $scope.proposalData.voiceUntimedDID[0] = 0;
        $scope.proposalData.bFUntimedDiscount = [];
        $scope.proposalData.bFUntimedDiscount[0] = 0;
        $scope.proposalData.voiceCompDID = [];
        $scope.proposalData.voiceCompDID[0] = 0;    
        $scope.proposalData.voiceSolutionChannel = [];
        $scope.proposalData.voiceSolutionChannel[0] = 0;  
        $scope.proposalData.voiceSolutionDID = [];
        $scope.proposalData.voiceSolutionDID[0] = 0;  
        $scope.proposalData.bFSolutionDiscount = [];
        $scope.proposalData.bFSolutionDiscount[0] = 0;  
        $scope.proposalData.bFSolutionCallDiscount = [];
        $scope.proposalData.bFSolutionCallDiscount[0] = 0;  
        $scope.proposalData.voiceCompAnalougeDisNBNUnli = [];
        $scope.proposalData.voiceCompAnalougeDisNBNUnli[0] = 0;
        $scope.proposalData.voiceEssentialChannel = [];
        $scope.proposalData.voiceEssentialChannel[0] = 0; 
        $scope.proposalData.voiceEssentialDID = [];
        $scope.proposalData.voiceEssentialDID[0] = 0;  
        $scope.proposalData.bECapDiscount = [];
        $scope.proposalData.bECapDiscount[0] = 0; 
        $scope.proposalData.ipMidbandDis = [];
        $scope.proposalData.ipMidbandDis[0] = 0; 
        $scope.proposalData.voiceCompAnalougeDis = [];
        $scope.proposalData.voiceCompAnalougeDis[0] = 0;
        $scope.proposalData.voiceCompBriDis = [];
        $scope.proposalData.voiceCompBriDis[0] = 0;
        $scope.proposalData.voiceCompPri = [];
        $scope.proposalData.voiceCompPri[0] = 0;
        $scope.proposalData.voiceCompPriDis = [];
        $scope.proposalData.voiceCompPriDis[0] = 0;
        $scope.proposalData.voiceCompCallDis = [];
        $scope.proposalData.voiceCompCallDis[0] = 0;
        $scope.proposalData.voiceCompAnalougeDisDSL = [];
        $scope.proposalData.voiceCompAnalougeDisDSL[0] = 0;
        $scope.proposalData.UnlimitedPlans = [];
        $scope.proposalData.UnlimitedPlans[0] = 0;
        $scope.proposalData.mobileWirelessDis = [];
        $scope.proposalData.mobileWirelessDis[0] = 0;
        $scope.proposalData.nbnUnlimitedDis = [];
        $scope.proposalData.nbnUnlimitedDis[0] = 0;
        $scope.proposalData.voiceCompAnalougeNBNUnli = [];
        $scope.proposalData.voiceCompAnalougeNBNUnli[0] = 0;
        $scope.proposalData.ipMidbandUnliDis = [];
        $scope.proposalData.ipMidbandUnliDis[0] = 0;
        $scope.proposalData.ethernetDis = [];
        $scope.proposalData.ethernetDis[0] = 0;
        $scope.proposalData.fibreDis = [];
        $scope.proposalData.fibreDis[0] = 0;
        $scope.proposalData.telstraUntimedDis = 0;
        $scope.proposalData.mobileUtDis = 0;
        $scope.proposalData.mobileCapDis = 0;
        $scope.proposalData.telstraWirelessDis = [];
        $scope.proposalData.telstraWirelessDis[0] = 0;
        $scope.proposalData.rate131300 = {};
        $scope.proposalData.rate131300.qt1800 = [];
        $scope.proposalData.rate131300.qt1800[0] = 0;
        $scope.proposalData.rate131300.qt1300 = [];
        $scope.proposalData.rate131300.qt1300[0] = 0;
        $scope.proposalData.rate131300.qt13 = [];
        $scope.proposalData.rate131300.qt13[0] = 0;
        $scope.proposalData.rate131300Dis = {};
        $scope.proposalData.rate131300Dis.qt1800 = [];
        $scope.proposalData.rate131300Dis.qt1800[0] = 0;
        $scope.proposalData.rate131300Dis.qt1300 = [];
        $scope.proposalData.rate131300Dis.qt1300[0] = 0;
        $scope.proposalData.rateDis131300 = {};
        $scope.proposalData.rateDis131300.discount = [];
        $scope.proposalData.rateDis131300.discount[0] = 0;
        $scope.proposalData.siteType = {};
        $scope.proposalData.leaser = 0;
        $scope.proposalData.fa = 0;
        $scope.proposalData.siteType = "Existing Site";
        $scope.proposalData.adsl2Dis = [];
        $scope.proposalData.adsl2Dis[0] = 0;
        $scope.proposalData.comLocation = {};
        $scope.proposalData.comLocation = {
            'nbf_cap'               : [],
            'nbf_std'               : [],
            'voice_comp'              : [],
            'voice_solution_untimed'  : [],
            'voice_essentials_cap'    : [],
            'voice_solution_standard' : [],
            'data_adsl'               : [],
            'ip_midband'              : [],
            'ip_midbandunli'          : [],
            'nbn'                     : [],
            'nbnUnlimited'            : [],
            'mobile_mega'             : [],
            'mobile_ut'               : [],
            'mobile_wireless'         : [],
            '1300'                    : [],
            '1300_discounted'         : [],
            'fibre'                   : [],
            'ethernet'                : [],
            'telstraUntimed'          : [],
            'telstraWireless'         : [],
            'printer'                 : []
        };
        $scope.proposalData.voiceFaxToEmail = {};
        $scope.proposalData.voiceFaxQty = {};
        $scope.proposalData.voiceMobility = {};
        $scope.proposalData.voiceMobQty = {};

        $scope.proposalData.voiceStdFaxToEmail = {};
        $scope.proposalData.voiceStdFaxQty = {};
        $scope.proposalData.voiceStdMobility = {};
        $scope.proposalData.voiceStdMobQty = {};

        $scope.proposalData.voiceCompAnalouge = {};
        $scope.proposalData.voiceCompBri = {};
        $scope.proposalData.voiceCompFaxToEmail = {};
        $scope.proposalData.voiceCompFaxQty = {};
        $scope.proposalData.voiceCompMobility = {};
        $scope.proposalData.voiceCompMobQty = {};
        // $scope.proposalData.voiceCompDID = {};
        // $scope.proposalData.voiceCompAnalougeDis = {};
        // $scope.proposalData.voiceCompBriDis = {};
        // $scope.proposalData.voiceCompPriDis = {};
        //$scope.proposalData.voiceCompPri = {};

        $scope.proposalData.voiceUntimedFaxToEmail = {};
        $scope.proposalData.voiceUntimedFaxQty = {};
        $scope.proposalData.voiceUntimedMobility = {};
        $scope.proposalData.voiceUntimedMobQty = {};

        $scope.proposalData.voiceSolutionFaxToEmail = {};
        $scope.proposalData.voiceSolutionFaxQty = {};
        $scope.proposalData.voiceSolutionMobility = {};
        $scope.proposalData.voiceSolutionMobQty = {};

        $scope.proposalData.ipMidbandDownload = {
            0:$scope.download_plans[1]
        };

        $scope.proposalData.alarmPlans = $scope.recurring_services[0];

        //console.log($scope.proposalData.alarmPlans);

        $scope.proposalData.transactionDetails = "Commercial Premises";
        $scope.proposalData.hardwareInstalled = "MPM (multi path monitoring device)";
        $scope.proposalData.monitoredServices = "Burglary";
        $scope.proposalData.panelType = "NESS";

        $scope.proposalData.adsl2Plans = {
            0:$scope.adsl_plans[0]
        }

        $scope.proposalData.telstraUntimedPlans = {
            0:$scope.telstraUntimed_plans[0]
        }

        $scope.proposalData.telstraWirelessPlans = {
            0:$scope.telstraWireless_plans[0]
        }

        $scope.proposalData.ethernetPlans = {
            0:$scope.ethernet_plans[0]
        }

        $scope.proposalData.UnlimitedPlans = {
            0:$scope.nbnUnlimited_plans[0]
        }

        $scope.proposalData.ipMidbandPlans = {
            0:$scope.ip_midband_plans[1]
        };

        $scope.proposalData.ipMidbandUnliPlans = {
            0:$scope.ip_midbandunli_plans[0]
        };

        $scope.proposalData.ipMidbandUnliProf = {
            0:$scope.professional_install[0]
        };

        $scope.proposalData.ipMidbandDownloadVoice = {
            0:$scope.download_plans[0]
        };

        $scope.proposalData.ipMidbandPlansVoice = {
            0:$scope.ip_midband_plans[0]
        };

        $scope.proposalData.mobileUtPlans = {
            0:$scope.mobile_4G_Untimed_Calls[0]
        };

        $scope.proposalData.fibreUtPlans = {
            0:$scope.fibre_unli_plans[0]
        };

        $scope.proposalData.mobileWirelessPlans = {
            0:$scope.wireless_cap_plans[0]
        };

        $scope.proposalData.voiceCompAnalougeDSL = {
            0:0
        };

        $scope.proposalData.voiceCompAnalougeNBNMonthly = {
            0:0
        };

        $scope.proposalData.voiceCompAnalougeNBNUnli = {
            0:0
        };

        $scope.proposalData.voiceCompAnalouge = {
            0:0
        };

        $scope.proposalData.voiceCompBri = {
            0:0
        };

        $scope.proposalData.qt1300new = {};
        $scope.proposalData.qt800new = {};
        $scope.proposalData.qt13new = {};

        
    }

    $scope.reset();

    $scope.tinymceOptions = {
      selector: "textarea",
      theme: "modern",
      paste_data_images: true,
      plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern"
      ],
      toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
      toolbar2: "print preview media | forecolor backcolor emoticons "
    };

    $scope.removeOptionsBlock = function(form_type, index) {
        $scope.items[form_type].splice(index, 1);
    };

    $scope.setValue = function() {
        $scope.proposalData.tradingName = "Value Initialize";
        $scope.proposalData.contactPerson = "Sky Pillar";
        $scope.proposalData.currentHardware = "currentHardware to Show";
        $scope.proposalData.currentServices = "currentServices to Show";
        $scope.proposalData.abn = "01234567891";
        $scope.proposalData.suburb = "suburb to Show";
        $scope.proposalData.address = "Address to Show";
        $scope.proposalData.emailAddress = "emailtocontact@gmail.com";
    }

    $scope.array = [];
    $scope.array_ = angular.copy($scope.array);

    var uploader = $scope.uploader = new FileUploader({
        url: 'proposal/upload'
    });

    var dataPaths;
    $scope.hiders = [{
        id: 1,
        title: "No"
    }, {
        id: 2,
        title: "Yes"
    }]

    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        dataPaths = response.FilePath
        $scope.proposalData.files = dataPaths; 
    };

    if ($stateParams.id) {
        Proposals.get({
            id: $stateParams.id
        }, function(data) {
            $scope.proposalData = data;
            $scope.array = data.selectedFeedbacks;
            $scope.editId= data.id;
            // alert($scope.array);
        });
    } 
    // $.each ($rootScope.rates,function(index,value){
    //         subrate = (rental_per_month/value.rate)*1000; 
    //         //rates.push(subrate);
    //         if($scope.checkRange(subrate,value.lowerLimit,value.upperLimit)){
    //           financeAmt = subrate;
    //         }
    //         console.log
    //     });
    $scope.proposalData.fa = 0;
    $scope.getCurrentDate = function(){
        var currentDate = new Date();
        var month =  new Array();
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";   
        return month[currentDate.getMonth()]+"-"+currentDate.getDate()+"-"+currentDate.getFullYear();
    }
    
    $scope.dollarSign = function(val){
        var strCurrency = '';
        if (angular.isDefined(val))
            strCurrency = '$' + val;
        //alert(strCurrency);
        return strCurrency;
    }
    
    $scope.computeFinanceAmmount = function(){
      $scope.proposalData.fa = 0;
      angular.forEach($scope.proposalData.schOption, function(value, key) {
          // $scope.proposalData.fa = parseInt($scope.proposalData.fa.replace(/[^\d.]/g,''));
          $scope.proposalData.fa += ($scope.proposalData.schQty[key] * value.price); 
      });
      // $scope.proposalData.fa = $scope.dollarSign($scope.proposalData.fa);
      // $scope.proposalData.fa = parseInt($scope.proposalData.fa.replace(/[^\d.]/g,''));
    }

    

    $scope.counter = function(){
         $scope.proposalData.term = $('#term').val();
         $scope.proposalData.billingType = $('#type').val();
         // $scope.proposalData.fa = parseInt($scope.proposalData.fa);
         var rates = Form.getRates({term:$scope.proposalData.term,
                                    type:$scope.proposalData.billingType
                                    }, function(data) {
            if (data.success) {
                $scope.rates = data.rates;
            }
            //console.log($scope.rates);
            if($scope.proposalData.billingType == 'rental'){
                // $scope.proposalData.fa = parseInt($scope.proposalData.fa);
              angular.forEach($scope.rates, function(value, key){
                       //console.log('Rental:' + key + ': ' + value.upperLimit + ':' + value.lowerLimit + ':' + value.rate + $scope.proposalData.fa);
                       if($scope.checkRange($scope.proposalData.fa,value.lowerLimit,value.upperLimit)){
                          $scope.proposalData.leaser = ($scope.proposalData.fa/1000)*value.rate;
                          $scope.proposalData.leaser = parseInt($scope.proposalData.leaser);
                         // $scope.leaser = $scope.dollarSign($scope.leaser);
                     }
                 });
            }
            else if($scope.proposalData.billingType == 'leasing'){
              angular.forEach($scope.rates, function(value, key){
                       //console.log(key + ': ' + value.upperLimit + ':' + value.lowerLimit + ':' + value.rate + $scope.proposalData.fa);
                       if($scope.checkRange($scope.proposalData.fa,value.lowerLimit,value.upperLimit)){
                          // $scope.proposalData.fa = parseInt($scope.proposalData.fa.replace(/[^\d.]/g,''));
                          $scope.proposalData.leaser = ($scope.proposalData.fa/1000)*value.rate;
                          $scope.proposalData.leaser = parseInt($scope.proposalData.leaser);
                         // $scope.leaser = $scope.dollarSign($scope.leaser);
                     }
                 });
            } else {
              $scope.proposalData.leaser = $scope.proposalData.fa;
            }
        });
            
  }
   $scope.checkRange = function(val,lower_limit,upper_limit){
        //alert(val + '-' + lower_limit + '-' + upper_limit);
        if (upper_limit==0){
            if (val>=lower_limit){
                return true;
            } else {
                return false;
            }
        } else {
            if (val >= lower_limit && val <= upper_limit){
                return true;
            } else {
                return false;
            }
        }
    }
    
    $scope.calculateDiscount = function(currentVal,plan, planOptionId, completeOptions,optionId) {
        var payment = 0;
        var capTotalVal =0;
        //console.log(planOptionId);
        if (planOptionId != '') {
            var planStr = $("#"+planOptionId+" option:selected").text();
            var planVal =parseInt(planStr.match(/\$(\d+)/)[1]);
        }
        currentVal = parseInt(currentVal);
        //alert(currentVal);
        if(angular.isDefined($scope.proposalData.leaser)) {
            payment = parseInt($scope.proposalData.leaser);
        }
        //alert(completeOptions);
        var c_options = angular.isDefined(completeOptions)?completeOptions:0;
        //alert(c_options);
        $(".main-disc").each(function() {
            capTotalVal += parseInt($(this).val());
        });
        console.log("Current Val : " + currentVal + " | Cap Total Val : " + capTotalVal + " | Plan Value : " + planVal);
        if (plan == 'voice-cap' || plan == 'data-plan' || plan == 'mobile-plan' ) {
            if (currentVal > planVal) {
                alert("Discount cannot exceed monthly payment");
                //$('#' + optionId).val('');
            } else {
                if(capTotalVal  == currentVal) {
                    if (currentVal> planVal/2) {
                        var disc = planVal/2;
                        alert("Maximum Discount should not go over $"+disc);
                        //$('#' + optionId).val('');
                    }
                } else {
                    var remaining = payment-(capTotalVal-currentVal);
                    if (remaining<0) {
                        remaining = 0;
                    }

                    var disc = remaining + planVal*30/100;
                    if (currentVal > disc) {
                        alert("Maximum Discount should not go over $"+disc);
                        //$('#' + optionId).val('');
                    }
                }
            }
        } else if (plan == 'standard-plan') {
            if (currentVal>100) {
                alert("Maximum Discount should not go over $100");
               // $('#' + optionId).val('');
            }
        }  else if (plan == 'complete-bri') {
            if (currentVal>50*c_options) {
                alert("Maximum Discount should not go over $"+50*c_options);
                //$('#' + optionId).val('');
            }
        } else if (plan == 'complete-pri') {
            if (currentVal>100*c_options) {
                alert("Maximum Discount should not go over $"+100*c_options);
               // $('#' + optionId).val('');
            }
        } else if (plan == 'complete-analog') {
            if (currentVal>(10*c_options)) {
                alert("Maximum Discount should not go over $" + 10*c_options);
                //$('#' + optionId).val('');
            }
        } else if (plan == 'complete-call' || plan == '1300-plan' || plan == 'standard-call') {
            if (currentVal> payment) {
                alert("Maximum Discount should not go over monthly payment");
                //$('#' + optionId).val('');
            }
        } else if (plan == 'unlimited-plan') {
            if (currentVal> planVal) {
                alert("Maximum Discount should not go over monthly payment");
                //$('#' + optionId).val('');
            } else {
                if (currentVal> planVal/2) {
                    var disc = planVal/2;
                    alert("Maximum Discount should not go over $"+disc);
                   // $('#' + optionId).val('');
                }
            }
        }
    }

    $scope.prepDiscounts = function(payment) {
      payment = payment.replace(/^\D+|\,+/g, '');
       $scope.EqPayment = payment;
      }

    $scope.showOptions = function(form) {
        // alert($scope.proposalData.type.voiceCap);
        $(".optionBox").hide();
        $("#"+form+"Option").show();
        $(".form-label label").css('color','black');
        $("#"+form+"Label label").css('color','green');
        $scope.locationCount = 0;
    }

    $scope.saveAsDrafted = function(proposalData) {

        Proposals.save({files:proposalData,
                        user : $scope.user_id,                    
                        editId : $scope.editId }, function(data) {
                        $scope.editId = data.editId;
        });
    }
    
    
    $scope.telephone = function(item){
        if(item.category === 'Telephone System'){
            return item;
        }
    };
    $scope.printer = function(item){
        if(item.category === 'MULTI-FUNCTION PRINTERS'){
            return item;
        }
    };
    $scope.camera = function(item){
        if(item.category === 'Camera'){
            return item;
        }
    };

    $scope.roundVal = function(val) {
        return val.toFixed(2);
    }

    $scope.downloadPdf = function() {
        var userProfile =  Scopes.get('DefaultCtrl').profile;
        var formHtmlData = new Array();
        var fileNameArr = new Array();
        //$interval.cancel(saveDraft);

        var pdf = ProposalActions.createPdf({
            htmlData :formHtmlData,
            // userDetails : { name : $scope.proposalData.tradingName,
            //                 email :$scope.proposalData.emailAddress},
            // account: {name: userProfile.name, email: userProfile.email}
          },
            function(data) {
              // alert(data);
              console.log(data.success);
              if($rootScope.editId) {
                 var editId = $rootScope.editId;
                 $rootScope.editId = null;
              }
            ProposalActions.save({ data : $scope.proposalData,
                        user : userProfile.id,
                        editId : editId,
                        fileName : fileNameArr,
                        type:"Completed"}, function(data) {
                if (data.success) {
                    $scope.proposalData = {};
                    // $("#loading").hide();
                    $scope.loading = false;
                    // FlashMessage.setMessage({
                    // success:true,
                    // message:"The forms will be sent to your email shortly."});
                    alert("The forms will be sent to your email shortly.");                    
                    $window.location.reload();
                }
            });
        });
    }
    
    $scope.processCreditCard = function (){
      
    }

    $scope.signDocument = function(templateName, formData) {
        var userProfile =  Scopes.get('DefaultCtrl').profile;
        console.log(userProfile);
        var datum = $scope.proposalData;
        datum.tradingName = datum.tradingName.toUpperCase();
        console.log($scope.proposalData.schOption);
        var datePrepared = new Date();
        datum.datePrepared = datePrepared.toDateString();
        datum.preparedBy = userProfile.name;
        datum.preparedEmail = userProfile.email;
        datum.preparePhone = userProfile.phone;
        $scope.template = "templates/proposal/"+templateName+".html?t=" + _version;        
        $rootScope.formData = formData;

        ngDialog.open({
            data: angular.toJson({
                proposalData: datum
            }),
            template: $scope.template,
            plan: true,
            controller : 'ProposalCtrl',
            width: 900,
            height: 600,
            scope: $scope,
            className: 'ngdialog-theme-default',
            preCloseCallback : function(value) {
                var allSigned = true;

                $(".signatureImg").each(function() {
                    console.log($(this).attr('id'),$(this).children("img").length);
                    if ($(this).children("img").length == 0) {
                        allSigned = false;
                        return;
                    }
                });
                if (!allSigned) {
                   $('#isSigned').val('F');
                   if (confirm("You have not signed all the fields.Do you want to exit")) {
                       return true;
                   }
                   return false;
                    $timeout(angular.noop());
                } else {
                    $('#isSigned').val('T');
                    // var button = $('#formData').find('a[href="#finish"]');
                    // button.show();
                    //alert($('#isSigned').val());                  // In case of forms with location fields.
                    if ($scope.locationAvailableTemps.indexOf(templateName) !== -1 && $scope.items[templateName].length > 1) {
                        $scope.htmlData[templateName] = [];
                        angular.forEach($scope.items[templateName], function(item, index) {
                            var clone = $('.ngdialog-content').clone();
                            clone.find('[class*="preview"]').each(function(divIndex,data){
                                //console.log(divIndex,data,index);  
                                if (index!=divIndex)  {
                                    clone.find('.preview-' + divIndex).remove();
                                    // console.log(clone.html());
                                    console.log($scope.items[templateName]);
                                }                        
                            });
                            //clone.find('.preview-' + index).remove();
                            var tableHtml = clone.html();
                            $scope.htmlData[templateName].push(tableHtml);
                        });
                    } else {
                        $scope.htmlData[templateName] = $(".ngdialog-content").html();
                    }

                    $scope[templateName] = true;
                    $timeout(angular.noop());
                }
            }
            });
    }

    $scope.showProposal = function() {
      var userProfile =  Scopes.get('DefaultCtrl').profile;
      //console.log(userProfile);
      var datum = $scope.proposalData;
      datum.tradingName = datum.tradingName.toUpperCase();
      // console.log($scope.proposalData.schOption);
      var datePrepared = new Date();
      datum.datePrepared = datePrepared.toDateString();
      datum.preparedBy = userProfile.name;
      datum.preparedEmail = userProfile.email;
      datum.preparePhone = userProfile.phone;
      console.log(datum);
      $scope.template = "templates/proposal/proposal.html";
      ngDialog.open({
            data: angular.toJson({
              proposalData: datum
          }),
           template: $scope.template,
           plan: true,
           controller : 'ProposalCtrl',
           width: '22cm',
           className: 'ngdialog-theme-default',
           showClose: true,
           closeByEscape: true,
           closeByDocument: true
       });
    }
     $scope.showQuotation = function() {
      var userProfile =  Scopes.get('DefaultCtrl').profile;
      console.log(userProfile);
      var datum = $scope.proposalData;
      datum.tradingName = datum.tradingName.toUpperCase();
      console.log($scope.proposalData.schOption);
      var datePrepared = new Date();
      datum.datePrepared = datePrepared.toDateString();
      datum.preparedBy = userProfile.name;
      datum.preparedEmail = userProfile.email;
      datum.preparePhone = userProfile.phone;
      // datum.totalMonthly 
      // datum.totalSog
      console.log(datum);
      $scope.template = "templates/proposal/quotation.html";
      ngDialog.open({
            data: angular.toJson({
              proposalData: datum
          }),
           template: $scope.template,
           plan: true,
           controller : 'ProposalCtrl',
           width: '22cm',
           className: 'ngdialog-theme-default',
           showClose: true,
           closeByEscape: true,
           closeByDocument: true
       });
    }
    $scope.sign = function(id) {
       $("#testDi").dialog("open");
       $('#iAccept').attr('data-id', id);
       if (!$('#'+id).find('canvas').length) {
            $('#signatureBox').html("<span>Sign here!</span>&nbsp;&nbsp;<i class='fa fa-refresh'onclick=clearSg('signatureBox')></i>");
            $('#signatureBox').jSignature({
                color:"#000000",lineWidth:1,
                    width :490, height :98,
                    cssclass : "signature-canvas",
                   "decor-color": 'transparent'
                });
            clicked = true;
        }
    }

    $scope.sched_goods = ScheduleGoods.query();
    $scope.true = 'true';
    
    $scope.prepDiscounts = function(fa) {
      fa = fa.replace(/^\D+|\,+/g, '');
       $scope.EqPayment = fa;
    }

    $scope.dollarSign = function(val){
        var strCurrency = '';
        if (angular.isDefined(val))
            strCurrency = '$' + val.toFixed(2);
        //alert(strCurrency);
        return strCurrency;
    }


    $scope.getTotal = function(){
        $scope.proposalData.sum=0;
        $scope.proposalData.landlineNumRow = 1;
        // NBF CAP
      if (angular.isDefined($scope.proposalData.type)){
        if($scope.proposalData.type.voiceCap == true){
          angular.forEach($scope.proposalData.voiceStd,function(value,key){
                $scope.proposalData.sum+=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.voiceFaxQty,function(value,key){                
                if (parseFloat(value)>0){
                  $scope.proposalData.sum+=parseFloat(value * 9.95);
                  $scope.proposalData.landlineNumRow++;
                }
          });
          angular.forEach($scope.proposalData.dID,function(value,key){
                $scope.proposalData.sum+=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.bFCapDiscount,function(value,key){
                 if (parseFloat(value)>0){
                  $scope.proposalData.sum-=parseFloat(value);
                  $scope.proposalData.landlineNumRow++;
                }
          });
        }
        // Focus Standard
        if($scope.proposalData.type.focusStandard == true){
          angular.forEach($scope.proposalData.voiceStdChannel,function(value,key){
                $scope.proposalData.sum+=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.voiceStdDID,function(value,key){
                $scope.proposalData.sum+=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.voiceStdFaxQty,function(value,key){
                if (parseFloat(value)>0 && $scope.proposalData.voiceFaxToEmail[key]==true){
                  $scope.proposalData.sum+=parseFloat(value * 9.95);
                  $scope.proposalData.landlineNumRow++;
                }
          });
          angular.forEach($scope.proposalData.bFCallDiscount,function(value,key){
                $scope.proposalData.sum-=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.bFStdDiscount,function(value,key){
                $scope.proposalData.sum-=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
          });
        }
        // Complete Office
        if($scope.proposalData.type.completeOffice == true){
          angular.forEach($scope.proposalData.voiceCompAnalouge,function(value,key){
                $scope.proposalData.sum+=parseFloat(value * 39.95);
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.voiceCompBri,function(value,key){
                $scope.proposalData.sum+=parseFloat(value * 99.85);
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.voiceCompPri,function(value,key){
                  $scope.proposalData.sum+=parseFloat(value); 
                  $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.voiceCompDID,function(value,key){
                $scope.proposalData.sum+=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.voiceCompFaxQty,function(value,key){
                $scope.proposalData.sum+=parseFloat(value * 9.95);
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.voiceCompAnalougeDis,function(value,key){
                $scope.proposalData.sum-=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.voiceCompBriDis,function(value,key){
                $scope.proposalData.sum-=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.voiceCompPriDis,function(value,key){
                $scope.proposalData.sum-=parseFloat(value); 
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.voiceCompCallDis,function(value,key){
                $scope.proposalData.sum-=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
          });
        }
        // SOLUTION CAP
        if($scope.proposalData.type.voiceUt == true){
          angular.forEach($scope.proposalData.voiceUntimedChannel,function(value,key){
                $scope.proposalData.sum+=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.voiceUntimedFaxQty,function(value,key){
                $scope.proposalData.sum+=parseFloat(value * 9.95);
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.voiceUntimedDID,function(value,key){
                $scope.proposalData.sum+=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.bFUntimedDiscount,function(value,key){
                $scope.proposalData.sum-=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
          });
        }
        // Solution Standard 
        if($scope.proposalData.type.voiceSolution == true){
          angular.forEach($scope.proposalData.voiceSolutionChannel,function(value,key){
                $scope.proposalData.sum+=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.voiceSolutionDID,function(value,key){
                $scope.proposalData.sum+=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.voiceSolutionFaxQty,function(value,key){
                $scope.proposalData.sum+=parseFloat(value * 9.95);
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.bFSolutionDiscount,function(value,key){
                $scope.proposalData.sum-=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.bFSolutionCallDiscount,function(value,key){
                $scope.proposalData.sum-=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
          });
        }
        // Essential Cap
        if($scope.proposalData.type.voiceEssential == true){
          angular.forEach($scope.proposalData.voiceEssentialChannel,function(value,key){
                $scope.proposalData.sum+=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.voiceEssentialDID,function(value,key){
                $scope.proposalData.sum+=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.bECapDiscount,function(value,key){
                $scope.proposalData.sum-=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.ipMidbandPlansVoice,function(value,key){
                $scope.proposalData.sum+=parseFloat(value.price);
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.ipMidbandDownloadVoice,function(value,key){
                $scope.proposalData.sum+=parseFloat(value.price);
                $scope.proposalData.landlineNumRow++;
          });
          angular.forEach($scope.proposalData.ipMidbandDis,function(value,key){
                $scope.proposalData.sum-=parseFloat(value);
                $scope.proposalData.landlineNumRow++;
          });
        }
        // ADSL
        if($scope.proposalData.type.adsl2 == true){
          angular.forEach($scope.proposalData.adsl2Plans,function(value,key){
                $scope.proposalData.sum+=parseFloat(value.price);
          });
          angular.forEach($scope.proposalData.adsl2Dis,function(value,key){
                $scope.proposalData.sum-=parseFloat(value);
          });
          angular.forEach($scope.proposalData.voiceCompAnalougeDSL,function(value,key){
                $scope.proposalData.sum+=parseFloat(value * 39.95);
          });
          angular.forEach($scope.proposalData.voiceCompAnalougeDisDSL,function(value,key){
                $scope.proposalData.sum-=parseFloat(value);
          });
        }
        // NBN UNLIMITED
        if($scope.proposalData.type.nbnUnlimitedPlans == true){
          angular.forEach($scope.proposalData.UnlimitedPlans,function(value,key){
                $scope.proposalData.sum+=parseFloat(value.price);
          });
          angular.forEach($scope.proposalData.voiceCompAnalougeNBNUnli,function(value,key){
                $scope.proposalData.sum+=parseFloat(value * 39.95);
          });
          angular.forEach($scope.proposalData.nbnUnlimitedDis,function(value,key){
                $scope.proposalData.sum-=parseFloat(value);
          });
          angular.forEach($scope.proposalData.voiceCompAnalougeDisNBNUnli,function(value,key){
                $scope.proposalData.sum-=parseFloat(value);
          });
        } 
        // Midband Monthly
        if($scope.proposalData.type.ipMidband == true){
          angular.forEach($scope.proposalData.ipMidbandPlans,function(value,key){
                $scope.proposalData.sum+=parseFloat(value.price);
          });
          angular.forEach($scope.proposalData.ipMidbandDownload,function(value,key){
                $scope.proposalData.sum+=parseFloat(value.price);
          });
          angular.forEach($scope.proposalData.ipMidbandDis,function(value,key){
                $scope.proposalData.sum-=parseFloat(value);
          });
        }
        // Midband Unlimited
        if($scope.proposalData.type.ipMidbandUnli == true){
          angular.forEach($scope.proposalData.ipMidbandUnliPlans,function(value,key){
                $scope.proposalData.sum+=parseFloat(value.price);
          });
          angular.forEach($scope.proposalData.ipMidbandUnliDis,function(value,key){
                $scope.proposalData.sum-=parseFloat(value);
          });
        }
        // Ehternet Unlimited(EFM)
        if($scope.proposalData.type.ethernet == true){
          angular.forEach($scope.proposalData.ethernetPlans,function(value,key){
                $scope.proposalData.sum+=parseFloat(value.price);
          });
          angular.forEach($scope.proposalData.ethernetDis,function(value,key){
                $scope.proposalData.sum-=parseFloat(value);
          });
        }
        // Fibre
        if($scope.proposalData.type.fibre == true){
          angular.forEach($scope.proposalData.fibreUtPlans,function(value,key){
                $scope.proposalData.sum+=parseFloat(value.price);
          });
          angular.forEach($scope.proposalData.fibreDis,function(value,key){
                $scope.proposalData.sum-=parseFloat(value);
          });
        }
        // Telstra 4G
        if($scope.proposalData.type.telstraUntimed == true){
          angular.forEach($scope.proposalData.telstra,function(value,key){
                if(value == 49){
                  value = 49;
                  $scope.proposalData.sum+=parseFloat(value);
                }
                if(value == 69){
                  value = 69;
                  $scope.proposalData.sum+=parseFloat(value);
                }
                if(value == 79){
                  value = 79;
                  $scope.proposalData.sum+=parseFloat(value);
                }
                if(value == 89){
                  value = 89;
                  $scope.proposalData.sum+=parseFloat(value);
                }
          });
          angular.forEach($scope.proposalData.boltOn,function(value,key){
                if(value == 9.89){
                  value = 9.89;
                  $scope.proposalData.sum+=parseFloat(value);
                }
                if(value == 31.99){
                  value = 31.99;
                  $scope.proposalData.sum+=parseFloat(value);
                }
          });
          $scope.proposalData.sum-=parseFloat($scope.proposalData.telstraUntimedDis);
        }
        // Telstra Wireless
        if($scope.proposalData.type.telstraWireless == true){
          angular.forEach($scope.proposalData.telstraWirelessPlans,function(value,key){
                $scope.proposalData.sum+=parseFloat(value.price);
          });
          angular.forEach($scope.proposalData.telstraWirelessDis,function(value,key){
                $scope.proposalData.sum-=parseFloat(value);
          });
        }
        // Mega Cap
        if($scope.proposalData.type.mobileCap == true){
          angular.forEach($scope.proposalData.megaRate,function(value,key){
                if(value == 39){
                  value = 39;
                  $scope.proposalData.sum+=parseFloat(value);
                }
                if(value == 49){
                  value = 49;
                  $scope.proposalData.sum+=parseFloat(value);
                }
                if(value == 59){
                  value = 59;
                  $scope.proposalData.sum+=parseFloat(value);
                }
          });
          $scope.proposalData.sum-=parseFloat($scope.proposalData.mobileCapDis);
        } 
        // Mega Untime
        if($scope.proposalData.type.mobileUt == true){
          angular.forEach($scope.proposalData.untimed,function(value,key){
                if(value == 49){
                  value = 49;
                  $scope.proposalData.sum+=parseFloat(value);
                }
                if(value == 59){
                  value = 59;
                  $scope.proposalData.sum+=parseFloat(value);
                }
                if(value == 79){
                  value = 79;
                  $scope.proposalData.sum+=parseFloat(value);
                }
                if(value == 89){
                  value = 89;
                  $scope.proposalData.sum+=parseFloat(value);
                }
          });
          $scope.proposalData.sum-=parseFloat($scope.proposalData.mobileUtDis);
        }  
        // angular.forEach($scope.proposalData.dID,function(value,key){
        //       $scope.proposalData.sum+=parseFloat(value);
        // });
        // voiceStdangular.forEach($scope.proposalData.dID,function(value,key){
        //       $scope.proposalData.sum+=parseFloat(value);
        // });
        // angular.forEach($scope.proposalData.dID,function(value,key){
        //       $scope.proposalData.sum+=parseFloat(value);
        // });
        // angular.forEach($scope.proposalData.dID,function(value,key){
        //       $scope.proposalData.sum+=parseFloat(value);
        // });
        // angular.forEach($scope.proposalData.dID,function(value,key){
        //       $scope.proposalData.sum+=parseFloat(value);
        // });
        // angular.forEach($scope.proposalData.dID,function(value,key){
        //       $scope.proposalData.sum+=parseFloat(value);
        // });
      }
      $scope.proposalData.sum = $scope.proposalData.sum.toFixed(2); 
      console.log($scope.proposalData.landlineNumRow);
    };

    $scope.addRentalGoodOption =  function() {
      console.log($scope.schItemCount);
      $scope.schItemCount++;
      console.log($scope.schItemCount);
      $scope.proposalData.schItems.push({count:$scope.schItemCount});
      $scope.proposalData.schOptions.push({count:$scope.schItemCount});
      console.log($scope.proposalData.schItems);
    };

    $scope.removeRentalGoodOption = function(index) {
        console.log(index);
        console.log($scope.proposalData.schItems);
        $scope.proposalData.schItems.splice(index,1);
        console.log($scope.proposalData.schItems);
        // var spliceIndex = $scope.proposalData.schItems.indexOf({count:index});
        // var spliceIndexes = $scope.proposalData.schOption.indexOf({count:index});
        // var spliceIndexeses = $scope.proposalData.schQty.indexOf({count:index});
        // $scope.proposalData.schItems.splice(spliceIndex,1);
        // $scope.proposalData.schOption.splice(spliceIndexes,1);
        // $scope.proposalData.schQty.splice(spliceIndexeses,1);
        // console.log("Items",$scope.proposalData.schItems,"count",$scope.schItemCount,"option",$scope.proposalData.schQty);
        // console.log("index",spliceIndex);
    };

    $scope.transformTelFields  = function() {
        $('.tel').on("keyup", function()
        {
                var val = this.value.replace(/[^0-9]/ig, '');
                if (val.length >10) {
                    val = val.substr(0,10);
                }
                var newVal = '';
                if (val.length >2) {
                  newVal +='('+val.substr(0,2)+') ';
                  val = val.substr(2);
                }
                while (val.length > 4)   {
                  newVal += val.substr(0, 4) +'-';
                  val = val.substr(4);
                }
                newVal += val;
                this.value = newVal;
        });
        $('.telRange').on("keyup",function(){
            var val = this.value.replace(/[^0-9]/ig, '');
            if (val.length>4) {
                val = val.substr(0,4);
            }
            this.value = val;            
        });
    }

    $scope.transformTelFields();
    
    $scope.changeColTransparent = function(val) {
        console.log(val);
        if ((val.item == '_') || (val.plan=='_')) {
            $('.col-sel-desc .chosen-container').addClass('transparent');
        } else {
            $('.col-sel-desc .chosen-container').removeClass('transparent');
        }

        $scope.telstraPlans = [];
        $scope.mobileMegaPlans = [];
        $scope.mobile4GUntimedPlans = [];
        //console.log($scope.configMobPort);
        if (angular.isDefined(val.plan)){
            angular.forEach($scope.configMobPort,function(item,index){
                //console.log(item);
                if (proposalData.telstra=="Telstra 4G"){
                    if (angular.isUndefined($scope.telstraPlans[proposalData.telstra.itemIndex]))
                        $scope.telstraPlans[proposalData.telstra.itemIndex]=1;
                    else
                        $scope.telstraPlans[proposalData.telstra.itemIndex]++;
                } else if(proposalData.megaRate=="Mobile Mega"){
                    if (angular.isUndefined($scope.mobileMegaPlans[proposalData.telstra.itemIndex]))
                        $scope.mobileMegaPlans[proposalData.telstra.itemIndex]=1;
                    else
                        $scope.mobileMegaPlans[val.itemIndex]++;
                    proposalData.boltOn = {};
                } else if(proposalData.untimed=="Mobile 4G Untimed"){
                    if (angular.isUndefined($scope.mobile4GUntimedPlans[proposalData.telstra.itemIndex]))
                        $scope.mobile4GUntimedPlans[proposalData.telstra.itemIndex]=1;
                    else
                        $scope.mobile4GUntimedPlans[proposalData.telstra.itemIndex]++;
                    // proposalData.boltOn = {};
                }
            });
            // console.log($scope.telstraPlans);
            // console.log($scope.mobileMegaPlans);
            // console.log($scope.mobile4GUntimedPlans);
            // $scope.processBoltOnCount();
            //console.log($scope.mobileMegaPlans);
        }        
    }
    // $scope.processBoltOnCount = function(){
    //     $scope.boltOnPlans = [];
    //     angular.forEach($scope.configMobPort,function(item,index){
    //         //console.log(item.boltOn.itemIndex);
    //         if (angular.isDefined(proposalData.boltOn.itemIndex)){
    //             if (angular.isUndefined($scope.boltOnPlans[proposalData.boltOn.itemIndex])){
    //                 $scope.boltOnPlans[proposalData.boltOn.itemIndex]=1;    
    //             } else {
    //                 $scope.boltOnPlans[proposalData.boltOn.itemIndex]++;
    //             }
    //         }
    //     });
    // }
});



Qms.controller('ProposalListCtrl', function($scope,$rootScope, Scopes, $filter, $http, $state, Proposals, FlashMessage, $window, $timeout,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder,ngDialog){
    $scope.vmpr = {};
        $scope.vmpr.dtInstance = {};   
        $scope.vmpr.dtOptions = DTOptionsBuilder.newOptions()
                          .withOption('paging', true)
                          .withOption('searching', true)
                          .withOption('info', true)
                          .withOption('order', [[0, 'desc']])
                          .withOption('lengthMenu',[5,10,15,20,50,100,200,500])
    $scope.proposals = Proposals.query();
    $scope.views = function() {       
        $scope.template = "templates/proposal/proposal-view.html";
        ngDialog.open({
             template: $scope.template,
             plan: true,
             controller : 'ProposalListCtrl',
             width: 960,
             height: 720,
             scope: $scope,
             className: 'ngdialog-theme-default',
             showClose: true,
             closeByEscape: true,
             closeByDocument: true
             });
    }

    $scope.edit = function(row_id) {
        var x=window.confirm("Do you want to Modify this?")
        if (x)
        $state.go('edit_proposal', {
            id: row_id
        });
        }
  // $scope.edit = function(id) {
  //     var x=window.confirm("Do you want to Modify this?")
  //       if (x)
  //       var data = Form.show({id : id}, function(data) {
  //           angular.forEach(data.formData,function(name,index){
  //               //console.log(name.comLocation['schedule_of_goods'].length);
  //               if (index==data){
  //                   //alert(index);
  //                   angular.forEach(name,function(val,index){
  //                       if (index=='voiceEssentialChannel'){
  //                           //alert(angular.isArray(name));
  //                           if (!angular.isArray(name)){
  //                               var key = val;
  //                               delete val;
  //                               name.voiceEssentialChannel = [{'voiceEssentialChannel-0' : key}];
  //                           }
  //                       }
  //                       if (index=='voiceEssentialDID'){
  //                           //alert(angular.isArray(name));
  //                           if (!angular.isArray(val)){
  //                               var key = val;
  //                               delete val;
  //                               name.voiceEssentialDID = [{'voiceEssentialDID-0' : key}];
  //                           }
  //                       }
  //                   });
  //               } 
  //           })
  //           $rootScope.viewFormData = data.formData;
  //          //console.log($rootScope.viewFormData);
  //           $state.go('forms');
  //       });
  //   }

    $scope.create = function(row_id) {
          // alert(row_id);
          // $state.go('forms');
          $state.go('forms', {
            id: row_id
        });
        //   Proposals.show({id : id},function(data) {
        //   $scope.proposalData = data.data;
        //     $state.go('proposal');
        // });
      }

    $scope.delete = function(row_id) {
      var x=window.confirm("Do you want to Delete this?")
        if (x)
        Proposals.delete({
            id: row_id
        }, function(data) {
            FlashMessage.setMessage(data);
            if (data.success == true)
                $scope.proposals = Proposals.query();
        });
    }
});


Qms.controller("FormDataCtrl", function($scope,$stateParams,Proposals, $rootScope, $interval, $window, $anchorScroll, $location, $timeout, $http, Scopes, Form, Rates, FlashMessage, ngDialog) {
     
    $scope.scheduleCount = 0;
    $scope.locationCount = 0;
    $scope.accountLocation = 0;
    $scope.addTelCount = 0;
    $scope.addMobPortCount = 0;
    $scope.htmlData = {};
    $scope.discountArr = {};
    $scope.companyLocations = [];
    $rootScope.draftMode = false;

    $scope.locationAvailableTemps = [
        'nbf_cap',
        'nbf_std',
        'voice_comp',
        'voice_solution_untimed',
        'voice_essentials_cap',
        'voice_solution_standard',
        'data_adsl',
        'ip_midband',
        'ip_midbandunli',
        'nbnUnlimited',
        'nbn',
        'mobile_mega',
        'mobile_ut',
        'mobile_wireless',
        '1300',
        '1300_discounted',
        'fibre',
        'ethernet',
        'telstraUntimed',
        'telstraWireless',
        'printer',
        'schedule_of_goods'
    ];

    $scope.items = {
        'nbf_cap'               : [0],
        'nbf_std'               : [0],
        'voice_comp'            : [0],
        'voice_solution_untimed'  : [0],
        'voice_essentials_cap'    : [0],
        'voice_solution_standard' : [0],
        'data_adsl'               : [0],
        'ip_midband'              : [0],
        'ip_midbandunli'          : [0],
        'nbn'                     : [0],
        'nbnUnlimited'            : [0],
        'mobile_mega'             : [0],
        'mobile_ut'               : [0],
        'mobile_wireless'         : [0],
        '1300'                    : [0],
        '1300_discounted'         : [0],
        'fibre'                   : [0],
        'ethernet'                : [0],
        'telstraUntimed'          : [0],
        'telstraWireless'         : [0],
        'printer'                 : [0],
        'schedule_of_goods'       : [0]
    };

    $scope.recurring_services = [
        {'description' : 'Monitoring Centre connect 24 hours','price' : 59.00},
        {'description' : "Monitoring Centre connect 24 hours (Including NBN/3G Wireless Unit)",'price' : 89}
    ];

    $scope.ethernet_plans = [
        {'description' : 'Up to 10Mbps/10Mbps* Unlimited','price':499},
        {'description' : 'Up to 20Mbps/20Mbps* Unlimited','price':599},
    ];

    $scope.nbnUnlimited_plans = [
        {'description' : 'NBN Standard 25','inclusion' : 'Download speeds Between 5Mbps and 25Mbps*<br/>Upload speeds Between 1Mbps and 5Mbps*','price':159},
        {'description' : 'NBN Fast 50','inclusion' : 'Download speeds Between 12Mbps and 50Mbps*<br/>Upload speeds Between 1Mbps and 20Mbps*','price':179},
        {'description' : 'NBN Ultra fast 100','inclusion' : 'Download speeds Between 12Mbps and 100Mbps*<br/>Upload speeds Between 1Mbps and 40Mbps*','price':189}
    ];

    $scope.mobile_mega_plans = [
        {'description' : 'Cap 39','inclusions' : '$250 inc. calls 500MB','price':39},
        {'description' : 'Cap 49','inclusions' : '$2500 inc. calls 2GB','price':49},
        {'description' : 'Cap 59','inclusions' : '$2500 inc. calls 3GB','price':59},
    ];
    $scope.data_bolt_on_plans = [
        {'description' : 'Data Bolt-on 1GB','memory': '1GB','price':9.89,'itemIndex':0},
        {'description' : 'Data Bolt-on 5GB','memory': '5GB','price':31.99,'itemIndex':1}
    ];
    
    $scope.mobile_rate_plans = [
        {'plan' : 'Select a Mobile Plan','inclusions':''},
        {'group':'Telstra 4G','plan':'Telstra 4G Lite','inclusions':'$1000 National Calls 1GB Data','price':49,'itemIndex':0},
        {'group':'Telstra 4G','plan':'Telstra 4G Business Executive','inclusions':'Unlimited Calls 3GB Data','price':69,'itemIndex':1},
        {'group':'Telstra 4G','plan':'Telstra 4G Business Professional','inclusions':'Unlimited Calls 10GB Data','price':79,'itemIndex':2},
        {'group':'Telstra 4G','plan':'Telstra 4G Business Maximum','inclusions':'Unlimited Calls 20GB Data','price':89,'itemIndex':3},
        // {'group':'Mobile Mega','plan':'Mobile Mega Cap 39','inclusions':'$250 inc. calls 500Mb','price':39,'itemIndex':0},
        // {'group':'Mobile Mega','plan':'Mobile Mega Cap 49','inclusions':'$2500 inc. calls 2GB','price':49,'itemIndex':1},
        // {'group':'Mobile Mega','plan':'Mobile Mega Cap 59','inclusions':'$2500 inc. calls 3GB','price':59,'itemIndex':2},
        {'group':'Mobile 4G Untimed','plan':'Moblie 4G Untimed 49','inclusions':'Includes 700MB','price':49,'itemIndex':0},
        {'group':'Mobile 4G Untimed','plan':'Moblie 4G Untimed 59','inclusions':'Includes 2GB','price':59,'itemIndex':1},
        {'group':'Mobile 4G Untimed','plan':'Moblie 4G Untimed 79','inclusions':'Includes 4GB and 300 INTERNATIONAL MINS','price':79,'itemIndex':2},
        {'group':'Mobile 4G Untimed','plan':'Moblie 4G Untimed 89','inclusions':'Includes 6GB and 300 INTERNATIONAL MINS','price':89,'itemIndex':3}
    ];

    $scope.configMobPort = [
        {'confAccHolder':'','confMobNo':'','confProvider':'','confAccNo':'','confPatPlan':$scope.mobile_rate_plans[0],'boltOn':{}}
    ];

    $scope.adsl_plans = [
        {'description' : 'ADSL 2 + LITE','details' : '(Unlimited Lite (no static IP))','price':79},
        {'description' : 'ADSL 2 + Unlimited','details' : 'Business Grade (ON NET)','price':109},
        {'description' : 'ADSL 2 + 100GB','details' : 'Business Grade (OFF NET)','price':109},
        {'description' : 'ADSL 2 + 200GB','details' : 'Business Grade (OFF NET)','price':129},
        {'description' : 'ADSL 2 + 300GB','details' : 'Business Grade (OFF NET)','price':139},
        {'description' : 'ADSL 2 + 500GB','details' : 'Business Grade (OFF NET)','price':149},
        {'description' : 'ADSL 2 + 1TB','details' : 'Business Grade (OFF NET)','price':169},
    ];

    $scope.telstraUntimed_plans = [
        {'description' : 'Lite','inclusions' : 'INCLUDES $1000 National Calls,INCLUDES 1GB,UNLIMITED NATIONAL SMS,$0.69 PER MMS','price':49},
        {'description' : 'Business Executive','inclusions' : 'UNLIMITED NATIONAL CALLS**,INCLUDES 3GB,UNLIMITED NATIONAL SMS,UNLIMITED NATIONAL MMS,100 INTERNATIONAL MINS*','price':69},
        {'description' : 'Business Professional','inclusions' : 'UNLIMITED NATIONAL CALLS**,INCLUDES 10GB,UNLIMITED NATIONAL SMS,UNLIMITED NATIONAL MMS,300 INTERNATIONAL MINS*','price':79},
        {'description' : 'Business Maximum','inclusions' : 'UNLIMITED NATIONAL CALLS**,INCLUDES 20GB,UNLIMITED NATIONAL SMS,UNLIMITED NATIONAL MMS,300 INTERNATIONAL MINS*','price':89},
    ];


    $scope.telstraWireless_plans = [
        {'description' : 'Broadband Wireless 3GB','price':43},
        {'description' : 'Broadband Wireless 6GB','price':64},
        {'description' : 'Broadband Wireless 10GB','price':93}
    ];

    $scope.ip_midbandunli_plans = [
        {'description' : '10Mbps/10Mbps*','price':549},
        {'description' : '20Mbps/20Mbps*','price':699},
        {'description' : '40Mbps/40Mbps*','price':999}
    ];

    $scope.ip_midband_plans = [
        {'description' : 'None','price':0},
        {'description' : '6Mbps/6Mbps*','price':319},
        {'description' : '8Mbps/8Mbps*','price':349},
        {'description' : '18Mbps/18Mbps*','price':399},
        {'description' : '20Mbps/20Mbps*','price':449},
        {'description' : '30Mbps/30Mbps*','price':799},
        {'description' : '40Mbps/40Mbps*','price':849}
    ];

    $scope.professional_install = [
        {'description' : 'ASDL 2+ 20/1Mbps','price':399},
        {'description' : 'ASDL 2+ 20/3 to 40Mbps Midband','price':599},
    ];

    $scope.download_plans = [
        {'description' : 'None','price':0},
        {'description' : '50 GB','price':19},
        {'description' : '100 GB','price':35},
        {'description' : '200 GB','price':69},
        {'description' : '300 GB','price':99},
        {'description' : '500 GB','price':149},
        {'description' : '1000 GB','price':289}
    ];

    $scope.mobile_4G_Untimed_Calls = [
        {'description' : '4G Untimed 49','inclusions' : 'Includes 700MB','price':49},
        {'description' : '4G Untimed 59','inclusions' : 'Includes 2GB','price':59},
        {'description' : '4G Untimed 79','inclusions' : 'Includes 4GB,300 INTERNATIONAL MINS*','price':79},
        {'description' : '4G Untimed 89','inclusions' : 'Includes 6GB,300 INTERNATIONAL MINS*','price':89}
    ];

    $scope.wireless_cap_plans = [
        {'description' : 'Wireless 15 - 500MB','price':15},
        {'description' : 'Wireless 19 - 1GB','price':19},
        {'description' : 'Wireless 29 - 2GB','price':29},
        {'description' : 'Wireless 39 - 3GB','price':39},  
        {'description' : 'Wireless 44 - 4GB','price':44},
        {'description' : 'Wireless 49 - 6GB','price':49},
        {'description' : 'Wireless 59 - 8GB','price':59},
        {'description' : 'Wireless 119 - 12GB','price':119}
    ];

    $scope.fibre_unli_plans = [
        {'description' : '10Mbps / 10Mbps Unlimited - 36 month term','price':599},
        {'description' : '20Mbps / 20Mbps Unlimited - 36 month term','price':699},
        {'description' : '100Mbps / 100Mbps Unlimited - 36 month term','price':1199},
        {'description' : '400Mbps / 400Mbps Unlimited - 36 month term','price':758},
        {'description' : '500Mbps / 500Mbps Unlimited - 36 month term','price':1599},
        {'description' : '1000Mbps / 1000Mbps Unlimited - 24 month term','price':1899},
        {'description' : '1000Mbps / 1000Mbps Unlimited - 36 month term','price':1699},
        {'description' : '1000Mbps / 1000Mbps Unlimited - 48 month term','price':1299},
    ];

    $http.get("profile").success(function(response) {
        $scope.profile = response;
        $rootScope.profile = response;
    });
    
    $scope.dollarSign = function(val){
        var strCurrency = '';
        if (angular.isDefined(val))
            strCurrency = '$' + val;
        //alert(strCurrency);
        return strCurrency;
    }

    $scope.isEmptyArray = function(arr) {
        return angular.equals([], arr);
    }

    $scope.reset = function() {
        $scope.formData = {};
        $scope.formData.fibreTerm = [];
        $scope.formData.fibreTerm[0] = 36;
        $scope.formData.schItems = [];
        $scope.formData.schItems[0] = [{count:1}];
        $scope.schItemCount = [];
        $scope.schItemCount[0] = 1;
        $scope.formData.aBillingTerm = 60;
        $scope.formData.aTerm = 60;
        $scope.formData.schAddItems = [];
        $scope.formData.schAddItems[0] = [{count:1}];
        $scope.schAddItemCount = [];
        $scope.schAddItemCount[0] = 1;
        $scope.formData.cPosition = "";
        $scope.formData.cSPosition = "";
        $scope.formData.aIdenQuestion = "";
        $scope.formData.referals = [0,1,2,3,4];
        $scope.formData.rBusinessName = [null,null,null,null,null];
        $scope.formData.rContactPerson = [null,null,null,null,null];
        $scope.formData.rPhoneNumber = [null,null,null,null,null];
        $scope.referalCount=5;
        $scope.formData.dID = [];
        $scope.formData.dID[0] = 10;
        $scope.formData.voiceStd = [];
        $scope.formData.voiceStd[0] = 5;
        $scope.formData.voiceStdDID = [];
        $scope.formData.voiceStdDID[0] = 10;
        $scope.formData.voiceStdChannel = [];
        $scope.formData.voiceStdChannel[0] = 5;
        $scope.formData.bFCapDiscount = [];
        $scope.formData.bFCapDiscount[0] = 0;
        $scope.formData.bFStdDiscount = [];
        $scope.formData.bFStdDiscount[0] = 0;
        $scope.formData.bFCallDiscount = [];
        $scope.formData.bFCallDiscount[0] = 0;
        $scope.formData.voiceUntimedChannel = [];
        $scope.formData.voiceUntimedChannel[0] = 3;
        $scope.formData.voiceUntimedDID = [];
        $scope.formData.voiceUntimedDID[0] = 10;
        $scope.formData.bFUntimedDiscount = [];
        $scope.formData.bFUntimedDiscount[0] = 0;
        $scope.formData.voiceCompDID = [];
        $scope.formData.voiceCompDID[0] = 0;    
        $scope.formData.voiceSolutionChannel = [];
        $scope.formData.voiceSolutionChannel[0] = 3;  
        $scope.formData.voiceSolutionDID = [];
        $scope.formData.voiceSolutionDID[0] = 10;  
        $scope.formData.bFSolutionDiscount = [];
        $scope.formData.bFSolutionDiscount[0] = 0;  
        $scope.formData.bFSolutionCallDiscount = [];
        $scope.formData.bFSolutionCallDiscount[0] = 0;  
        $scope.formData.voiceCompAnalougeDisNBNUnli = [];
        $scope.formData.voiceCompAnalougeDisNBNUnli[0] = 0;
        $scope.formData.voiceEssentialChannel = [];
        $scope.formData.voiceEssentialChannel[0] = 4; 
        $scope.formData.voiceEssentialDID = [];
        $scope.formData.voiceEssentialDID[0] = 10;  
        $scope.formData.bECapDiscount = [];
        $scope.formData.bECapDiscount[0] = 0; 
        $scope.formData.ipMidbandDis = [];
        $scope.formData.ipMidbandDis[0] = 0; 
        $scope.formData.voiceCompAnalougeDis = [];
        $scope.formData.voiceCompAnalougeDis[0] = 0;
        $scope.formData.voiceCompBriDis = [];
        $scope.formData.voiceCompBriDis[0] = 0;
        $scope.formData.voiceCompPri = [];
        $scope.formData.voiceCompPri[0] = 0;
        $scope.formData.voiceCompPriDis = [];
        $scope.formData.voiceCompPriDis[0] = 0;
        $scope.formData.voiceCompCallDis = [];
        $scope.formData.voiceCompCallDis[0] = 0;
        $scope.formData.voiceCompAnalougeDisDSL = [];
        $scope.formData.voiceCompAnalougeDisDSL[0] = 0;
        $scope.formData.UnlimitedPlans = [];
        $scope.formData.UnlimitedPlans[0] = 159;
        $scope.formData.mobileWirelessDis = [];
        $scope.formData.mobileWirelessDis[0] = 0;
        $scope.formData.nbnUnlimitedDis = [];
        $scope.formData.nbnUnlimitedDis[0] = 0;
        $scope.formData.voiceCompAnalougeNBNUnli = [];
        $scope.formData.voiceCompAnalougeNBNUnli[0] = 0;
        $scope.formData.ipMidbandUnliDis = [];
        $scope.formData.ipMidbandUnliDis[0] = 0;
        $scope.formData.ethernetDis = [];
        $scope.formData.ethernetDis[0] = 0;
        $scope.formData.fibreDis = [];
        $scope.formData.fibreDis[0] = 0;
        $scope.formData.telstraUntimedDis = 0;
        $scope.formData.mobileUtDis = 0;
        $scope.formData.mobileCapDis = 0;
        $scope.formData.telstraWirelessDis = [];
        $scope.formData.telstraWirelessDis[0] = 0;
        $scope.formData.rate131300 = {};
        $scope.formData.rate131300.qt1800 = [];
        $scope.formData.rate131300.qt1800[0] = 0;
        $scope.formData.rate131300.qt1300 = [];
        $scope.formData.rate131300.qt1300[0] = 0;
        $scope.formData.rate131300.qt13 = [];
        $scope.formData.rate131300.qt13[0] = 0;
        $scope.formData.rate131300Dis = {};
        $scope.formData.rate131300Dis.qt1800 = [];
        $scope.formData.rate131300Dis.qt1800[0] = 0;
        $scope.formData.rate131300Dis.qt1300 = [];
        $scope.formData.rate131300Dis.qt1300[0] = 0;
        $scope.formData.rateDis131300 = {};
        $scope.formData.rateDis131300.discount = [];
        $scope.formData.rateDis131300.discount[0] = 0;
        $scope.formData.siteType = {};
        $scope.formData.siteType = "Existing Site";
        $scope.formData.adsl2Dis = [];
        $scope.formData.adsl2Dis[0] = 0;
        $scope.formData.accounts = [];
        $scope.formData.accounts = [
            {'configCarrier':'','configAccno':''}
        ];

        $scope.formData.comLocation = {};
        $scope.formData.comLocation = {
            'nbf_cap'               : [],
            'nbf_std'               : [],
            'voice_comp'              : [],
            'voice_solution_untimed'  : [],
            'voice_essentials_cap'    : [],
            'voice_solution_standard' : [],
            'data_adsl'               : [],
            'ip_midband'              : [],
            'ip_midbandunli'          : [],
            'nbn'                     : [],
            'nbnUnlimited'            : [],
            'mobile_mega'             : [],
            'mobile_ut'               : [],
            'mobile_wireless'         : [],
            '1300'                    : [],
            '1300_discounted'         : [],
            'fibre'                   : [],
            'ethernet'                : [],
            'telstraUntimed'          : [],
            'telstraWireless'         : [],
            'printer'                 : [],
            'schedule_of_goods'       : []
        };

        $scope.formData.comSuburb = {};
        $scope.formData.comSuburb = {
            'nbf_cap'               : [],
            'nbf_std'               : [],
            'voice_comp'              : [],
            'voice_solution_untimed'  : [],
            'voice_essentials_cap'    : [],
            'voice_solution_standard' : [],
            'data_adsl'               : [],
            'ip_midband'              : [],
            'ip_midbandunli'          : [],
            'nbn'                     : [],
            'nbnUnlimited'            : [],
            'mobile_mega'             : [],
            'mobile_ut'               : [],
            'mobile_wireless'         : [],
            '1300'                    : [],
            '1300_discounted'         : [],
            'fibre'                   : [],
            'ethernet'                : [],
            'telstraUntimed'          : [],
            'telstraWireless'         : [],
            'printer'                 : [],
            'schedule_of_goods'       : []
        };

        $scope.formData.comZipCode = {};
        $scope.formData.comZipCode = {
            'nbf_cap'               : [],
            'nbf_std'               : [],
            'voice_comp'              : [],
            'voice_solution_untimed'  : [],
            'voice_essentials_cap'    : [],
            'voice_solution_standard' : [],
            'data_adsl'               : [],
            'ip_midband'              : [],
            'ip_midbandunli'          : [],
            'nbn'                     : [],
            'nbnUnlimited'            : [],
            'mobile_mega'             : [],
            'mobile_ut'               : [],
            'mobile_wireless'         : [],
            '1300'                    : [],
            '1300_discounted'         : [],
            'fibre'                   : [],
            'ethernet'                : [],
            'telstraUntimed'          : [],
            'telstraWireless'         : [],
            'printer'                 : [],
            'schedule_of_goods'       : []       
        };

        $scope.formData.voiceFaxToEmail = {};
        $scope.formData.voiceFaxQty = {};
        $scope.formData.voiceMobility = {};
        $scope.formData.voiceMobQty = {};

        $scope.formData.voiceStdFaxToEmail = {};
        $scope.formData.voiceStdFaxQty = {};
        $scope.formData.voiceStdMobility = {};
        $scope.formData.voiceStdMobQty = {};

        $scope.formData.voiceCompAnalouge = {};
        $scope.formData.voiceCompBri = {};
        $scope.formData.voiceCompFaxToEmail = {};
        $scope.formData.voiceCompFaxQty = {};
        $scope.formData.voiceCompMobility = {};
        $scope.formData.voiceCompMobQty = {};
        // $scope.formData.voiceCompDID = {};
        // $scope.formData.voiceCompAnalougeDis = {};
        // $scope.formData.voiceCompBriDis = {};
        // $scope.formData.voiceCompPriDis = {};
        //$scope.formData.voiceCompPri = {};

        $scope.formData.voiceUntimedFaxToEmail = {};
        $scope.formData.voiceUntimedFaxQty = {};
        $scope.formData.voiceUntimedMobility = {};
        $scope.formData.voiceUntimedMobQty = {};

        $scope.formData.voiceSolutionFaxToEmail = {};
        $scope.formData.voiceSolutionFaxQty = {};
        $scope.formData.voiceSolutionMobility = {};
        $scope.formData.voiceSolutionMobQty = {};

        $scope.formData.ipMidbandDownload = {
            0:$scope.download_plans[1]
        };

        $scope.formData.alarmPlans = $scope.recurring_services[0];

        //console.log($scope.formData.alarmPlans);

        $scope.formData.transactionDetails = "Commercial Premises";
        $scope.formData.hardwareInstalled = "MPM (multi path monitoring device)";
        $scope.formData.monitoredServices = "Burglary";
        $scope.formData.panelType = "NESS";

        $scope.formData.adsl2Plans = {
            0:$scope.adsl_plans[0]
        }

        $scope.formData.telstraUntimedPlans = {
            0:$scope.telstraUntimed_plans[0]
        }

        $scope.formData.telstraWirelessPlans = {
            0:$scope.telstraWireless_plans[0]
        }

        $scope.formData.ethernetPlans = {
            0:$scope.ethernet_plans[0]
        }

        $scope.formData.UnlimitedPlans = {
            0:$scope.nbnUnlimited_plans[0]
        }

        $scope.formData.ipMidbandPlans = {
            0:$scope.ip_midband_plans[1]
        };

        $scope.formData.ipMidbandUnliPlans = {
            0:$scope.ip_midbandunli_plans[0]
        };

        $scope.formData.ipMidbandUnliProf = {
            0:$scope.professional_install[0]
        };

        $scope.formData.ipMidbandDownloadVoice = {
            0:$scope.download_plans[0]
        };

        $scope.formData.ipMidbandPlansVoice = {
            0:$scope.ip_midband_plans[0]
        };

        $scope.formData.mobileUtPlans = {
            0:$scope.mobile_4G_Untimed_Calls[0]
        };

        $scope.formData.fibreUtPlans = {
            0:$scope.fibre_unli_plans[0]
        };

        $scope.formData.mobileWirelessPlans = {
            0:$scope.wireless_cap_plans[0]
        };

        $scope.formData.voiceCompAnalougeDSL = {
            0:0
        };

        $scope.formData.voiceCompAnalougeNBNMonthly = {
            0:0
        };

        $scope.formData.voiceCompAnalougeNBNUnli = {
            0:0
        };

        $scope.formData.voiceCompAnalouge = {
            0:0
        };

        $scope.formData.voiceCompBri = {
            0:0
        };

        $scope.formData.qt1300new = {};
        $scope.formData.qt800new = {};
        $scope.formData.qt13new = {};
        $scope.formData.aCardExpiry = '';
        $scope.formData.rates = [];
        $scope.rates = [];
    };

    $scope.reset();

    $scope.tinymceOptions = {
      selector: "textarea",
      theme: "modern",
      paste_data_images: true,
      plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern"
      ],
      toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
      toolbar2: "print preview media | forecolor backcolor emoticons "
    };
    
    //console.log($stateParams.id);
    // alert($stateParams.id);
    // if ($stateParams.id) {
    //     Proposals.get({
    //         id: $stateParams.id
    //     }, function(data) {
    //         $scope.formData.cName = data.companyName;
    //         $scope.formData.aTerm = data.aTerm;
    //         $scope.formData.dID[0] = data.diD;
    //         $scope.formData.cEmail = data.emailAddress;
    //         $scope.formData.cTrading = data.businessName;
    //         $scope.formData.cTel = data.contactNumber;
    //         $scope.formData.schOption = data.schOption;
    //         $scope.formData.schItems = data.schItems;
    //         // alert($scope.formData);
    //         Form.save({data:$scope.formData, user : user.id, fileName : '',
    //                 editId : $scope.editId, type:"draft"}, function(data) {
    //                     $scope.editId = data.editId;
    //         });
    //     });
    // }
        // $scope.formData.cEmail = "info@somelongcompany.com.au";
        // $scope.formData.dName = "Arthur Conan Doyle";
        // $scope.formData.cPosition = "";
        // $scope.formData.dSName = "Sherlock Holmes";
        // $scope.formData.cSPosition = "";
        // $scope.formData.cContact = "Arthur Conan Doyle";
        // $scope.formData.cTel = "(02) 4342-1232";
        // $scope.formData.cFax = "(02) 4342-1232";
        // $scope.formData.cMobile = "4532-533-267";
        // $scope.formData.cWebsite = "http://somelongcompany.com.au";
        // $scope.formData.aAccountant = "James Moriarty";
        // $scope.formData.aIndustry = "PROFESSIONAL OFFICE";
        // $scope.formData.aTradeRef = "A Study in Scarlet";
        // $scope.formData.aCPerson = "Dr. Watson";
        // $scope.formData.aTel = "(02) 2340-5444";
        // $scope.formData.pName = "Sherlock Holmes";
        // $scope.formData.pAddress = "220B Baker Street";
        // $scope.formData.pSuburb = "London";
        // $scope.formData.pPostcode = 2000;
        // $scope.formData.pPhone = "(02) 4342-1232";
        // $scope.formData.pLicense = "2332323243A";
        // $scope.formData.pValue = "$ 600,000";
        // $scope.formData.pMortgage = "$ 400,000";
        // $scope.formData.pOwned = true;
        // $scope.formData.tTel = "(02) 4342-1232";
        // $scope.formData.aContact = "Mrs. Hudson";
        // $scope.formData.tContact = "Mr. Hudson";
        // $scope.formData.aNemp = "1000";
        // $scope.formData.aInsurer = "Insurance Company";
        // $scope.formData.aPolicy = "A/127/2016";

    $scope.getRates = function(){
      $scope.formData.term = $('#term').val();
      var rates = Form.getRates({term:$scope.formData.term}, function(data) {
            if (data.success) {
            // $scope.formData.terms = data;
           }
           // console.log($scope.formData.terms);

    });
  }

    $scope.changeFibreTerm = function (fibrePlan,index){
      //console.log(fibrePlan);
      switch (fibrePlan.price){
        case 599 :
        case 699 : 
        case 1199 : 
        case 758 :
        case 1599 :
        case 1699 : $scope.formData.fibreTerm[index] = 36;
                    break;
        case 1899 : $scope.formData.fibreTerm[index] = 24;
                    break;
        case 1299 : $scope.formData.fibreTerm[index] = 48;
                    break;
      }
      console.log($scope.formData);
    };

    $scope.addNewReferal = function() {
        $scope.formData.referals.push($scope.referalCount++);
    };  
        
    $scope.removeReferal = function(index) {
        $scope.formData.referals.splice(index,1);
        $scope.formData.rBusinessName.splice(index,1);
        $scope.formData.rContactPerson.splice(index,1);
        $scope.formData.rPhoneNumber.splice(index,1);
    };

    // $scope.draftMode = function (val){
    //     $rootScope.isDraftMode = val;
    //     $scope.closeThisDialog();
    // }

    $scope.updateCardExpiry = function (year,month){
        var monthNum=month + "";
        while (monthNum.length < 2)
            monthNum = "0" + monthNum;
        $scope.formData.aCardExpiry = monthNum + '/' + year;
        //alert($scope.formData.aCardExpiry);
    }

    // $scope.preSignDocument = function() {
    //     $scope.template = "templates/forms/pre_sign.html";
    //     ngDialog.open({
    //         template: $scope.template,
    //         plan: true,
    //         controller : 'FormDataCtrl',
    //         width: 500,
    //         height: 500,
    //         scope: $scope,
    //         className: 'ngdialog-theme-default presign',
    //         showClose: true,
    //         closeByEscape: true,
    //         closeByDocument: true,
    //         preCloseCallback : function(value) {
    //             var signData = $('img.imported').attr('src');
    //             if ((!signData) && (!$rootScope.isDraftMode)) {
    //                 if (alert("You have not signed yet. Please sign to start filling out data. You can also select Draft Mode to be able to start.")) {
    //                     return true;
    //                 }
    //                 return false;
    //                 $timeout(angular.noop());
    //             }
    //         }
    //     });
    // }


    $scope.sendAsDraft = function(){
        var userProfile =  Scopes.get('DefaultCtrl').profile;
        var formHtmlData = new Array();
        var fileNameArr = new Array();
        //$interval.cancel(saveDraft);

        if (!angular.isDefined($scope.htmlData) || angular.equals({},$scope.htmlData)) {
          FlashMessage.setMessage({success:false, message:"No Forms Selected"});
          return;
        }
        $("#loading").show();
        angular.forEach($scope.htmlData, function(value,key) {
            var nametail = new Date().getTime();
            var rand = Math.floor((Math.random()*1000)+10);
            var fileName = key+rand+nametail+"-draft.pdf";
            fileNameArr.push(fileName);
            $("#downloadPdfBn").prop('disabled',true);
            formHtmlData.push({html:value, fileName : fileName});

        });

        var pdf = Form.update({htmlData :formHtmlData,
            userDetails : { name : $scope.formData.dName,
                                            email :$scope.formData.cEmail},
            account: {name: userProfile.name, email: userProfile.email}},
            function(data) {
            if($rootScope.editId) {
               var editId = $rootScope.editId;
               $rootScope.editId = null;
            }
            Form.save({ data : $scope.formData,
                        user : userProfile.id,
                        editId : editId,
                        fileName : fileNameArr,
                        type:"draft"}, function(data) {
                            if (data.success) {
                                //$scope.formData = {};
                                $("#loading").hide();
                                FlashMessage.setMessage({
                                    success:true,
                                    message:"The draft forms will be sent to your email shortly."
                                });
                            }
                        });
             });
    }

    $scope.saveDraftForm = function(templateName,formData) {
        $scope.template = "templates/forms/" + templateName + ".html?t=" + _version;
        //setting the values changed by Script.
        formData.dateVal = $("#dateVal").val();
        formData.pBirth  = $("#pBirth").val();
        formData.pIAddress = $("#pIAddress").val();
        formData.pIState = $("#pIState").val();
        formData.pIPostcode =  $("#pIPostcode").val();
        formData.aPeriod = 'monthly';
        formData.pISuburb =  $("#pISuburb").val();
        $rootScope.draftMode = true;
        $scope.userSign = "";
        // Check if exists empty location fields.
        var comLocationObj = $('input.' + templateName);
        var emptyLocationCnt = 0;
        angular.forEach(comLocationObj, function(item, index) {
            var location = $(item).val();
            if (!location) {
                emptyLocationCnt ++;                
            }
        });

        if (emptyLocationCnt) {
            return;
        }

        $(".reAssign").each(function() {
            var id = $(this).prop("id");
            formData[id] = $(this).val();
        });

        formData["type"] = {};

        $("input[name=formTypes]:checked").each(function() {
            var id = $(this).prop("id");
            formData["type"][id] = $(this).prop("checked");
        });

        if (formData.type.configApp) {
            var telNo = new Array();
            $(".telNo").each(function() {
                  var item = $(this).find("input");
                  var idCount = item.data('count');
                  var telRange = $('#telItemRange' + '-' + idCount).val()!=''?' to '+$('#telItemRange' + '-' + idCount).val():'';
                  var obj = {
                                  tel : item[0].value + telRange 
                            };
                  telNo.push(obj);
            });
            $scope.formData.configAppTelNo = telNo;
            // var mobPort = new Array();
            // $(".mob-port-box").each(function() {
            //     var confAccHolder = $(this).find(".confAccHolder");
            //     var confMobNo = $(this).find(".confMobNo");
            //     var confProvider = $(this).find(".confProvider");
            //     var confAccNo = $(this).find(".confAccNo");
            //     var confPatPlan = $(this).find(".confPatPlan");
            //     var confDob = $(this).find(".confDob");
            //     var confDLicence = $(this).find(".confDLicence");

            //     var obj = {
            //                     confAccHolder : confAccHolder[0].value,
            //                     confMobNo : confMobNo[0].value,
            //                     confProvider : confProvider[0].value,
            //                     confAccNo : confAccNo[0].value,
            //                     confPatPlan : confPatPlan[0].value,
            //                     confDob : confDob[0].value,
            //                     confDLicence : confDLicence[0].value,
            //               };
            //     mobPort.push(obj);
            // });
            $scope.formData.configMobPort = $scope.configMobPort;
        }

        if(formData.type.rental || formData.type.leasing ||formData.singleOrder) {
            formData["rental"] = {};
            var rental = parseInt(formData.aPayment.replace(/[^\d.]/g,''));
            formData["aGST"] = isNaN(rental) ? '0.00' : (rental*10/100).toFixed(2);
            formData["aTotal"] = isNaN(rental) ? '0.00' : (rental + rental*10/100).toFixed(2);
        }

        // var schedule = new Array();
        // var scheduleOptions = new Array();
        // angular.forEach($scope.formData.schItems, function(data) {
        //     if (angular.isDefined(formData.schOption)) {
        //         var obj = {
        //                     qty : formData.schQty[data.count],
        //                     desc :formData.schOption[data.count].item != '_' ? formData.schOption[data.count].item : ''
        //                 };
        //         scheduleOptions.push(obj);
        //     }
        // });

        // $(".schedule-goods").each(function() {
        //     var item = $(this).find("input");
        //     var obj = { qty : item[0].value,
        //                 desc :item[1].value,
        //                 //itemNo : item[2].value,
        //                 //serNo : item[3].value
        //             };
        //     schedule.push(obj);
        // });

        // $scope.formData.scheduleOptions = scheduleOptions;
        // $scope.formData.schedule = schedule;

        if($scope.editId) {
            $rootScope.editId = $scope.editId;
        }

        $rootScope.formData = formData;

        ngDialog.open({
            template: $scope.template,
            plan: true,
            controller : 'FormCtrl',
            width: 900,
            height: 600,
            scope: $scope,
            className: 'ngdialog-theme-default',
            preCloseCallback : function(value) {
                if (confirm("This will create a draft form. Continue?")) {
                    $scope.htmlData[templateName] = $(".ngdialog-content").html();
                    $scope[templateName] = true;
                    $timeout(angular.noop());
                    $('#isSigned').val('T');
                    $('#sendDraft').show();
                }
            }
        });

        $rootScope.$on('ngDialog.opened', function (e, $dialog) {
            var newHash = 'anchor';
            if ($location.hash() !== newHash) {
                // set the $location.hash to `newHash` and
                // $anchorScroll will automatically scroll to it
                $location.hash('anchor');
            } else {
                // call $anchorScroll() explicitly,
                // since $location.hash hasn not changed
                $location.hash('anchor');
                $anchorScroll();
            }
        });
    }
    
    $scope.sendDraftForms = function($id) {

        var forms = Form.downloadForms({id:id}, function(data) {
           if (data.success) {
               angular.forEach(data.files, function(name, index){
                    var filename = name.substr(0, name.indexOf('.')) + '.pdf'; 
                    window.open("assets/files/"+filename,'_blank');
               });
           }
        });
    }

    // setTimeout(function() {
    //    if (!$('.ngdialog.presign').length) {
    //         $scope.preSignDocument();
    //     }
    // }, 1000);

    $scope.changeColTransparent = function(val) {
        //console.log(val);
        if ((val.item == '_') || (val.plan=='_')) {
            $('.col-sel-desc .chosen-container').addClass('transparent');
        } else {
            $('.col-sel-desc .chosen-container').removeClass('transparent');
        }

        $scope.telstraPlans = [];
        $scope.mobileMegaPlans = [];
        $scope.mobile4GUntimedPlans = [];
        //console.log($scope.configMobPort);
        if (angular.isDefined(val.plan)){
            angular.forEach($scope.configMobPort,function(item,index){
                //console.log(item);
                if (item.confPatPlan.group=="Telstra 4G"){
                    if (angular.isUndefined($scope.telstraPlans[item.confPatPlan.itemIndex]))
                        $scope.telstraPlans[item.confPatPlan.itemIndex]=1;
                    else
                        $scope.telstraPlans[item.confPatPlan.itemIndex]++;
                } else if(item.confPatPlan.group=="Mobile Mega"){
                    if (angular.isUndefined($scope.mobileMegaPlans[item.confPatPlan.itemIndex]))
                        $scope.mobileMegaPlans[item.confPatPlan.itemIndex]=1;
                    else
                        $scope.mobileMegaPlans[val.itemIndex]++;
                    item.boltOn = {};
                } else if(item.confPatPlan.group=="Mobile 4G Untimed"){
                    if (angular.isUndefined($scope.mobile4GUntimedPlans[item.confPatPlan.itemIndex]))
                        $scope.mobile4GUntimedPlans[item.confPatPlan.itemIndex]=1;
                    else
                        $scope.mobile4GUntimedPlans[item.confPatPlan.itemIndex]++;
                    item.boltOn = {};
                }
            });
            // console.log($scope.telstraPlans);
            // console.log($scope.mobileMegaPlans);
            // console.log($scope.mobile4GUntimedPlans);
            $scope.processBoltOnCount();
            //console.log($scope.mobileMegaPlans);
        }        
    }

    $scope.processBoltOnCount = function(){
        $scope.boltOnPlans = [];
        angular.forEach($scope.configMobPort,function(item,index){
            //console.log(item.boltOn.itemIndex);
            if (angular.isDefined(item.boltOn.itemIndex)){
                if (angular.isUndefined($scope.boltOnPlans[item.boltOn.itemIndex])){
                    $scope.boltOnPlans[item.boltOn.itemIndex]=1;    
                } else {
                    $scope.boltOnPlans[item.boltOn.itemIndex]++;
                }
            }
        });
    }

    if ($rootScope.viewFormData != null) {
        if (angular.isDefined($rootScope.viewFormData)) {
            $scope.editId = $rootScope.viewFormData.id;
        }
    }

    Form.getScheduleGoods(function(data) {
        $scope.scheduleGoodsList = data.goods;
        //console.log(data.goods);
        $scope.scheduleGoodsList.unshift({
            item: "_"
        });
    });


    function onChangeTemp(e)
    {
        //console.log(e);
    }

    $scope.handleTermChange = function (formData) {
        formData.promotedText = '';
        if ( formData.aTerm == '48promoted') {
              formData.aTermNew = 48;
              formData.PromotedText = "(First 3 Payments $0, followed by 45 payments of rental amount)"
        }
        if  (formData.aTerm =='60promoted') {
            formData.aTermNew = 60;
            formData.PromotedText = "(First 3 Payments $0, followed by 57 payments of rental amount)"
        }
        if  (formData.aTerm =='60') {
            formData.aTermNew = 60;
        }
        if (formData.aTerm =='48') {
            formData.aTermNew = 48;
        }
        if (formData.aTerm =='36') {
            formData.aTermNew = 36;
        }
        if  (formData.aTerm =='24') {
            formData.aTermNew = 24;
        }
        if (formData.aTerm =='Outright') {
            formData.aTermNew = "Outright";
        }
        return formData
    }

    $(".validUrl").on("keyup", function() {
        var val = this.value.replace(/^(http\:\/\/)/,'');
        this.value = "http://"+val;
    });

    $(".onlyAlpha").on("keyup", function() {
       var val = this.value.replace(/\d/g,'');
       this.value = val;
    });
     $(".onlyNumber").on("keyup", function() {
       var val = this.value.replace(/\D/g,'');
       this.value = val;
    });

    $(".dollarSign").on("keyup", function() {
        var val = this.value.replace(/\D/g,'');
        var parts = val.toString().split(".");
        parts =  parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
        this.value = "$ "+parts;

        if (parts == "" || parts == "0") {
            this.value = "$ 0";
        } else {
            if (parts.indexOf("0") == 0) {
                parts = parts.replace("0","");
            }
            this.value = "$ "+parts;
        }
    });

    $("#dName").on("focusout", function() {
        $("#cContact").val($(this).val());
        $("#pName").val($(this).val());
    });

    $('#dSName').on("focusout", function() {
        $("#g2Name").val($(this).val());
    });

    $http.get("profile").success(function(response) {
        $scope.formData.wName = response.name;
        $scope.formData.userSign = response.sign;
    });

    $scope.transformTelFields  = function() {
        $('.tel').on("keyup", function()
        {
                var val = this.value.replace(/[^0-9]/ig, '');
                if (val.length >10) {
                    val = val.substr(0,10);
                }
                var newVal = '';
                if (val.length >2) {
                  newVal +='('+val.substr(0,2)+') ';
                  val = val.substr(2);
                }
                while (val.length > 4)   {
                  newVal += val.substr(0, 4) +'-';
                  val = val.substr(4);
                }
                newVal += val;
                this.value = newVal;
        });
        $('.telRange').on("keyup",function(){
            var val = this.value.replace(/[^0-9]/ig, '');
            if (val.length>4) {
                val = val.substr(0,4);
            }
            this.value = val;            
        });
    }

    $scope.addTel = function() {
        $timeout(function(){
            //var content = $(".addTelBtn").siblings(".telNo");
            var content = '';
            content = '<div class="row telNo form-row">\n';
            content +='     <div class="row-element-label">\n\
                                 <label class="control-label">Tel No</label>\n\
                                  \n\<i id="adTel-' + $scope.addTelCount + '" class="fa fa-times"></i>\n\
                             </div>\n\
                             <div class="row-element-field">\n\
                                <div class="col-lg-6 col-md-12">\n\
                                    <input class="telItem tel form-control" data-count="' + $scope.addTelCount + '" id="telItemConfigApp-' + $scope.addTelCount + '" type="text" autofocus placeholder="From">\n\
                                </div>\n\
                                <div class=" col-lg-6 col-md-12">\n\
                                    <input class="telItem telRange form-control" data-count="' + $scope.addTelCount + '" id="telItemRange-' + $scope.addTelCount + '" type="text" autofocus placeholder="To">\n\
                                </div>\n\
                            </div>\n\
                        </div>';
            $(".addTelBtn").before(content);
            $("#telItemConfigApp-"+$scope.addTelCount).focus();
            $("#adTel-"+$scope.addTelCount).click(function() {
                  $(this).parent().parent().remove();
                  $scope.addTelCount--;
              });
            $scope.transformTelFields();
            $scope.addTelCount++;
        });

    }

    $scope.addAccount = function(){
       $newAccount = {
            'configCarrier':'',
            'configAccNo':'',
        };
        $scope.accountLocation[index]++;
        $scope.formData.accounts.push($newAccount);
        // $scope.accountLocation++;
        console.log($scope.formData.accounts);
    }
    $scope.removeAccount = function(index) {
        $scope.formData.accounts.splice(index,1);
    }

    $scope.addPortingAuth = function() {
        // $timeout(function(){
        $newMobPort = {
            'confAccHolder':$scope.configMobPort[$scope.addMobPortCount].confAccHolder,
            'confMobNo':'',
            'confProvider':$scope.configMobPort[$scope.addMobPortCount].confProvider,
            'confAccNo':$scope.configMobPort[$scope.addMobPortCount].confAccNo,
            'confPatPlan':$scope.mobile_rate_plans[0],
            'boltOn' : {}
        };
        $scope.addMobPortCount[index]+1;
        $scope.configMobPort.push($newMobPort);
        //     //var content = $(".addTelBtn").siblings(".telNo");
        //     var content =$(".mob-port-box").html();
        //     var wrapped = "<div class='form-row mob-port-box' id='mob-port"+$scope.addMobPortCount+"'>"+content+"</div>";
        //     $(".portingAuthBtn").before(wrapped);
        //     $('.date').datetimepicker({
        //         format : "DD/MM/YYYY",
        //         defaultDate : today
        //     });
        //     $scope.transformTelFields();
        // });
    }

    $scope.saveAsDraft = function(formData) {
        formData.pBirth = $('#pBirth').val();
        //formData.aBillingTerm = $('#aBillingTerm').val();
        //formData.aIdenQuestion = $('#aIdenQuestion').val();
        // formData.bfCapDiscount = $('#bFCapDiscount').val();
        // console.log(formData);
        // console.log(formData);
        if (!$scope.editId) {
            $scope.editId = "";
        }
        var user = Scopes.get('DefaultCtrl').profile;
        $(".reAssign").each(function() {
            var id = $(this).prop("id");
            if (id=='alarmPlans'){
                var alarmPlan = $scope.formData.alarmPlans;
                formData[id] = $(this).val();
                $scope.formData.alarmPlans = alarmPlan;
            } else {
                formData[id] = $(this).val();
            }
            if (id == "aPayout" && !$(this).val())
                formData[id] = "$ 0";
        });

        formData.configMobPort = $scope.configMobPort;
        // formData.printer.cpiColourAmt = $scope.printer.cpiColourAmt;
        // formData.printer.cpiBwAmt = $scope.printer.cpiBwAmt;
        
        // console.log(formData.bFCapDiscount);

        // var schedule = new Array();
        // var scheduleOptions = new Array();
        // // angular.forEach($scope.formData.schItems, function(data) {
        // //     if(angular.isDefined(formData.schOption)) {
        // //         /* If description added, we should set the quantity to 1 or entered value.*/
        // //         var qty;
        // //         if (angular.isUndefined(formData.schQty)) {
        // //             qty = '1';
        // //         } else {
        // //             qty = formData.schQty[data.count] == "null" ? '1' : formData.schQty[data.count];
        // //         }

        // //         if (angular.isUndefined(formData.schOption[data.count])) {
        // //             qty = '0';
        // //         } else {
        // //             var obj = {
        // //                     qty : qty,
        // //                     desc :formData.schOption[data.count].item != '_' ? formData.schOption[data.count].item : ''
        // //                 };
        // //             scheduleOptions.push(obj);
        // //         }
        // //     }
        // // });
        // // $(".schedule-goods").each(function() {
        // //       var item = $(this).find("input");
        // //             var obj = {
        // //                     qty : item[0].value,
        // //                     desc :item[1].value,
        // //                 };
        // //             schedule.push(obj);
        // //  });

        // // $scope.formData.schedule = schedule;
        // // $scope.formData.scheduleOptions = scheduleOptions;
        //console.log(formData);
        Form.save({data:formData, user : user.id, fileName : '',
                    editId : $scope.editId, type:"draft"}, function(data) {
                        $scope.editId = data.editId;
        });
    }

    $scope.prepDiscounts = function(payment) {
      payment = payment.replace(/^\D+|\,+/g, '');
       $scope.EqPayment = payment;
      }

    $scope.updateComLocations = function (){
        $scope.formData.comLocation = {
            'nbf_cap'               : [$scope.formData.pIAddress],
            'nbf_std'               : [$scope.formData.pIAddress],
            'voice_comp'              : [$scope.formData.pIAddress],
            'voice_solution_untimed'  : [$scope.formData.pIAddress],
            'voice_essentials_cap'    : [$scope.formData.pIAddress],
            'voice_solution_standard' : [$scope.formData.pIAddress],
            'data_adsl'               : [$scope.formData.pIAddress],
            'ip_midband'              : [$scope.formData.pIAddress],
            'ip_midbandunli'          : [$scope.formData.pIAddress],
            'nbn'                     : [$scope.formData.pIAddress],
            'nbnUnlimited'            : [$scope.formData.pIAddress],
            'mobile_mega'             : [$scope.formData.pIAddress],
            'mobile_ut'               : [$scope.formData.pIAddress],
            'mobile_wireless'         : [$scope.formData.pIAddress],
            '1300'                    : [$scope.formData.pIAddress],
            '1300_discounted'         : [$scope.formData.pIAddress],
            'fibre'                   : [$scope.formData.pIAddress],
            'ethernet'                : [$scope.formData.pIAddress],
            'telstraUntimed'          : [$scope.formData.pIAddress],
            'telstraWireless'         : [$scope.formData.pIAddress],
            'printer'                 : [$scope.formData.pIAddress],
            'schedule_of_goods'       : [$scope.formData.pIAddress]
        };
    }

    $scope.updateComSuburbs = function (){
        $scope.formData.comSuburb = {
            'nbf_cap'               : [$scope.formData.pISuburb],
            'nbf_std'               : [$scope.formData.pISuburb],
            'voice_comp'              : [$scope.formData.pISuburb],
            'voice_solution_untimed'  : [$scope.formData.pISuburb],
            'voice_essentials_cap'    : [$scope.formData.pISuburb],
            'voice_solution_standard' : [$scope.formData.pISuburb],
            'data_adsl'               : [$scope.formData.pISuburb],
            'ip_midband'              : [$scope.formData.pISuburb],
            'ip_midbandunli'          : [$scope.formData.pISuburb],
            'nbn'                     : [$scope.formData.pISuburb],
            'nbnUnlimited'            : [$scope.formData.pISuburb],
            'mobile_mega'             : [$scope.formData.pISuburb],
            'mobile_ut'               : [$scope.formData.pISuburb],
            'mobile_wireless'         : [$scope.formData.pISuburb],
            '1300'                    : [$scope.formData.pISuburb],
            '1300_discounted'         : [$scope.formData.pISuburb],
            'fibre'                   : [$scope.formData.pISuburb],
            'ethernet'                : [$scope.formData.pISuburb],
            'telstraUntimed'          : [$scope.formData.pISuburb],
            'telstraWireless'         : [$scope.formData.pISuburb],
            'printer'                 : [$scope.formData.pISuburb],
            'schedule_of_goods'       : [$scope.formData.pISuburb]
        };
    }

    $scope.updateComPostCode = function (){
        $scope.formData.comZipCode = {
            'nbf_cap'               : [$scope.formData.pIPostcode],
            'nbf_std'               : [$scope.formData.pIPostcode],
            'voice_comp'              : [$scope.formData.pIPostcode],
            'voice_solution_untimed'  : [$scope.formData.pIPostcode],
            'voice_essentials_cap'    : [$scope.formData.pIPostcode],
            'voice_solution_standard' : [$scope.formData.pIPostcode],
            'data_adsl'               : [$scope.formData.pIPostcode],
            'ip_midband'              : [$scope.formData.pIPostcode],
            'ip_midbandunli'          : [$scope.formData.pIPostcode],
            'nbn'                     : [$scope.formData.pIPostcode],
            'nbnUnlimited'            : [$scope.formData.pIPostcode],
            'mobile_mega'             : [$scope.formData.pIPostcode],
            'mobile_ut'               : [$scope.formData.pIPostcode],
            'mobile_wireless'         : [$scope.formData.pIPostcode],
            '1300'                    : [$scope.formData.pIPostcode],
            '1300_discounted'         : [$scope.formData.pIPostcode],
            'fibre'                   : [$scope.formData.pIPostcode],
            'ethernet'                : [$scope.formData.pIPostcode],
            'telstraUntimed'          : [$scope.formData.pIPostcode],
            'telstraWireless'         : [$scope.formData.pIPostcode],
            'printer'                 : [$scope.formData.pIPostcode],
            'schedule_of_goods'       : [$scope.formData.pIPostcode]
        };
    }

    $scope.showPlan = function(plan){
        //console.log(plan);
    }

    $scope.calculateDiscount = function(currentVal,plan, planOptionId, completeOptions,optionId) {
        var payment = 0;
        var capTotalVal =0;
        //console.log(planOptionId);
        if (planOptionId != '') {
            var planStr = $("#"+planOptionId+" option:selected").text();
            var planVal =parseInt(planStr.match(/\$(\d+)/)[1]);
        }
        currentVal = parseInt(currentVal);
        //alert(currentVal);
        if(angular.isDefined($scope.EqPayment)) {
            payment = parseInt($scope.EqPayment);
        }
        //alert(completeOptions);
        var c_options = angular.isDefined(completeOptions)?completeOptions:0;
        //alert(c_options);
        $(".main-disc").each(function() {
            capTotalVal += parseInt($(this).val());
        });
        if (plan == 'voice-cap' || plan == 'data-plan' || plan == 'mobile-plan' ) {
            if (currentVal > planVal) {
                alert("Discount cannot exceed monthly payment");
                //$('#' + optionId).val('');
            } else {
                if(capTotalVal  == currentVal) {
                    if (currentVal> planVal/2) {
                        var disc = planVal/2;
                        alert("Maximum Discount should not go over $"+disc);
                        //$('#' + optionId).val('');
                    }
                } else {
                    var remaining = payment-(capTotalVal-currentVal);
                    if (remaining<0) {
                        remaining = 0;
                    }

                    var disc = remaining + planVal*30/100;
                    if (currentVal > disc) {
                        alert("Maximum Discount should not go over $"+disc);
                        //$('#' + optionId).val('');
                    }
                }
            }
        } else if (plan == 'standard-plan') {
            if (currentVal>100) {
                alert("Maximum Discount should not go over $100");
               // $('#' + optionId).val('');
            }
        }  else if (plan == 'complete-bri') {
            if (currentVal>50*c_options) {
                alert("Maximum Discount should not go over $"+50*c_options);
                //$('#' + optionId).val('');
            }
        } else if (plan == 'complete-pri') {
            if (currentVal>100*c_options) {
                alert("Maximum Discount should not go over $"+100*c_options);
               // $('#' + optionId).val('');
            }
        } else if (plan == 'complete-analog') {
            if (currentVal>(10*c_options)) {
                alert("Maximum Discount should not go over $" + 10*c_options);
                //$('#' + optionId).val('');
            }
        } else if (plan == 'complete-call' || plan == '1300-plan' || plan == 'standard-call') {
            if (currentVal> payment) {
                alert("Maximum Discount should not go over monthly payment");
                //$('#' + optionId).val('');
            }
        } else if (plan == 'unlimited-plan') {
            if (currentVal> planVal) {
                alert("Maximum Discount should not go over monthly payment");
                //$('#' + optionId).val('');
            } else {
                if (currentVal> planVal/2) {
                    var disc = planVal/2;
                    alert("Maximum Discount should not go over $"+disc);
                   // $('#' + optionId).val('');
                }
            }
        }
    }

    $scope.showOptions = function(form) {
        //alert($scope.formData.type.monitoring_solution);
        $(".optionBox").hide();
        $("#"+form+"Option").show();
        $(".form-label label").css('color','black');
        $("#"+form+"Label label").css('color','green');
        if (form=='copierAgreement'){
            $scope.processCopierAgreement();
        }
        // Initialize location count and locations object.
        $scope.locationCount = 0;
    }

    $scope.processCopierAgreement = function(){
        //alert('Process Copier Agreement');
        var printers = [];
        //console.log($scope.items['schedule_of_goods']);
        angular.forEach($scope.items['schedule_of_goods'],function(item,index){
            printers[index] = [];
            //console.log($scope.formData.schItems[index]);
           angular.forEach($scope.formData.schItems[index],function(schItem){
            //console.log(schItem);
                if (!angular.isUndefined($scope.formData.schOption)){
                    if (!angular.isUndefined($scope.formData.schOption[index])){
                        if (!angular.isUndefined($scope.formData.schOption[index][schItem.count])){
                            if (($scope.formData.schOption[index][schItem.count].group.toUpperCase().indexOf('KYOCERA')!==-1) || ($scope.formData.schOption[index][schItem.count].group.toUpperCase().indexOf('FUJI')!==-1) || ($scope.formData.schOption[index][schItem.count].group.toUpperCase().indexOf('OKI')!==-1)){
                                var cpiBwAmt = $scope.formData.schQty[index][schItem.count] * $scope.formData.schOption[index][schItem.count].cpiBw;
                                if (cpiBwAmt==0){
                                    cpiBwAmt = 1.0;
                                }
                                var cpiColourAmt = $scope.formData.schQty[index][schItem.count] * $scope.formData.schOption[index][schItem.count].cpiColour;
                                if (cpiColourAmt==0){
                                    cpiColourAmt = 10.0;
                                }
                                var printer = {
                                    "model" : $scope.formData.schOption[index][schItem.count].model,
                                    "group" : $scope.formData.schOption[index][schItem.count].group,
                                    "item"  : $scope.formData.schOption[index][schItem.count].item,
                                    "qty"  : $scope.formData.schQty[index][schItem.count],
                                    "cpiBwAmt"  : cpiBwAmt.toFixed(1),
                                    "cpiColourAmt"  : cpiColourAmt.toFixed(1)
                                }
                                printers[index].push(printer);
                            }
                        }
                    }
                }
             });
        });
        angular.forEach(printers, function(printer,index){
            if (printer.length==0){
                printers.splice(index,1);
            }
        });
        //console.log(printers);
        if (printers.length!=0){
            $scope.formData.printers = printers;
        }
        else{
            alert('No printer is selected in the schedule of goods.');
        }            
    }

    // $scope.ipPlan = {
    //     10 : {rate : 499, desc : "Up to 10/10 Mbps**"},
    //     T20 : {rate : 599, desc : "Up to 20/20 Mbps**"},
    //     400 : {rate : 758, desc : "Up to 400/400Mbps**"},
    //     4 : {rate : 419, desc : "4Mbps/4Mbps"},
    //     8 : {rate : 429, desc : "8Mbps/8Mbps*"},
    //     18 : {rate : 489, desc : "18Mbps/18Mbps*"},
    //     20 : {rate : 599, desc : "20Mbps/20Mbps*"},
    //     30 : {rate : 998, desc : "30Mbps/30Mbps*"}
    // }

    // $scope.ipDownload = {
    //     50 :{plan : "50GB", rate: 69},
    //     100 :{plan : "100GB", rate : 79},
    //     200 : {plan : "200GB" , rate :139},
    //     300 : {plan : "300GB", rate : 209},
    //     500 : {plan : "500GB",rate : 339},
    //     1000 : {plan : "1000GB", rate : 639}
    // }

    $scope.nbnPlan = {
        255 : {rate : 49, desc : "NBN 25Mbps/5Mbps*"},
        2510 : {rate : 59, desc : "NBN 25Mbps/10Mbps*"},
        5020 : {rate : 69, desc : "Up to 50/20Mbps**"},
        10040 : {rate : 79, desc : "NBN 100Mbps/40Mbps*"}
    }
    $scope.nbnDownload = {
        100 :{plan : "100GB", rate : 19},
        200 : {plan : "200GB" , rate :25},
        500 : {plan : "500GB",rate : 29},
        1000 : {plan : "1000GB", rate : 49}
    }

    $scope.setValues = function() {
        $scope.formData.cName = "Some Long Name Comp.";
        $scope.formData.cAbn = 12323231321;
        $scope.formData.cTrading = "Long trade name";
        $scope.formData.cAddress = "221B Baker Street";        
        $scope.formData.cPostCode = 2000;

        $scope.formData.pIAddress = "221B Baker Street";
        $scope.formData.pISuburb = "London";
        $scope.formData.pIState = "ACT";
        $scope.formData.pIPostcode = "1234";
        $scope.formData.aBillingTerm = 60;
        $scope.formData.aTerm = 60;

        $scope.formData.comLocation = {};
        $scope.formData.comLocation = {
            'nbf_cap'               : [$scope.formData.pIAddress],
            'nbf_std'               : [$scope.formData.pIAddress],
            'voice_comp'              : [$scope.formData.pIAddress],
            'voice_solution_untimed'  : [$scope.formData.pIAddress],
            'voice_essentials_cap'    : [$scope.formData.pIAddress],
            'voice_solution_standard' : [$scope.formData.pIAddress],
            'data_adsl'               : [$scope.formData.pIAddress],
            'ip_midband'              : [$scope.formData.pIAddress],
            'ip_midbandunli'          : [$scope.formData.pIAddress],
            'nbn'                     : [$scope.formData.pIAddress],
            'nbnUnlimited'            : [$scope.formData.pIAddress],
            'mobile_mega'             : [$scope.formData.pIAddress],
            'mobile_ut'               : [$scope.formData.pIAddress],
            'mobile_wireless'         : [$scope.formData.pIAddress],
            '1300'                    : [$scope.formData.pIAddress],
            '1300_discounted'         : [$scope.formData.pIAddress],
            'fibre'                   : [$scope.formData.pIAddress],
            'ethernet'                : [$scope.formData.pIAddress],
            'telstraUntimed'          : [$scope.formData.pIAddress],
            'telstraWireless'         : [$scope.formData.pIAddress],
            'printer'                 : [$scope.formData.pIAddress],
            'schedule_of_goods'       : [$scope.formData.pIAddress]
        };

        $scope.formData.cSuburb = "London";
        $scope.formData.comSuburb = {};
        $scope.formData.comSuburb = {
            'nbf_cap'               : [$scope.formData.pISuburb],
            'nbf_std'               : [$scope.formData.pISuburb],
            'voice_comp'              : [$scope.formData.pISuburb],
            'voice_solution_untimed'  : [$scope.formData.pISuburb],
            'voice_essentials_cap'    : [$scope.formData.pISuburb],
            'voice_solution_standard' : [$scope.formData.pISuburb],
            'data_adsl'               : [$scope.formData.pISuburb],
            'ip_midband'              : [$scope.formData.pISuburb],
            'ip_midbandunli'          : [$scope.formData.pISuburb],
            'nbn'                     : [$scope.formData.pISuburb],
            'nbnUnlimited'            : [$scope.formData.pISuburb],
            'mobile_mega'             : [$scope.formData.pISuburb],
            'mobile_ut'               : [$scope.formData.pISuburb],
            'mobile_wireless'         : [$scope.formData.pISuburb],
            '1300'                    : [$scope.formData.pISuburb],
            '1300_discounted'         : [$scope.formData.pISuburb],
            'fibre'                   : [$scope.formData.pISuburb],
            'ethernet'                : [$scope.formData.pISuburb],
            'telstraUntimed'          : [$scope.formData.pISuburb],
            'telstraWireless'         : [$scope.formData.pISuburb],
            'printer'                 : [$scope.formData.pISuburb],
            'schedule_of_goods'       : [$scope.formData.pISuburb]
        };

        $scope.formData.comZipCode = {};
        $scope.formData.comZipCode = {
            'nbf_cap'               : [$scope.formData.pIPostcode],
            'nbf_std'               : [$scope.formData.pIPostcode],
            'voice_comp'              : [$scope.formData.pIPostcode],
            'voice_solution_untimed'  : [$scope.formData.pIPostcode],
            'voice_essentials_cap'    : [$scope.formData.pIPostcode],
            'voice_solution_standard' : [$scope.formData.pIPostcode],
            'data_adsl'               : [$scope.formData.pIPostcode],
            'ip_midband'              : [$scope.formData.pIPostcode],
            'ip_midbandunli'          : [$scope.formData.pIPostcode],
            'nbn'                     : [$scope.formData.pIPostcode],
            'nbnUnlimited'            : [$scope.formData.pIPostcode],
            'mobile_mega'             : [$scope.formData.pIPostcode],
            'mobile_ut'               : [$scope.formData.pIPostcode],
            'mobile_wireless'         : [$scope.formData.pIPostcode],
            '1300'                    : [$scope.formData.pIPostcode],
            '1300_discounted'         : [$scope.formData.pIPostcode],
            'fibre'                   : [$scope.formData.pIPostcode],
            'ethernet'                : [$scope.formData.pIPostcode],
            'telstraUntimed'          : [$scope.formData.pIPostcode],
            'telstraWireless'         : [$scope.formData.pIPostcode],
            'printer'                 : [$scope.formData.pIPostcode],
            'schedule_of_goods'       : [$scope.formData.pIPostcode]
        };
        $scope.formData.aIdenQuestion = "";
        $scope.formData.cEmail = "info@somelongcompany.com.au";
        $scope.formData.dName = "Arthur Conan Doyle";
        $scope.formData.cPosition = "";
        $scope.formData.dSName = "Sherlock Holmes";
        $scope.formData.cSPosition = "";
        $scope.formData.cContact = "Arthur Conan Doyle";
        $scope.formData.cTel = "(02) 4342-1232";
        $scope.formData.cFax = "(02) 4342-1232";
        $scope.formData.cMobile = "4532-533-267";
        $scope.formData.cWebsite = "http://somelongcompany.com.au";
        $scope.formData.aAccountant = "James Moriarty";
        $scope.formData.aIndustry = "PROFESSIONAL OFFICE";
        $scope.formData.aTradeRef = "A Study in Scarlet";
        $scope.formData.aCPerson = "Dr. Watson";
        $scope.formData.aTel = "(02) 2340-5444";
        $scope.formData.pName = "Sherlock Holmes";
        $scope.formData.pABN = "0919231921";
        $scope.formData.pFax = "(02) 4324-1234";
        $scope.formData.pEmail = "info@somelongcompany.com.au";
        $scope.formData.pAddress = "220B Baker Street";
        $scope.formData.pSuburb = "London";
        $scope.formData.pPostcode = 2000;
        $scope.formData.pPhone = "(02) 4342-1232";
        $scope.formData.pLicense = "2332323243A";
        $scope.formData.pValue = "$ 600,000";
        $scope.formData.pMortgage = "$ 400,000";
        $scope.formData.pOwned = true;
        $scope.formData.tTel = "(02) 4342-1232";
        $scope.formData.aContact = "Mrs. Hudson";
        $scope.formData.tContact = "Mr. Hudson";
        $scope.formData.aNemp = "1000";
        $scope.formData.aInsurer = "Insurance Company";
        $scope.formData.aPolicy = "A/127/2016";
        $scope.formData.g2Name = "John Watson";
        $scope.formData.g2Address = "220B Baker Street";
        $scope.formData.g2ABN = "5352352525";
        $scope.formData.g2Suburb = "London";
        $scope.formData.g2Postcode = 2000;
        $scope.formData.g2Phone = "(02) 4342-1232";
        $scope.formData.g2License = "2332323243A";
        $scope.formData.g2Value = "$ 600,000";
        $scope.formData.g2Mortgage = "$ 400,000";
        $scope.formData.g2Owned = true;
        $scope.formData.g2Fax = "(02) 4444-1111";
        $scope.formData.g2Email = "guarantor2@guarantor.com";
        $scope.formData.aYear = "1";
        $scope.formData.cState = "ACT";
        $scope.formData.comments = "Referred by: " +  $scope.formData.cName + " " +  $scope.formData.cContact + " " + $scope.formData.cTel;
        //console.log($('#formData'));
    }
    $scope.saver = function(){
         $scope.formData.term = $('#term').val();
         $scope.formData.type = $('#type').val();
         var rates = Form.getRates({term:$scope.formData.term,
                                    type:$scope.formData.type
                                    }, function(data) {
            if (data.success) {
            $scope.rates = data.rates;
           }
            console.log($scope.rates);
            if($scope.formData.type == 'rental'){
            alert('rental');
            console.log($scope.rates);
            angular.forEach($scope.rates, function(value, key){
                
                   console.log('Rental:' + key + ': ' + value.upperLimit + ':' + value.lowerLimit + ':' + value.rate + $scope.formData.perMonth);
                   if($scope.checkRange($scope.formData.perMonth,value.lowerLimit,value.upperLimit)){
                   $scope.leaser = ($scope.formData.perMonth/1000)*value.rate;
                 }
             });
            
        }
        else{
            alert('leasing');
            console.log($scope.rates);
            angular.forEach($scope.rates, function(value, key){
                
                   console.log(key + ': ' + value.upperLimit + ':' + value.lowerLimit + ':' + value.rate);
                    if($scope.checkRange($scope.formData.perMonth,value.lowerLimit,value.upperLimit)){
                   $scope.leaser = ($scope.formData.perMonth/1000)*value.rate;
                 }
             });
        }
        
        });
            
  }
        $scope.checkRange = function(val,lower_limit,upper_limit){
        //alert(val + '-' + lower_limit + '-' + upper_limit);
        if (upper_limit==0){
            if (val>=lower_limit){
                return true;
            } else {
                return false;
            }
        } else {
            if (val >= lower_limit && val <= upper_limit){
                return true;
            } else {
                return false;
            }
        }
    }
    $scope.updateReferalInfo = function (){
        $scope.formData.comments =  "Referred by: ";
        if(angular.isDefined($scope.formData.cName)){
            $scope.formData.comments += $scope.formData.cName;
        } 
        if(angular.isDefined($scope.formData.cContact)){
            $scope.formData.comments += (' ' + $scope.formData.cContact);  
        }
        if(angular.isDefined($scope.formData.cTel)){
            $scope.formData.comments += (' ' + $scope.formData.cTel); 
        }
    }

    $('.mob').on("keyup", function()
    {
        var val = this.value.replace(/[^0-9]/ig, '');
        var newVal = '';
        /*if (val.length >10) {
            val = val.substr(0,10);
        }*/
        if (val.length >4) {
            newVal +=val.substr(0,4)+'-';
            val = val.substr(4);
        }
        while (val.length > 3)   {
            newVal += val.substr(0, 3) +'-';
            val = val.substr(3);

        }
        newVal += val;
        this.value = newVal;
    });

    $('.ccNo').on("keyup", function()
    {
        var val = this.value.replace(/[^0-9]/ig, '');
        var newVal = '';
        while (val.length > 4) {
            newVal += val.substr(0, 4) +'-';
            val = val.substr(4);
        }
        newVal += val;
        this.value = newVal;
    });

    $scope.transformTelFields();

    var dateObj = new Date();
    var dd = dateObj.getDate();
    var mm = dateObj.getMonth()+1;
    var yy = dateObj.getFullYear();
    var today = mm+"/"+dd+"/"+yy;
    var minYY = yy - 18; //(Applicant have to be atleast 18);
    var birthMinDate = mm+"/"+dd+"/"+minYY;

    $('.expiryDate').datetimepicker({
       format : "DD/MM/YYYY",
       minDate : today
    });

    $('.date').datetimepicker({
        format : "DD/MM/YYYY",
        defaultDate : today
    });
    $('.pBirthDate').datetimepicker({
        format : "DD/MM/YYYY",
        maxDate : birthMinDate


    });

    $scope.getRates = function(term,type){
      Form.getRates({term:term,type:type}, function(data) {
        if (data.success) {
          $rootScope.rates = data.rates;
        }
      });
    }

    $scope.$watch("formData.type.rental", function(val) {
        if (angular.isDefined(val)) {
            if (val == true) {
                $scope.getRates($scope.formData.aTerm,'rental');
                $scope.formData.type.leasing = false;
                $scope.formData.type.outright = false;
                $scope.formData.type.chattle = false;
            }
          //  $scope.showSchedule();
        }
    });

    $scope.$watch("formData.type.leasing", function(val) {
        if (angular.isDefined(val)) {
           if (val == true) {
                $scope.getRates($scope.formData.aTerm,'leasing');
                $scope.formData.type.rental = false;
                $scope.formData.type.outright = false;
                $scope.formData.type.chattle = false;
            }
           // $scope.showSchedule();
        }
    });

    $scope.$watch("formData.type.outright", function(val) {
        if (angular.isDefined(val)) {
            if (val == true) {
                $scope.formData.type.rental = false;
                $scope.formData.type.leasing = false;
                $scope.formData.type.chattle = false;
            }

        }
    });

    $scope.$watch("formData.type.chattle", function(val) {
        if (angular.isDefined(val)) {
            if (val == true) {

                $scope.formData.type.rental = false;
                $scope.formData.type.leasing = false;
                $scope.formData.type.outright = false;
            }
        }
    });

    $scope.sign = function(id) {
       $("#testDi").dialog("open");
       $('#iAccept').attr('data-id', id);
       if (!$('#'+id).find('canvas').length) {
            $('#signatureBox').html("<span>Sign here!</span>&nbsp;&nbsp;<i class='fa fa-refresh'onclick=clearSg('signatureBox')></i>");
            $('#signatureBox').jSignature({
                color:"#000000",lineWidth:1,
                    width :490, height :98,
                    cssclass : "signature-canvas",
                   "decor-color": 'transparent'
                });
            clicked = true;
        }
    }


    $scope.presign = function(id) {
       $("#testDi").dialog("open");
       $('#iAccept').attr('data-id', id);
       if (!$('#'+id).find('canvas').length) {
            $('#signatureBox').html("<span>Sign here!</span>&nbsp;&nbsp;<i class='fa fa-refresh'onclick=clearSg('signatureBox')></i>");
            $('#signatureBox').jSignature({
                color:"#000000",
                lineWidth:1,
                width :490, 
                height :98,
                cssclass : "signature-canvas",
                "decor-color": 'transparent'
            });
        }
    }

    $scope.sendReferral = function(){
        // console.log($scope.profile.name);
        // console.log($scope.formData.comments);
        // console.log($scope.formData.cName);
        // console.log($scope.formData.rContactPerson);
        // console.log($scope.formData.rPhoneNumber);
        // console.log($scope.formData.rBusinessName);
        // alert($scope.formData.rContactPerson);
        // alert($scope.formData.rBusinessName);
        // alert($scope.formData.rPhoneNumber);
        Form.send_referrals({
                'lp_Comments'   : $scope.formData.comments + ' through ' + $scope.profile.name,
                'lp_Company'    : $scope.formData.rBusinessName,
                'lp_Phone'      : $scope.formData.rPhoneNumber,
                'lp_ContactFirstName' : $scope.formData.rContactPerson,
                'wl_salesrep'   : $scope.profile.name,
                'wl_referredby' : 'Referred by : ' + $scope.formData.cName + ' ' + $scope.formData.cContact + ' ' + $scope.formData.cTel + ' through ' + $scope.profile.name,
                'lp_UserField6' : 'Referral From Ap Nexgen',
            },function(data){
                if (data.response) {
                    // alert("response");
                    // console.log(data.response)
                }
                if (data.link) {
                    // alert("links");
                    // console.log(data.link)
                }
            }
        );
        
        // var str =  'Referred by ' +  $scope.formData.cName + '' + $scope.formData.cContact + '' + $scope.formData.cTel;
        // var regExpr = /[^a-zA-Z0-9-. ]/g;
        // var comments = $scope.formData.comments;
        // angular.forEach($scope.formData.referals, function(index,item){
        //     if ($scope.formData.rBusine ssName[index] != null && $scope.formData.rContactPerson[index] != null && $scope.formData.rPhoneNumber[index] != null ){
        //         $http({
        //             method: 'POST',
        //             url: 'https://www.crmtool.net/lp_NewLeadbeta.asp',
        //             data: { 
        //                 wl_salesrep : $scope.profile.name,
        //                 lp_Username : "NexgenWeb", 
        //                 lp_Password : "pDqEXCN2C35f",
        //                 lp_CompanyID : "12498",
        //                 lp_Comments : $scope.formData.comments,
        //                 wl_leadsource : "2",
        //                 lp_UserField6 : "Referral From Ap",
        //                 lp_Company  : $scope.formData.rBusinessName[index],
        //                 lp_ContactFirstName : $scope.formData.rContactPerson[index],
        //                 lp_Phone : $scope.formData.rPhoneNumber[index],
        //                 wl_referredby : "Referred by : " + $scope.formData.cName + ' ' + $scope.formData.cContact + ' ' + $scope.formData.configAppTelNo    
        //             },
        //             // url : 'https://www.crmtool.net/lp_NewLead.asp?' +
        //             //         'lp_Comments=' + comments.replace(/\W/g, ' ') + '&' +
        //             //         'lp_Company=' + $scope.formData.rBusinessName[index] + '&' + 
        //             //         'lp_CompanyID=12498' + '&'+
        //             //         'lp_ContactFirstName=' +  $scope.formData.rContactPerson[index] + '&' +
        //             //         'lp_Password=pDqEXCN2C35f' + '&' +
        //             //         'lp_Phone=' + $scope.formData.rPhoneNumber[index] + '&' +
        //             //         'lp_UserField6=Referral From Ap' + '&' +
        //             //         'lp_Username=NexgenWeb' + '&' +
        //             //         'wl_leadsource=2' + '&' +
        //             //         'wl_referredby=' + $scope.formData.comments + '&' +
        //             //         'wl_salesrep=' + $scope.profile.name,
        //             headers: {
        //                'Access-Control-Allow-Origin': '*',
        //                'Access-Control-Allow-Headers': 'Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With',
        //                'Access-Control-Allow-Methods': 'GET, POST, OPTIONS'
        //              }
        //           // url: 'http://www.crmtool.net/lp_NewLead.asp?',
        //           // params: { 
        //           //   wl_salesrep : $scope.profile.name,
        //           //   lp_Username : "NexgenWeb", 
        //           //   lp_Password : "pDqEXCN2C35f",
        //           //   lp_CompanyID : "12498",
        //           //   lp_Comments : $scope.formData.comments,
        //           //   wl_leadsource : "2",
        //           //   lp_UserField6 : "Referral From Ap",
        //           //   lp_Company  : $scope.formData.rBusinessName[index],
        //           //   lp_ContactFirstName : $scope.formData.rContactPerson[index],
        //           //   lp_Phone : $scope.formData.rPhoneNumber[index],
        //           //   wl_referredby : "Referred by : " + $scope.formData.cName + ' ' + $scope.formData.cContact + ' ' + $scope.formData.configAppTelNo
                    
        //           // }
        //         }).then(function successCallback(response) {
        //                 $scope.myRes = response.data;
        //                 $scope.statuscode = response.status;
        //           }, function errorCallback(response) {
        //               $scope.myRes = response.statusText;
        //           });
        //     } else {
        //          //console.log(index);
        //     }
        // });
    } 

    $scope.downloadPdf = function() {
        var userProfile =  Scopes.get('DefaultCtrl').profile;
        var formHtmlData = new Array();
        var fileNameArr = new Array();
        //$interval.cancel(saveDraft);

        $scope.formData.signedTimestamps = $window.signedTimeStamps;

        if (!angular.isDefined($scope.htmlData) || angular.equals({},$scope.htmlData)) {
          FlashMessage.setMessage({success:false, message:"No Forms Selected"});
          return;
        }
        // $("#loading").show();
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;

        angular.forEach($scope.htmlData, function(value, key) {
            var nametail = new Date().getTime();
            var rand = Math.floor((Math.random()*1000)+10);
            var fileName = key + rand + nametail + ".pdf";
            fileNameArr.push(fileName);
            $("#downloadPdfBn").prop('disabled',true);

            // In case of forms with location fields.
            if ($scope.locationAvailableTemps.indexOf(key) !== -1 && $scope.items[key].length > 1) {
                angular.forEach(value, function(data, index) {
                    rand = Math.floor((Math.random() * 1000) + 10);
                    fileName = key + rand + nametail + ".pdf";
                    formHtmlData.push({html: data, fileName : fileName});
                });
            } else {
                formHtmlData.push({html: value, fileName : fileName});
            }
        });

        var pdf = Form.update({
            htmlData :formHtmlData,
            userDetails : { name : $scope.formData.dName,
                            email :$scope.formData.cEmail},
            account: {name: userProfile.name, email: userProfile.email}},
            function(data) {
            if($rootScope.editId) {
               var editId = $rootScope.editId;
               $rootScope.editId = null;
            }
            Form.save({ data : $scope.formData,
                        user : userProfile.id,
                        editId : editId,
                        fileName : fileNameArr,
                        type:"Completed"}, function(data) {
                if (data.success) {
                    $scope.formData = {};
                    // $("#loading").hide();
                    $scope.loading = false;
                    // FlashMessage.setMessage({
                    // success:true,
                    // message:"The forms will be sent to your email shortly."});
                    alert("The forms will be sent to your email shortly.");                    
                    $window.location.reload();
                }
            });
        });
    }

    $scope.formGroups = {
        telecomService      : [
                                {file:'billing_app',id : 'configApp'},
                                {file:'monitoring_solution',id : 'monitoring_solution'} 
                              ],
        financeApplication  : [
                                {file:'rental',id : 'rental'},
                                {file: 'leasing', id : 'leasing'},
                                {file: 'chattle', id : 'chattle'}
                              ],
        orderSpec           : [
                                {file: 'single_order_spec', id : 'singleOrder'},
                                {file: 'copierAgreement', id: 'copierAgreement'}
                              ],
        voice               : [
                                {file: 'nbf_cap', id : 'voiceCap'},
                                {file: 'voice_comp', id : 'completeOffice'},
                                {file: 'voice_essentials_cap', id : 'voiceEssential'},
                                {file: 'voice_solution_standard', id : 'voiceSolution'},
                                {file: 'voice_solution_untimed', id : 'voiceUt'},
                                {file: 'nbf_std', id : 'focusStandard'}
                              ],
        data                : [
                                {file: 'data_adsl', id : 'adsl2'},
                                {file: 'ip_midband', id : 'ipMidband'},
                                {file: 'ip_midbandunli', id : 'ipMidbandUnli'},
                                {file: 'nbn', id : 'nbnMonthly'},
                                {file: 'ethernet', id: 'ethernet'},
                                {file: 'fibre', id : 'fibre'}
                              ],
        mobile              : [
                                {file: 'mobile_mega', id : 'mobileCap'},
                                {file: 'mobile_ut', id : 'mobileUt'},
                                {file: 'mobile_wireless', id : 'mobileWireless'},
                                {file: 'telstraUntimed', id: 'telstraUntimed'},
                                {file: 'telstraWireless', id: 'telstraWireless'}
                              ],
        r1300               : [
                                {file: '1300', id : 'rate131300'},
                                {file: '1300_discounted', id : 'rateDis131300'}
                              ]
    }

    $rootScope.$on('ngDialog.opened', function (e, $dialog) {
        $window.scrollTo(0, angular.element(".ngdialog-content").offsetTop);
    });

    $scope.checkAccount = function(){
      angular.forEach($scope.formData.accounts,function(data){
        if (data.configCarrier=='')
          return false;
        if (data.configAccno=='')
          return false;
      });
      return true;
    }

    $scope.signDocument = function(templateName, formData) {
        $scope.template = "templates/forms/"+templateName+".html?t=" + _version;
        //setting the values changed by Script.
        formData.dateVal = $("#dateVal").val();
        formData.pBirth  = $("#pBirth").val();
        formData.pIAddress = $("#pIAddress").val();
        formData.pIState = $("#pIState").val();
        formData.pIPostcode =  $("#pIPostcode").val();
        formData.aPeriod = 'monthly';
        formData.pISuburb =  $("#pISuburb").val();
        $rootScope.draftMode = false;

        // Check if exists empty location fields.
        var comLocationObj = $('input.' + templateName);
        var emptyLocationCnt = 0;
        angular.forEach(comLocationObj, function(item, index) {
            var location = $(item).val();
            if (!location) {
                emptyLocationCnt ++;                
            }
        });

        if (emptyLocationCnt) {
            alert("Please ensure that all locations are filled-up properly.")
            return;
        }

        $(".reAssign").each(function() {
            var id = $(this).prop("id");
            if (id=='alarmPlans'){
                var alarmPlan = $scope.formData.alarmPlans;
                formData[id] = $(this).val();
                $scope.formData.alarmPlans = alarmPlan;
            } else {
                formData[id] = $(this).val();
            }
        });

        formData["type"] = {};

        $("input[name=formTypes]:checked").each(function() {
            var id = $(this).prop("id");
            formData["type"][id] = $(this).prop("checked");
        });

        if (formData.type.configApp) {
            var telNo = new Array();
            $(".telNo").each(function() {
                  var item = $(this).find("input");
                  var idCount = item.data('count');
                  var telRange = $('#telItemRange' + '-' + idCount).val()!=''?' to '+$('#telItemRange' + '-' + idCount).val():'';
                  var obj = {
                                  tel : item[0].value + telRange 
                            };
                  telNo.push(obj);
            });
            $scope.formData.configAppTelNo = telNo;
            // var mobPort = new Array();
            // $(".mob-port-box").each(function() {
            //     var confAccHolder = $(this).find(".confAccHolder");
            //     var confMobNo = $(this).find(".confMobNo");
            //     var confProvider = $(this).find(".confProvider");
            //     var confAccNo = $(this).find(".confAccNo");
            //     var confPatPlan = $(this).find(".confPatPlan");
            //     var confDob = $(this).find(".confDob");
            //     var confDLicence = $(this).find(".confDLicence");

            //     var obj = {
            //                     confAccHolder : confAccHolder[0].value,
            //                     confMobNo : confMobNo[0].value,
            //                     confProvider : confProvider[0].value,
            //                     confAccNo : confAccNo[0].value,
            //                     confPatPlan : confPatPlan[0].value,
            //                     confDob : confDob[0].value,
            //                     confDLicence : confDLicence[0].value,
            //               };
            //     mobPort.push(obj);
            // });
            $scope.formData.configMobPort = $scope.configMobPort;

            // if ((!$scope.formData.configCarrier) || (!$scope.formData.configAccno)) {
        if (!$scope.checkAccount()){
              alert("Please  assign a Carrier or Account Number to the Billing Form.");
              return;
            }
        }

        if (formData.type.monitoring_solution){
            if (($scope.formData.advancedBussinessProject) && (!$scope.formData.panelType)){
                alert("Please complete the details for Panel Type.");
                return;
            }
        }

        if(formData.type.rental || formData.type.leasing ||formData.singleOrder) {
            formData["rental"] = {};
            var rental = parseInt(formData.aPayment.replace(/[^\d.]/g,''));
            formData["aGST"] = isNaN(rental) ? '0.00' : (rental*10/100).toFixed(2);
            formData["aTotal"] = isNaN(rental) ? '0.00' : (rental + rental*10/100).toFixed(2);
        }

        var schedule = new Array();
        var scheduleOptions = new Array();
        angular.forEach($scope.formData.schItems, function(data) {
            if (angular.isDefined($scope.formData.schOption)){
                if (angular.isDefined($scope.formData.schOption[data.count])) {
                    var obj = {
                                qty : $scope.formData.schQty[data.count],
                                desc :$scope.formData.schOption[data.count].item != '_' ? $scope.formData.schOption[data.count].item : ''
                            };
                    scheduleOptions.push(obj);
                }
            }
        });

        $(".schedule-goods").each(function() {
            var item = $(this).find("input");
            var obj = { qty : item[0].value,
                        desc :item[1].value,
                        //itemNo : item[2].value,
                        //serNo : item[3].value
                    };
            schedule.push(obj);
        });

        $scope.formData.scheduleOptions = scheduleOptions;
        $scope.formData.schedule = schedule;

        if($scope.editId) {
            $rootScope.editId = $scope.editId;
        }
        //console.log(formData);
        $rootScope.formData = formData;

        ngDialog.open({
            template: $scope.template,
            plan: true,
            controller : 'FormCtrl',
            width: 900,
            height: 600,
            scope: $scope,
            className: 'ngdialog-theme-default',
            preCloseCallback : function(value) {
                var allSigned = true;

                $(".signatureImg").each(function() {
                    console.log($(this).attr('id'),$(this).children("img").length);
                    if ($(this).children("img").length == 0) {
                        allSigned = false;
                        return;
                    }
                });
                if (!allSigned) {
                   $('#isSigned').val('F');
                   if (confirm("You have not signed all the fields.Do you want to exit")) {
                       return true;
                   }
                   return false;
                    $timeout(angular.noop());
                } else {
                    $('#isSigned').val('T');
                    // var button = $('#formData').find('a[href="#finish"]');
                    // button.show();
                    //alert($('#isSigned').val());                  // In case of forms with location fields.
                    if ($scope.locationAvailableTemps.indexOf(templateName) !== -1 && $scope.items[templateName].length > 1) {
                        $scope.htmlData[templateName] = [];
                        angular.forEach($scope.items[templateName], function(item, index) {
                            var clone = $('.ngdialog-content').clone();
                            clone.find('[class*="preview"]').each(function(divIndex,data){
                                //console.log(divIndex,data,index);  
                                if (index!=divIndex)  {
                                    clone.find('.preview-' + divIndex).remove();
                                    //console.log(clone.html());
                                }                        
                            });
                            //clone.find('.preview-' + index).remove();
                            var tableHtml = clone.html();
                            $scope.htmlData[templateName].push(tableHtml);
                        });
                    } else {
                        $scope.htmlData[templateName] = $(".ngdialog-content").html();
                    }

                    $scope[templateName] = true;
                    $timeout(angular.noop());
                }
            }
        });

        $rootScope.$on('ngDialog.opened', function (e, $dialog) {
            var newHash = 'anchor';
            if ($location.hash() !== newHash) {
                // set the $location.hash to `newHash` and
                // $anchorScroll will automatically scroll to it
                $location.hash('anchor');
            } else {
                // call $anchorScroll() explicitly,
                // since $location.hash hasn not changed
                $location.hash('anchor');
                $anchorScroll();
            }
        });
    }
    $scope.isDraftMode = function (){
        //alert($rootScope.draftMode);
        return $rootScope.draftMode;
    }
    $scope.isSigned = function(form, formName) {
        //alert(form,formName);
        if ($scope[form] && formName) {
            return true;
        } else {
            if (formName == false) {
                if ( angular.isDefined($scope.htmlData[form])) {
                    delete $scope.htmlData[form];
                }
                    $scope[form] = false;
                }

            return false;
        }
    }

    $scope.checkGroupSigned = function(forms, group) {
        var flag = false;
        if (angular.isDefined($scope.formGroups[group]) && angular.isDefined(forms)) {
            angular.forEach($scope.formGroups[group], function(item) {
                if (angular.isDefined(forms[item.id]) && forms[item.id] == true) {
                    if (forms[item.id] == true && !$scope[item.file]) {
                        flag = false;
                        return;
                    } else if (forms[item.id] == true && $scope[item.file]) {
                        flag = true;
                    }
                }
            });
        }
        return flag;
    }

    //console.log($scope.formData.schItems);

    $scope.addRentalGoods =  function(index) {
        // console.log($scope.schAddItemCount[index]);
        $scope.schAddItemCount[index]++;
        $scope.formData.schAddItems[index].push({count:$scope.schAddItemCount[index]+1});
        // console.log("Num:",$scope.schAddItemCount[index]);
        // console.log("Installation Location:",index,"Item Count",$scope.schAddItems[index],"Items",$scope.formData.schAddItemCount[index]);
        // $scope.scheduleCount++;


        //   var wrapped = '<div class="row">\n\
        //                     <div class="col-md-2 col-sm-2">\n\
        //                         <button class="btn btn-primary" id="schedule-'+$scope.scheduleCount+'">\n\
        //                            <i class="fa fa-times"></i>\n\
        //                         </button>\n\
        //                     </div>\n\
        //                     <div class="col-md-10 schedule-goods">\n\
        //                         <div class="col-md-3 col-sm-3">\n\
        //                             <div class="form-group">\n\
        //                                 <div class="col-xs-4">\n\
        //                                     <label class="control-label">Quantity</label>\n\
        //                                 </div>\n\
        //                                 <div class ="col-xs-8">\n\
        //                                     <input type="number" id="qty-item-'+$scope.scheduleCount+'" class="qunatity form-control">\n\
        //                                 </div>\n\
        //                             </div>\n\
        //                         </div>\n\
        //                         <div class="col-md-9 col-sm-9">\n\
        //                             <div class="form-group">\n\
        //                                 <div class="col-xs-3 text-right">\n\
        //                                         <label class="control-label">Description</label>\n\
        //                                 </div>\n\
        //                                 <div class ="col-xs-9">\n\
        //                                     <input type="text" class ="desc form-control">\n\
        //                                 </div>\n\
        //                             </div>\n\
        //                         </div>\n\
        //                     </div>\n\
        //                 </div>';
        // $(".rentalGoods").append(wrapped);
        // $("#schedule-"+$scope.scheduleCount).click(function() {
        //     $(this).parent().parent().remove();
        // });
        // $("#qty-item-"+$scope.scheduleCount).focus();
    };

    $scope.removeRentalGood = function(count,index) {
        var spliceIndex = $scope.formData.schAddItems[index].indexOf({count:count});
        $scope.formData.schAddItems[index].splice(spliceIndex,1);
    };

    $scope.removePortingAuth = function(index) {
        $scope.configMobPort.splice(index,1);
    }

    //console.log($scope.formData.schItems);
    

    $scope.$watch("formData.schOption", function(){

    });

    // console.log($scope.schItemCount[0],$scope.formData.schItems[0]);

    $scope. addRentalGoodsOption =  function(index) {
        $scope.schItemCount[index]++;
        $scope.formData.schItems[index].push({count:$scope.schItemCount[index]+1});
        // console.log("Installation Location:",index,"Item Count",$scope.schItemCount[index],"Items",$scope.formData.schItems[index]);
    };

    $scope.removeRentalGoodsOption = function(count,index) {
        // $scope.schItemCount[index]--;
        var spliceIndex = $scope.formData.schItems[index].indexOf({count:count});
        $scope.formData.schItems[index].splice(spliceIndex,1);
        // console.log("Installation Location:",index,"Item Count:",$scope.schItemCount[index],"Items",$scope.formData.schItems[index]);
    };


    //

    
    // Add location function
    $scope.addCompanyLocation = function(form_type) {
        if ($scope.items[form_type].length!=0){
            $scope.locationCount = $scope.items[form_type].length-1;
        }
        $scope.locationCount++;
        if (form_type=='voice_solution_standard'){
            $scope.formData.voiceSolutionChannel[$scope.locationCount] = 3;
            $scope.formData.voiceSolutionDID[$scope.locationCount] = 10;
            $scope.formData.bFSolutionDiscount[$scope.locationCount] = 0;
            $scope.formData.bFSolutionCallDiscount[$scope.locationCount] = 0;
        }
        if (form_type=='voice_solution_untimed'){
            $scope.formData.voiceUntimedChannel[$scope.locationCount] = 199;
            $scope.formData.voiceUntimedDID[$scope.locationCount] = 14.95;
            $scope.formData.bFUntimedDiscount[$scope.locationCount] = 0;
        }
        if (form_type=='voice_comp'){
            $scope.formData.voiceCompAnalouge[$scope.locationCount] = 0;
            $scope.formData.voiceCompBri[$scope.locationCount] = 0;
            $scope.formData.voiceCompPri[$scope.locationCount] = 0;
            $scope.formData.voiceCompDID[$scope.locationCount] = 0;
            $scope.formData.voiceCompBriDis[$scope.locationCount] = 0;
            $scope.formData.voiceCompPriDis[$scope.locationCount] = 0;
            $scope.formData.voiceCompCallDis[$scope.locationCount] = 0;
            $scope.formData.voiceCompAnalougeDisDSL[$scope.locationCount] = 0;
            $scope.formData.voiceCompAnalougeDis[$scope.locationCount] = 0;
        }
        if (form_type=='nbf_std'){
            $scope.formData.voiceStdChannel[$scope.locationCount] = 199;
            $scope.formData.voiceStdDID[$scope.locationCount] = 9.99;
            $scope.formData.bFStdDiscount[$scope.locationCount] = 0;
            $scope.formData.bFCallDiscount[$scope.locationCount] = 0;
        }
        if (form_type=='nbf_cap'){
            $scope.formData.voiceStd[$scope.locationCount] = 5;
            $scope.formData.dID[$scope.locationCount] = 10;
            $scope.formData.bFCapDiscount[$scope.locationCount] = 0;
        }
        if (form_type=='voice_essentials_cap'){
            $scope.formData.ipMidbandPlansVoice[$scope.locationCount] = $scope.ip_midband_plans[0];
            $scope.formData.ipMidbandDownloadVoice[$scope.locationCount] = $scope.download_plans[0];
            $scope.formData.voiceEssentialChannel[$scope.locationCount] = 196;
            $scope.formData.voiceEssentialDID[$scope.locationCount] = 9.99;
            $scope.formData.bECapDiscount[$scope.locationCount] = 0;
            $scope.formData.ipMidbandDis[$scope.locationCount] = 0;
            // $scope.formData.ipMidbandDownloadVoice[$scope.locationCount] = 0;
        } 
        if (form_type=='data_adsl'){
            // $scope.formData.adsl2Plans[$scope.locationCount] = 79;
            $scope.formData.adsl2Plans[$scope.locationCount] = $scope.adsl_plans[0];
            $scope.formData.adsl2Dis[$scope.locationCount] = 0;
            $scope.formData.voiceCompAnalougeDSL[$scope.locationCount] = 0;
            $scope.formData.voiceCompAnalougeDisDSL[$scope.locationCount] = 0;
        }
        if (form_type=='ip_midband'){
            $scope.formData.ipMidbandDownload[$scope.locationCount] = $scope.download_plans[1];
            $scope.formData.ipMidbandPlans[$scope.locationCount] = $scope.ip_midband_plans[1];
            $scope.formData.ipMidbandDis[$scope.locationCount] = 0;
        }
        if (form_type=='ip_midbandunli'){
            $scope.formData.ipMidbandUnliPlans[$scope.locationCount] = $scope.ip_midbandunli_plans[0];
            $scope.formData.ipMidbandUnliProf[$scope.locationCount] = $scope.professional_install[0];
            $scope.formData.ipMidbandUnliDis[$scope.locationCount] = 0;
        }
        if (form_type=='1300_discounted'){
            $scope.formData.rate131300Dis.qt1800[$scope.locationCount] = 0;
            $scope.formData.rate131300Dis.qt1300[$scope.locationCount] = 0;
            $scope.formData.rateDis131300.discount[$scope.locationCount] = 0;
        }
        if (form_type=='1300'){
            $scope.formData.rate131300.qt1800[$scope.locationCount] = 0;
            $scope.formData.rate131300.qt1300[$scope.locationCount] = 0;
            $scope.formData.rate131300.qt13[$scope.locationCount] = 0;
        }
        if (form_type=='mobile_wireless'){
            $scope.formData.mobileWirelessPlans[$scope.locationCount] = $scope.wireless_cap_plans[0];
            // $scope.formData.mobileWirelessPlans[$scope.locationCount] = 15;
            $scope.formData.mobileWirelessDis[$scope.locationCount] = 0;
        }        
        if (form_type=='fibre'){
            $scope.formData.fibreUtPlans[$scope.locationCount] = $scope.fibre_unli_plans[0];
            $scope.formData.fibreDis[$scope.locationCount] = 0;
        }
        // if (form_type=='adsl'){
        //     $scope.formData.adsl2Plans[$scope.locationCount] = $scope.adsl_plans[0];
        // }
        if (form_type=='ethernet'){
            $scope.formData.ethernetPlans[$scope.locationCount] = $scope.ethernet_plans[0];
            $scope.formData.ethernetDis[$scope.locationCount] = 0;
        }
        if (form_type=='nbnUnlimited'){
            $scope.formData.UnlimitedPlans[$scope.locationCount] = $scope.nbnUnlimited_plans[0];
            $scope.formData.nbnUnlimitedDis[$scope.locationCount] = 0;
            $scope.formData.voiceCompAnalougeDisNBNUnli[$scope.locationCount] = 0;
            $scope.formData.voiceCompAnalougeNBNUnli[$scope.locationCount] =0;
        }
        if (form_type=='telstraUntimed'){
            $scope.formData.telstraUntimedPlans[$scope.locationCount] = $scope.telstraUntimed_plans[0];
        }
        if (form_type=='telstraWireless'){
            $scope.formData.telstraWirelessPlans[$scope.locationCount] = $scope.telstraWireless_plans[0];
            $scope.formData.telstraWirelessDis[$scope.locationCount] = 0;
            $scope.formData.comLocation['telstraWireless'][$scope.locationCount] = $scope.formData.comLocation['telstraWireless'][0];
            $scope.formData.comSuburb['telstraWireless'][$scope.locationCount] = $scope.formData.comSuburb['telstraWireless'][0];
            $scope.formData.comZipCode['telstraWireless'][$scope.locationCount] = $scope.formData.comZipCode['telstraWireless'][0];
        }
        if (form_type=='schedule_of_goods'){
            $scope.formData.schItems[$scope.locationCount] = [{count:1}];
            $scope.schItemCount[$scope.locationCount] = 1;
            $scope.formData.schAddItems[$scope.locationCount] = [{count:1}];
            $scope.schAddItemCount[$scope.locationCount] = 1;
            //console.log($scope.formData.schItems);
        }
        $scope.items[form_type].push($scope.locationCount);
    };

    $scope.removeOptionsBlock = function(form_type, index) {
        var comLocationObj = $scope.formData.comLocation[form_type];
        comLocationObj.splice(index, 1);
        $scope.items[form_type].splice(index, 1);
        if (form_type[form_type]=='schedule_of_goods'){
            $scope.formData.schItems.splice(index,1);
        }
    };

    $scope.formData.wPosition = "Sales";
    
    if ($rootScope.viewFormData)    {
        $scope.formData = $rootScope.viewFormData.data;
        console.log($scope.formData.printers);

            if(angular.isUndefined($scope.formData.comLocation['nbf_cap']) ||angular.isUndefined($scope.formData.comLocation['schedule_of_goods']) ||
                angular.isUndefined($scope.formData.comLocation['nbf_std']) ||angular.isUndefined($scope.formData.comLocation['voice_solution_untimed']) ||
                angular.isUndefined($scope.formData.comLocation['voice_solution_standard']) ||angular.isUndefined($scope.formData.comLocation['voice_essentials_cap']) ||
                angular.isUndefined($scope.formData.comLocation['voice_comp']) ||angular.isUndefined($scope.formData.comLocation['1300']) ||
                angular.isUndefined($scope.formData.comLocation['nbnUnlimited']) ||angular.isUndefined($scope.formData.comLocation['ethernet']) ||
                angular.isUndefined($scope.formData.comLocation['fibre']) ||angular.isUndefined($scope.formData.comLocation['telstraWireless']) ||
                angular.isUndefined($scope.formData.comLocation['ip_midbandunli']) ||angular.isUndefined($scope.formData.comLocation['ip_midband']) ||
                angular.isUndefined($scope.formData.comLocation['mobile_wireless']) || angular.isUndefined($scope.formData.comLocation['data_adsl'])) 
            {
                alert("null");
            } else{
                for (var index=0;index<$scope.formData.comLocation['schedule_of_goods'].length;index++){
                    $scope.locationCount = $scope.items['schedule_of_goods'].length-1;
                    $scope.items['schedule_of_goods'][index] = index;

                    angular.forEach($scope.formData.schItems[index],function(data,ind){
                        // console.log("value",data,"index",ind);
                        $scope.schItemCount[index] = data.count-1;
                    });
                    // console.log("value1",$scope.formData.schAddItems[index]);
                    angular.forEach($scope.formData.schAddItems[index],function(add,indx){
                        // console.log("value1",add,"index1",indx);
                        $scope.schAddItemCount[index] = add.count-1;       
                    });
                }
                for (var index=0;index<$scope.formData.comLocation['nbf_cap'].length;index++){
                    $scope.locationCount = $scope.items['nbf_cap'].length-1;
                    $scope.items['nbf_cap'][index] = index;
                    }
                for (var index=0;index<$scope.formData.comLocation['nbf_std'].length;index++){
                    $scope.locationCount = $scope.items['nbf_std'].length-1;
                    $scope.items['nbf_std'][index] = index;
                    } 
                for (var index=0;index<$scope.formData.comLocation['voice_solution_untimed'].length;index++){
                    $scope.locationCount = $scope.items['voice_solution_untimed'].length-1;
                    $scope.items['voice_solution_untimed'][index] = index;
                    } 
                for (var index=0;index<$scope.formData.comLocation['voice_solution_standard'].length;index++){
                    $scope.locationCount = $scope.items['voice_solution_standard'].length-1;
                    $scope.items['voice_solution_standard'][index] = index;
                    } 
                for (var index=0;index<$scope.formData.comLocation['voice_essentials_cap'].length;index++){
                    $scope.locationCount = $scope.items['voice_essentials_cap'].length-1;
                    $scope.items['voice_essentials_cap'][index] = index;
                    } 
                for (var index=0;index<$scope.formData.comLocation['voice_comp'].length;index++){
                    $scope.locationCount = $scope.items['voice_comp'].length-1;
                    $scope.items['voice_comp'][index] = index;
                    } 
                for (var index=0;index<$scope.formData.comLocation['1300'].length;index++){
                    $scope.locationCount = $scope.items['1300'].length-1;
                    $scope.items['1300'][index] = index;
                    }
                for (var index=0;index<$scope.formData.comLocation['1300_discounted'].length;index++){
                    $scope.locationCount = $scope.items['1300_discounted'].length-1;
                    $scope.items['1300_discounted'][index] = index;
                    }
                for (var index=0;index<$scope.formData.comLocation['data_adsl'].length;index++){
                    $scope.locationCount = $scope.items['data_adsl'].length-1;
                    $scope.items['data_adsl'][index] = index;
                    }
                for (var index=0;index<$scope.formData.comLocation['nbnUnlimited'].length;index++){
                    $scope.locationCount = $scope.items['nbnUnlimited'].length-1;
                    $scope.items['nbnUnlimited'][index] = index;
                    }
                for (var index=0;index<$scope.formData.comLocation['ethernet'].length;index++){
                    $scope.locationCount = $scope.items['ethernet'].length-1;
                    $scope.items['ethernet'][index] = index;
                    }
                for (var index=0;index<$scope.formData.comLocation['fibre'].length;index++){
                    $scope.locationCount = $scope.items['fibre'].length-1;
                    $scope.items['fibre'][index] = index;
                    }
                for (var index=0;index<$scope.formData.comLocation['telstraWireless'].length;index++){
                    $scope.locationCount = $scope.items['telstraWireless'].length-1;
                    $scope.items['telstraWireless'][index] = index;
                    }
                for (var index=0;index<$scope.formData.comLocation['ip_midbandunli'].length;index++){
                    $scope.locationCount = $scope.items['ip_midbandunli'].length-1;    
                    $scope.items['ip_midbandunli'][index] = index;
                    }
                for (var index=0;index<$scope.formData.comLocation['ip_midband'].length;index++){
                    $scope.locationCount = $scope.items['ip_midband'].length-1;
                    $scope.items['ip_midband'][index] = index;
                    }
                for (var index=0;index<$scope.formData.comLocation['mobile_wireless'].length;index++){
                    $scope.locationCount = $scope.items['mobile_wireless'].length-1;
                    $scope.items['mobile_wireless'][index] = index;
                    }
            }

        $scope.editId = $rootScope.viewFormData.id;
         if (angular.isDefined($scope.formData.schedule)) {
            angular.forEach($scope.formData.schedule, function(item, count) {
                if (count == 0) {
                  $(".qunatity").val(item.qty);
                  $(".desc").val(item.desc);
                } else {
                  var wrapped = '<div class="row">\n\
                                            <div class="col-md-2 col-sm-2">\n\
                                                <button class="btn btn-primary" id="schedule-'+count+'">\n\
                                                   <i class="fa fa-times"></i> \n\
                                                </button></div>\n\
                                            <div class="col-md-10 schedule-goods">\n\
                                                <div class="col-md-3 col-sm-3">\n\
                                                    <div class="form-group">\n\
                                                        <div class="col-xs-4">\n\
                                                            <label class="control-label">Quantity</label>\n\
                                                        </div>\n\
                                                        <div class ="col-xs-8">\n\
                                                            <input type="number" value='+item.qty+' class="qunatity form-control">\n\
                                                        </div>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="col-md-9 col-sm-9">\n\
                                                    <div class="form-group">\n\
                                                        <div class="col-xs-3 text-right">\n\
                                                                <label class="control-label">Description</label>\n\
                                                        </div>\n\
                                                        <div class ="col-xs-9">\n\
                                                            <input type="text"  value='+item.desc+' class ="desc form-control">\n\
                                                        </div>\n\
                                                    </div>\n\
                                                </div>\n\
                                            </div>\n\
                                </div>';
                    $(".rentalGoods").append(wrapped);
                    $("#schedule-"+count).click(function() {
                        $(this).parent().parent().remove();
                    });
                    $scope.scheduleCount = count;
                }
            });
        }
        if (angular.isDefined($scope.formData.cState)) {
            $scope.formData.cState = $scope.formData.cState;
        }
        if (angular.isDefined($scope.formData.type)) {
            if ($scope.formData.type.configApp) {
                if (angular.isDefined($scope.formData.configAppTelNo)) {
                    angular.forEach($scope.formData.configAppTelNo, function(item,count) {
                      $scope.addTel();
                      $timeout(function() {
                          $("#telItemConfigApp-"+count).val(item.tel);
                      });
                    });
                }

                if (angular.isDefined($scope.formData.configMobPort)) {
                    $timeout(function() {
                        $scope.configMobPort = $scope.formData.configMobPort;
                    });
                }
                if ($scope.formData.config) {
                   if ($scope.formData.config.pstn) {
                       $scope.formData.config.pstn = true;
                   } else {
                        $scope.formData.config.pstn = false;
                   }
                   if ($scope.formData.config.lineHunt) {
                       $scope.formData.config.lineHunt = true;
                   }
                   if ($scope.formData.config.ISDN) {
                       $scope.formData.config.ISDN = true;
                   } else {
                        $scope.formData.config.pstn = false;
                   }
                   if ($scope.formData.config.indial) {
                       $scope.formData.config.indial = true;
                   }
                   if ($scope.formData.config.port ) {
                       $scope.formData.config.port = true;
                   }
                }
            }


        }

        $rootScope.viewFormData = null;
    }

    $("input[name=formTypes]").on("click", function() {
       if ($(this).prop("checked") === true) {
          var className = $(this).prop("class");
          var id = $(this).prop("id");
          if (className) {
              $("."+className).each(function() {
                 $(this).prop("checked",false);
              });
              $("#"+id).prop("checked", true);
          }
       }
    });

    $("#sameAddress").on("click", function() {
        var checked = $("#sameAddress").is(":checked");
        if (checked) {
            $("#pIAddress").val( $("#cAddress").val());
            $("#pIState").val( $("#cState").val());
            $("#pIPostcode").val( $("#cPostCode").val());
            $("#pISuburb").val( $("#cSuburb").val());
        } else {
            $("#pIAddress").val("");
            $("#pIState").val("");
            $("#pIPostcode").val("");
            $("#pISuburb").val("");
        }
        $scope.formData.pIAddress = $('#pIAddress').val();
        $scope.formData.pIState = $('#pIState').val();
        $scope.formData.pIPostcode = $('#pIPostcode').val();
        $scope.formData.pISuburb = $('#pISuburb').val();
        $scope.updateComLocations();
        $scope.updateComSuburbs();
        $scope.updateComPostCode();
    });

    $scope.load = function() {
        $scope.downloadPdf();
    }

    $scope.transformTelFields();
    /*var saveDraft =  $interval(function() {
       $scope.saveAsDraft($scope.formData);
    },300000);*/
});
Qms.controller("FormFieldCtrl", function($scope, $state, $rootScope) {
    $scope.formData = $rootScope.formData;

        $scope.load = function(formData) {
            $("#fields").validate();
            if ( $("#fields").valid() == true) {
                $rootScope.formData = formData;
             //  $state.go('rate_card');
            }
        }

});

Qms.controller("FormCtrl", function($scope, $filter, $timeout, $state, $rootScope, Form, Scopes, $locale,$q) {
    var user = Scopes.get('DefaultCtrl').profile; 
    // $scope.rates = [];
    if ($scope.isDraftMode()){
        $scope.userSign = false;
    } else {
        $scope.userSign = $rootScope.sign;
    }

    $scope.roundVal = function(val) {
        return val.toFixed(2);
    }
   
    /**
     * Retunrs true if form is signed otherwise false.
     * @param formName String name of the form as per the parent td element.
     */
    // $scope.isSigned = function(formName) {
    //     var isFound = false;
    //     if(!angular.isDefined($scope.formData.signedTimestamps)) {
    //         return false;
    //     }
    //     if( Object.prototype.toString.call( $scope.formData.signedTimestamps ) === '[object Array]' ) {
    //         $scope.formData.signedTimestamps.forEach(function(e) {
    //             if(e.form_name==formName) {
    //                 isFound = true;
    //                 return isFound;
    //             }
    //         });
    //     } else {
    //         if($scope.formData.signedTimestamps.form_name==formName) {
    //             isFound = true;
    //             return isFound;
    //         }
    //     }
    //     return isFound;
    // }

    /**
     * Returns the timestamp of the signature if exists.
     */
    $scope.getSignedTime = function(formName,userSign) {
        //console.log('getSigned');
        var time = '';
        if(!angular.isDefined($scope.formData.signedTimestamps)) {
            return 'Sign Here';
        }
        if( Object.prototype.toString.call( $scope.formData.signedTimestamps ) === '[object Array]' ) {
            $scope.formData.signedTimestamps.forEach(function(e) {
                if(e.form_name==formName) {
                    time = e.timestamp;
                }
            });
        } else {
            if($scope.formData.signedTimestamps.form_name==formName) {
                time = $scope.formData.signedTimestamps.timestamp;
            }
        }
        var currentDate = Date.parse(time);
        //console.log('curDate');
        //console.log(currentDate);
        var formatted = $filter('date')(time, 'HH:mm:ss dd-MM-yyyy');
        return formatted;
    }


    $scope.getCurrentDate = function(){
        var currentDate = new Date();   
        return (currentDate.getHours() < 10 ? "0" : "") + currentDate.getHours() + ":" + (currentDate.getMinutes() < 10 ? "0" : "") + currentDate.getMinutes() + ":" + (currentDate.getSeconds() < 10 ? "0" : "") + currentDate.getSeconds()+" "+currentDate.getDate()+"-"+(currentDate.getMonth()+1)+"-"+currentDate.getFullYear();
    }
    
    $scope.checkRange = function(val,lower_limit,upper_limit){
        //alert(val + '-' + lower_limit + '-' + upper_limit);
        if (upper_limit==0){
            if (val>=lower_limit){
                return true;
            } else {
                return false;
            }
        } else {
            if (val >= lower_limit && val <= upper_limit){
                return true;
            } else {
                return false;
            }
        }
    }

    $scope.setCheckboxValue = function(model){
        //console.log(model,$scope.formData.config.pstn);
        if ((model=='pstn') && (!$scope.formData.config.pstn)){
            $scope.formData.config.pstn_qty = '0';
        }
        if ((model=='isdn') && (!$scope.formData.config.ISDN)){
            $scope.formData.config.isdn_qty = '0';
        }
    }
 

    
    $scope.financedAmount = function(rental_per_month, term) {
        var rental_per_month  = Number(rental_per_month.replace(/^\D+|\,+/g, ''));
        var term  = Number(term.replace(/^\D+|\,+/g, ''));
             
        // var term_rates = $scope.getRates(term,'rental');

        var subrate = 0;
        var rates = [];
        var found = 0;
        $rootScope.rates[0].rate
        var financeAmt = 0;        
        $.each ($rootScope.rates,function(index,value){
            subrate = (rental_per_month/value.rate)*1000; 
            //rates.push(subrate);
            if($scope.checkRange(subrate,value.lowerLimit,value.upperLimit)){
              financeAmt = subrate;
            }
            console.log
        });

          // terms[24] = [55.143,49.51,49.12,48.52,47.97];
          // terms[36] = [39.336,35.15,34.69,34.2,33.7];
          // terms[48] = [31.702,27.97,27.53,27.18,26.90];
          // terms[60] = [26.477,23.75,23.25,22.95,22.72];
          // var financeAmt = 0;
          // if ($scope.checkRange(rates[0],1,9999.99)){
          //     financeAmt = rates[0];
          // }

          // if ($scope.checkRange(rates[1],10000,19999.99)){
          //     financeAmt = rates[1];
          // }

          // if ($scope.checkRange(rates[2],20000,34999.99)){
          //     financeAmt = rates[2];
          // }

          // if ($scope.checkRange(rates[3],35000,49999.99)){
          //     financeAmt = rates[3];
          // }

          // if ($scope.checkRange(rates[4],50000,0)){
          //     financeAmt = rates[4];
          // } 

          return $scope.roundVal(financeAmt).toLocaleString();
        }

    $scope.leasedAmount = function(lease_per_month, term) {
        var lease_per_month  = Number(lease_per_month.replace(/^\D+|\,+/g, ''));
        var term  = Number(term.replace(/^\D+|\,+/g, ''));

        // var terms = [];
        // terms[24] = [55.627,49.95,49.56,49.02,49.02];
        // terms[36] = [40.469,35.62,35.15,34.66,34.66];
        // terms[48] = [32.956,28.46,28.02,27.65,27.65];
        // terms[60] = [28.292,24.2,23.75,23.45,23.45];

        var subrate = 0;
        var rates = [];
        $rootScope.rates[0].rate
        var found = 0;
        var leaseAmt = 0;
        // $.each (terms[term],function(index,value){
        //     subrate = (lease_per_month/value.rate)*1000; 
        //     // rates.push(subrate); 
        //     if ($scope.checkRange(rates[0],1,9999.99)){
        //     leaseAmt = rates[0];
        // }
        // });
        $.each ($rootScope.rates,function(index,value){
            subrate = (lease_per_month/value.rate)*1000; 
            //rates.push(subrate);
            if($scope.checkRange(subrate,value.lowerLimit,value.upperLimit)){
              leaseAmt = subrate;
            }
            console.log
        });

        //console.log(rates);

        // var leaseAmt = 0;
        // if ($scope.checkRange(rates[0],1,9999.99)){
        //     leaseAmt = rates[0];
        // }

        // if ($scope.checkRange(rates[1],10000,19999.99)){
        //     leaseAmt = rates[1];
        // }

        // if ($scope.checkRange(rates[2],20000,34999.99)){
        //     leaseAmt = rates[2];
        // }

        // if ($scope.checkRange(rates[3],35000,49999.99)){
        //     leaseAmt = rates[3];
        // }

        // if ($scope.checkRange(rates[4],50000,0)){
        //     leaseAmt = rates[4];
        // } 

        //console.log(leaseAmt);
        // if (rental_amt >=2000 && rental_amt <= 9999.99) {
        //     bracket = 1;
        // } else if (rental_amt >=10000 && rental_amt <= 19999.99) {
        //     bracket = 2;
        // } else if (rental_amt >=20000 && rental_amt <= 34999.99) {
        //     bracket = 3;
        // } else if (rental_amt >= 35000 && rental_amt <= 49999.99) {
        //     bracket = 4;
        // } else if (rental_amt >=50000 && rental_amt <= 199999) {
        //     bracket = 5;
        // }


        // if (bracket == 1) {
        //     if (term == 24) {
        //         constVal = 55.14;
        //     } else if (term == 36) {
        //          constVal = 39.34;
        //     } else if (term == 48) {
        //          constVal = 31.70;
        //     } else if (term == 60) {
        //          constVal = 26.48;
        //     }

        // } else if (bracket == 2) {
        //     if (term == 24) {
        //         constVal = 49.51;
        //     } else if (term == 36) {
        //          constVal = 35.15;
        //     } else if (term == 48) {
        //          constVal = 27.97;
        //     } else if (term == 60) {
        //          constVal = 23.75;
        //     }

        // } else if(bracket == 3) {
        //     if (term == 24) {
        //         constVal = 49.12;
        //     } else if (term == 36) {
        //          constVal = 34.69;
        //     } else if (term == 48) {
        //          constVal = 27.53;
        //     } else if (term == 60) {
        //          constVal = 23.25;
        //     }

        // } else if(bracket == 4)  {
        //     if (term == 24) {
        //         constVal = 48.52;
        //     } else if (term == 36) {
        //          constVal = 34.20;
        //     } else if (term == 48) {
        //          constVal = 27.97;
        //     } else if (term == 60) {
        //          constVal = 22.95;
        //     }

        // } else if (bracket == 5) {
        //     if (term == 24) {
        //         constVal = 47.97;
        //     } else if (term == 36) {
        //          constVal = 33.70;
        //     } else if (term == 48) {
        //          constVal = 26.90;
        //     } else if (term == 60) {
        //          constVal = 22.72;
        //     }
        // }

        //financeAmt = $scope.roundVal(rental_per_month * 1000/constVal).toLocaleString();
        //alert(financeAmt);
        return $scope.roundVal(leaseAmt).toLocaleString();
    }

    $scope.rentofferedAmount = function(rentoffer_per_month, term) {
        var rentoffer_per_month  = Number(rentoffer_per_month.replace(/^\D+|\,+/g, ''));
        var term  = Number(term.replace(/^\D+|\,+/g, ''));
        var rentoffer_amt = rentoffer_per_month * term;

        var constVal = 0;
        var bracket = 0;

        if (rentoffer_amt >=2000 && rentoffer_amt <= 4999.99) {
            bracket = 1;
        } else if (rentoffer_amt >=5000 && rentoffer_amt <= 9999.99) {
            bracket = 2;
        } else if (rentoffer_amt >=10000 && rentoffer_amt <= 19999.99) {
            bracket = 3;
        } else if (rentoffer_amt >=20000 && rentoffer_amt <= 99999.99) {
            bracket = 4;
        // } else if (rentoffer_amt >=50000 && rentoffer_amt <= 99999) {
        //     bracket = 5;
        }

        if (bracket == 1) {
            if (term == 48) {
                constVal = 31.68;
            } else if (term == 60) {
                 constVal = 26.20;
            }

        } else if (bracket == 2) {
            if (term == 48) {
                constVal = 31.05;
            } else if (term == 60) {
                 constVal = 25.35;
            }

        } else if(bracket == 3) {
            if (term == 48) {
                constVal = 30.45;
            } else if (term == 60) {
                 constVal = 25.09;
            }

        } else if(bracket == 4)  {
            if (term == 48) {
                constVal = 30.31;
            } else if (term == 60) {
                 constVal = 24.82;
            }
        }

       //console.log(constVal);
       rentofferAmt = $scope.roundVal(rentoffer_per_month * 1000/constVal).toLocaleString();
       return rentofferAmt;
    }


    if (!angular.isDefined($scope.formData.aTermNew) ) {
        $scope.formData.aTermNew = 60;
    }

    if(angular.isDefined($scope.formData.type.voiceUt) && $scope.formData.type.voiceUt) {
        var suToE = 9.95;
        var suMob = 7.95;
        var rateStdUt = {
          3 : 199,
          4 : 259,
          5 : 319,
          6 : 379,
          7 : 439,
          8 : 499,
          9 : 559,
          10 : 619,
          11 : 679,
          12 : 739,
          13 : 799,
          14 : 859,
          15 : 919,
          16 : 979,
          17 : 1039,
          18 : 1099,
          19 : 1159,
          20 : 1219,
          21 : 1279,
          22 : 1339,
          23 : 1399,
          24 : 1459,
          25 : 1519,
          26 : 1579,
          27 : 1639,
          28 : 1699,
          29 : 1759,
          30 : 1819,
        };

        var sUDID = {
            10 : 14.95,
            50 : 24.95,
            100: 34.95
        }

        var monthlyTotalRate = [];
        angular.forEach($scope.items['voice_solution_untimed'], function(item, index) {
            if (!$scope.formData.voiceUntimedFaxToEmail[index] || angular.isUndefined($scope.formData.voiceUntimedFaxQty[index])) {
                $scope.formData.voiceUntimedFaxQty[index] = 0;
            }
            
            if (!$scope.formData.voiceUntimedMobility[index] || angular.isUndefined($scope.formData.voiceUntimedMobQty[index])) {
                $scope.formData.voiceUntimedMobQty[index] = 0;
            }

            var total = suToE * $scope.formData.voiceUntimedFaxQty[index] + suMob * $scope.formData.voiceUntimedMobQty[index] + sUDID[$scope.formData.voiceUntimedDID[index]] + rateStdUt[$scope.formData.voiceUntimedChannel[index]];
            
            monthlyTotalRate.push(total);
        });

        $scope.voiceUt = {
            template : "templates/forms/voice_solution_untimed.html?t=" + _version,
            rate : rateStdUt,
            sfToE : suToE,
            sMob : suMob,
            sDID : sUDID,
            totalRate : monthlyTotalRate
        };
    }

    if($scope.formData.type.voiceSolution) {

        var ssToE = 9.95;
        var ssMob = 7.95;
        var rateStdSolution = {
            3 : 149,
            4 : 159,
            5 : 169,
            6 : 179,
            7 : 189,
            8 : 199,
            9 : 209,
            10 : 219,
            11 : 229,
            12 : 239,
            13 : 249,
            14 : 259,
            15 : 269,
            16 : 279,
            17 : 289,
            18 : 299,
            19 : 309,
            20 : 319,
            21 : 329,
            22 : 339,
            23 : 349,
            24 : 359,
            25 : 369,
            26 : 370,
            27 : 389,
            28 : 399,
            29 : 409,
            30 : 419,
        };

        var sSolutionDID = {
            10 : 14.95,
            50 : 24.95,
            100: 34.95
        };
        
        var monthlyTotalRate = [];
        angular.forEach($scope.items['voice_solution_standard'], function(item, index) {
            if (!$scope.formData.voiceSolutionFaxToEmail[index] || angular.isUndefined($scope.formData.voiceSolutionFaxQty[index])) {
                $scope.formData.voiceSolutionFaxQty[index] = 0;
            }
            
            if (!$scope.formData.voiceSolutionMobility[index] || angular.isUndefined($scope.formData.voiceSolutionMobQty[index])) {
                $scope.formData.voiceSolutionMobQty[index] = 0;
            }

            var total = ssToE * $scope.formData.voiceSolutionFaxQty[index] + ssMob * $scope.formData.voiceSolutionMobQty[index] + sSolutionDID[$scope.formData.voiceSolutionDID[index]] + rateStdSolution[$scope.formData.voiceSolutionChannel[index]];
            
            monthlyTotalRate.push(total);
        });

        $scope.voiceSolution = {
            template : "templates/forms/voice_solution_standard.html?t=" + _version,
            rate : rateStdSolution,
            sfToE : ssToE,
            sMob : ssMob,
            sDID : sSolutionDID,
            totalRate : monthlyTotalRate
        };
    }

    if($scope.formData.type.voiceCap) {
        var fToE = 9.95;
        var mob = 7.95;
        var rate = {
            3 : 50,
            5 : 150,
            6 : 170,
            8 : 210,
            10: 300,
            12: 350,
            14: 400,
            16: 450,
            18: 500,
            20: 600,
            25: 800
        };
        var cap = {
            3 : 99,
            5 : 99,
            6 : 99,
            8 : 99,
            10: 99,
            12: 99,
            14: 79,
            16: 79,
            18: 79,
            20: 79,
            25: 79

        };
        var dID = {
            10 : 9.99,
            20 : 15.99,
            50 : 19.99,
            100: 25.99
        }
        
        var monthlyTotalRate = [];

        angular.forEach($scope.items['nbf_cap'], function(item, index) {
            if (!$scope.formData.voiceFaxToEmail[index] || angular.isUndefined($scope.formData.voiceFaxQty[index])) {
                $scope.formData.voiceFaxQty[index] = 0;
            }
            
            if (!$scope.formData.voiceMobility[index] || angular.isUndefined($scope.formData.voiceMobQty[index])) {
                $scope.formData.voiceMobQty[index] = 0;
            }

            var total = (cap[$scope.formData.voiceStd[index]] * $scope.formData.voiceStd[index]) +
                        fToE * $scope.formData.voiceFaxQty[index] + mob * $scope.formData.voiceMobQty[index]+
                        dID[$scope.formData.dID[index]];
            
            monthlyTotalRate.push(total);
        });

        $scope.voiceCap = {
            template : "templates/forms/nbf_cap.html?t=" + _version,
            rate : rate,
            fToE : fToE,
            mob : mob,
            dID : dID,
            cap : cap,
            total: monthlyTotalRate
        };
    }

    if ($scope.formData.type.copierAgreement){
        $scope.copierAgreement = {
            template : "templates/forms/copierAgreement.html?t=" + _version
        }
    }

    if ($scope.formData.type.singleOrder) {
          $scope.formData.aTermNew = $scope.formData.aTermNew.toString();
          //console.log($scope.formData.aTermNew);
          if ($scope.formData.aTermNew!='Outright'){
            var financeAmount = $scope.financedAmount($scope.formData.aPayment, $scope.formData.aTermNew);
            //alert(financeAmount);
            var leaseAmount = $scope.leasedAmount($scope.formData.aPayment, $scope.formData.aTermNew);
          }
          var rentofferAmount = $scope.rentofferedAmount($scope.formData.aPayment, $scope.formData.aTermNew);

        // $scope.singleOrder = {
        //     template : "templates/forms/single_order_spec.html",
        //     financeAmount : !isNaN(financeAmount) ? financeAmount : 0,
        //     leaseAmount : !isNaN(leaseAmount) ? leaseAmount : 0,
        //     rentofferAmount : !isNaN(rentofferAmount) ? rentofferAmount : 0
        // }

        $scope.singleOrder = {
            template : "templates/forms/single_order_spec.html?t=" + _version,
            financeAmount : financeAmount,
            leaseAmount : leaseAmount,
            rentofferAmount : rentofferAmount
        }

    }

    if($scope.formData.type.focusStandard) {
        var sfToE = 9.95;
        var sMob = 7.95;
        var rateStd = {
            5 : 199,
            6 : 219,
            10: 249,
            20: 299,
            30: 349
        };
        var sDID = {

            10 : 9.99,
            20 : 15.99,
            50 : 19.99,
            100: 25.99
        }

        var monthlyTotalRate = [];
        angular.forEach($scope.items['nbf_std'], function(item, index) {
            if (!$scope.formData.voiceStdFaxToEmail[index] || angular.isUndefined($scope.formData.voiceStdFaxQty[index])) {
                $scope.formData.voiceStdFaxQty[index] = 0;
            }

            if (!$scope.formData.voiceStdMobility[index] || angular.isUndefined($scope.formData.voiceStdMobQty[index])) {
                $scope.formData.voiceStdMobQty[index] = 0;
            }

            var total = sfToE * $scope.formData.voiceStdFaxQty[index] + sMob * $scope.formData.voiceStdMobQty[index] + rateStd[$scope.formData.voiceStdChannel[index]] + sDID[$scope.formData.voiceStdDID[index]];

            monthlyTotalRate.push(total);
        });

        $scope.voiceStd = {
            template : "templates/forms/nbf_std.html?t=" + _version,
            rate : rateStd,
            sfToE : sfToE,
            sMob : sMob,
            sDID : sDID,
            totalRate : monthlyTotalRate
        };
    }

    if ($scope.formData.type.voiceEssential) {
        var rateEssential = {
            4 : 196,
            6 : 294,
            8 : 392,
            10 : 490,
            12 : 588
        };

        var eDID = {
            10 : 9.99,
            20 : 15.99,
            50 : 19.99,
            100 : 25.99
        };

        var monthlyTotalRate = [];
        var totalDiscount = []
        //ip Midband Plans
        var ipMidbandMonthlyRate = [];
        var ipMidbandPlans = [];
        var ipMidbandDownloads = [];
        //console.log($scope.formData.ipMidbandPlans);
        //console.log($scope.formData.ipMidbandDownload);
        angular.forEach($scope.items['voice_essentials_cap'], function(item, index) {
            var total = rateEssential[$scope.formData.voiceEssentialChannel[index]] + eDID[$scope.formData.voiceEssentialDID[index]];
            monthlyTotalRate.push(total);
            var totalIpFee = $scope.formData.ipMidbandPlansVoice[index].price+$scope.formData.ipMidbandDownloadVoice[index].price;
            var discount = parseFloat($scope.formData.ipMidbandDis[index]) + parseFloat($scope.formData.bECapDiscount[index]);
            ipMidbandPlans.push($scope.formData.ipMidbandPlansVoice[index]);
            ipMidbandDownloads.push($scope.formData.ipMidbandDownloadVoice[index]);
            ipMidbandMonthlyRate.push(totalIpFee);
            totalDiscount.push(discount);
        });

        $scope.voiceEssential = {
            template: "templates/forms/voice_essentials_cap.html?t=" + _version,
            rate : rateEssential,
            eDID : eDID,
            totalRate : monthlyTotalRate,
            plan : ipMidbandPlans,
            download : ipMidbandDownloads,
            ipTotalRate : ipMidbandMonthlyRate,
            totalDiscount : totalDiscount
        }
    }

    if($scope.formData.type.completeOffice) {
        var cfToE = 9.95;
        var cMob = 7.95;

        var priRates = {
            0 : 0,
            10: 299.85,
            20:499.85,
            30:699.85
        };

        var cDID = {
            10 : 9.99,
            20 : 15.99,
            50 : 19.99,
            100: 49.95
        };

        var monthlyTotalRate = [];
        var monthlyAnalogue = [];
        var monthlyBri = [];
        var monthlyPri = [];
        angular.forEach($scope.items['voice_comp'], function(item, index) {
            var total = 0;

            if (!$scope.formData.voiceCompFaxToEmail[index] || angular.isUndefined($scope.formData.voiceCompFaxQty[index])) {
                $scope.formData.voiceCompFaxQty[index] = 0;
            }
            
            if (!$scope.formData.voiceCompMobility[index] || angular.isUndefined($scope.formData.voiceCompMobQty[index])) {
                $scope.formData.voiceCompMobQty[index] = 0;
            }
            monthlyAnalogue.push(39.95 * $scope.formData.voiceCompAnalouge[index]);
            monthlyBri.push(99.85 * $scope.formData.voiceCompBri[index]);
            monthlyPri.push(priRates[$scope.formData.voiceCompPri[index]]);
            total = (39.95 * $scope.formData.voiceCompAnalouge[index]) + (99.85 * $scope.formData.voiceCompBri[index]) + priRates[$scope.formData.voiceCompPri[index]] + (cfToE * $scope.formData.voiceCompFaxQty[index]) + (cMob * $scope.formData.voiceCompMobQty[index]);

            if ($scope.formData.voiceCompDID[index]) {
                total += cDID[$scope.formData.voiceCompDID[index]];
            }

            // Calcuate the total plan fee based on discount amount.
            if (angular.isDefined($scope.formData.voiceCompAnalougeDis[index])) {
                total -= $scope.formData.voiceCompAnalougeDis[index];
            }

            if (angular.isDefined($scope.formData.voiceCompBriDis[index])) {
                total -= $scope.formData.voiceCompBriDis[index];
            }

            if (angular.isDefined($scope.formData.voiceCompPriDis[index])) {
                total -= $scope.formData.voiceCompPriDis[index];
            }

            monthlyTotalRate.push(total);
        });

        $scope.voiceComp = {
            template : "templates/forms/voice_comp.html?t=" + _version,
            cfToE : cfToE,
            cMob : cMob,
            cDID : cDID,
            total: monthlyTotalRate,
            analogueFee : monthlyAnalogue,
            briFee : monthlyBri,
            priFee : monthlyPri
        };
    }

    if ($scope.formData.type.adsl2) {
        var monthlyTotalRate = [];
        var analoguePrice = [];
        var discount = [];
        
        angular.forEach($scope.items['data_adsl'], function(item, index) {
            var total = $scope.formData.adsl2Plans[index].price;
            var analogueAmt = $scope.formData.voiceCompAnalougeDSL[index]*39.95;
            if(angular.isUndefined($scope.formData.voiceCompAnalougeDisDSL)){
                $scope.formData.voiceCompAnalougeDisDSL = [];
                $scope.formData.voiceCompAnalougeDisDSL[index] = 0;
            }
            var planDis = $scope.formData.voiceCompAnalougeDisDSL[index] + $scope.formData.adsl2Dis[index];
            var totalRate = (total+analogueAmt);
            monthlyTotalRate.push(totalRate);
            analoguePrice.push(analogueAmt);
            discount.push(planDis);
        });

        $scope.dataAdsl = {
            template : "templates/forms/data_adsl.html?t=" + _version,
            totalFee : monthlyTotalRate,
            analoguePrice : analoguePrice,
            discount : discount,
            rate : rate
        };
    }

    if ($scope.formData.type.telstraUntimed) {
        var monthlyTotalRate = 0;
        //console.log($scope.formData.telstraUntimedDis);
        var discount = isNaN($scope.formData.telstraUntimedDis) || $scope.formData.telstraUntimedDis == null?0:parseFloat($scope.formData.telstraUntimedDis);
        var ratePerMob = 0;
        var dataBoltOnRate = 0;
        angular.forEach($scope.telstraPlans,function(item,index){
            ratePerMob = $scope.telstraUntimed_plans[index].price * item;
            monthlyTotalRate += ratePerMob;
        });

        angular.forEach($scope.boltOnPlans,function(item,index){
            dataBoltOnRate = $scope.data_bolt_on_plans[index].price * item;
            monthlyTotalRate += dataBoltOnRate;
        });

        $scope.telstraUntimed = {
            template : "templates/forms/telstraUntimed.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
        };
    }

    if ($scope.formData.type.telstraWireless){
        var monthlyTotalRate = [];
        var discount = [];
        angular.forEach($scope.items['telstraWireless'],function(item,index){
            var total = $scope.formData.telstraWirelessPlans[index].price;
            var planDis = $scope.formData.telstraWirelessDis[index];
            monthlyTotalRate.push(total);
            discount.push(planDis);
        });

        $scope.telstraWireless = {
            template : "template/forms/telstraWireless.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
        };
    }

    if ($scope.formData.type.ethernet){
        var monthlyTotalRate = [];
        var discount = [];
        angular.forEach($scope.items['ethernet'],function(item,index){
            monthlyTotalRate.push($scope.formData.ethernetPlans[index].price);
            if(angular.isDefined($scope.formData.ethernetDis[index])){
                discount.push($scope.formData.ethernetDis[index]);
            }
            else{
                $scope.formData.ethernetDis[index] = 0;
                discount.push($scope.formData.ethernetDis[index]);
            }
        });

        $scope.ethernet = {
            template : "templates/forms/ethernet.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
        }
    }

    if ($scope.formData.type.ipMidband) {
        var monthlyTotalRate = [];
        var discount = [];
        angular.forEach($scope.items['ip_midband'], function(item, index) {
            var totalIpFee = $scope.formData.ipMidbandPlans[index].price+$scope.formData.ipMidbandDownload[index].price;
            monthlyTotalRate.push(totalIpFee);
            discount.push($scope.formData.ipMidbandDis[index]);
        });

        $scope.ipMidband = {
            template : "templates/forms/ip_midband.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
        }
    }

    if ($scope.formData.type.fibre){
        var monthlyTotalRate = [];
        var discount = [];

        angular.forEach($scope.items['fibre'],function(item,index){
            monthlyTotalRate.push($scope.formData.fibreUtPlans[index].price);
            discount.push ($scope.formData.fibreDis[index]);
        });

        $scope.fibre = {
            template : "templates/form/fibre.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
        }
    }

    if ($scope.formData.type.ipMidbandUnli){
        var monthlyTotalRate = [];
        var discount = [];
        // var plans = [];
        // var prof_install = [];
        angular.forEach($scope.items['ip_midbandunli'],function(item,index){
            //var totalIpFee = $scope.formData.ipMidbandUnliPlans[index].price + $scope.formData.ipMidbandUnliProf[index].price;
            // plans.push($scope.formData.ipMidbandUnliPlans[index]);
            // prof_install.push($scope.formData.ipMidbandUnliProf[index]);
            monthlyTotalRate.push($scope.formData.ipMidbandUnliPlans[index].price);
            discount.push($scope.formData.ipMidbandUnliDis[index])
        });

        $scope.ipMidbandUnli = {
            template : "templates/forms/ip_midbandunli.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
            // plan : plans,
            // prof_install : prof_install
        }
    }

    if ($scope.formData.type.nbnMonthly) {
        var nbnPlan = {
            255 : {rate : 99, desc : "NBN 25Mbps/5Mbps*"},
            2510 : {rate : 109, desc : "NBN 25Mbps/10Mbps*"},
            5020 : {rate : 119, desc : "Up to 50/20Mbps**"},
            10040 : {rate : 129, desc : "NBN 100Mbps/40Mbps*"}
        };
        var nbnDownload = {
            100 :{plan : "100GB", rate : 39},
            150 :{plan : "150GB", rate : 49},
            200 : {plan : "200GB" , rate :59},
            300 :{plan : "300GB", rate : 69},
            500 : {plan : "500GB",rate : 79},
            1000 : {plan : "1000GB", rate : 99}
        };

        var plans = [];
        var downloads = [];
        var monthlyTotalRate = [];
        var totalDiscount = [];
        var analoguePrice = [];
        angular.forEach($scope.items['nbn'], function(item, index) {
            var totalNbnFee = nbnPlan[$scope.formData.nbnPlans[index]].rate + nbnDownload[$scope.formData.nbnDownload[index]].rate;
            var price = $scope.formData.voiceCompAnalougeNBNMonthly[index] * 39.95;
            monthlyTotalRate.push(totalNbnFee + price);
            plans.push(nbnPlan[$scope.formData.nbnPlans[index]]);
            downloads.push(nbnDownload[$scope.formData.nbnDownload[index]]);
            totalDiscount.push($scope.formData.voiceCompAnalougeDisNBNMonthly[index] + $scope.formData.nbnDis[index]);
            analoguePrice.push(price);
        });

        $scope.nbn = {
            template : "templates/forms/nbn.html?t=" + _version,
            totalFee : monthlyTotalRate,
            plan : plans,
            download : downloads,
            analoguePrice : analoguePrice,
            discount : totalDiscount
        }
    }

    if ($scope.formData.type.nbnUnlimitedPlans) {
        var monthlyTotalRate = [];
        var plans = [];
        var prices = [];
        var analoguePrice = [];
        var discount = [];
        angular.forEach($scope.items['nbnUnlimited'], function(item, index) {
            var totalNbnUnlimitedFee = $scope.formData.UnlimitedPlans[index].price;
            var price = $scope.formData.voiceCompAnalougeNBNUnli[index] * 39.95;
            monthlyTotalRate.push(totalNbnUnlimitedFee + price);
            discount.push($scope.formData.voiceCompAnalougeDisNBNUnli[index] + $scope.formData.nbnUnlimitedDis[index]);
            analoguePrice.push(price);
        });

        $scope.nbnUnlimitedPlans = {
            template : "templates/forms/nbnUnlimited.html?t=" + _version,
            totalFee : monthlyTotalRate,
            analoguePrice : analoguePrice,
            discount : discount
        }
    }

    $scope.addBlankSpace = function(count) {
        var spacer = ""
        for (var i = 0; i<count; i++) {
            spacer += '\xa0\xa0';
        }
        return spacer;
    }

    $scope.processFields = function(content, maxLimit) {
        var length = content.length;
        var space = $scope.addBlankSpace(maxLimit-(length*2));
        return content+space;
    }

    if (($scope.formData.type.monitoring_solution) || ($scope.formData.type.alarm_monitor)){
        
    }

    if ($scope.formData.type.configApp) {
        var rowWidth = 3;
        $scope.telRow = new Array();
        var tempRow = new Array();
        var tdCount = 0;

        //Put tel no 3 per row
        angular.forEach($scope.formData.configAppTelNo, function(data) {
            //alert('Billing Telephone');
            //console.log(data);
            if (tdCount < rowWidth) {
                tempRow.push(data.tel);
                tdCount++;
            }
            if (tdCount == rowWidth) {
                $scope.telRow.push(tempRow);
                tdCount = 0;
                tempRow = new Array();
            }
        });
        if (tdCount > 0 ) {
            $scope.telRow.push(tempRow);
        }

        if ($scope.formData.additionalServices){
            if (!$scope.formData.config.pstn){
                $scope.formData.config.pstn_qty = 0;
            }

            if (!$scope.formData.config.ISDN){
                $scope.formData.config.Isdn_qty = 0;
            }
        }
        
        $scope.configApp = {
            template : "templates/forms/billing_app.html?t=" + _version,
        }
    }

    if ($scope.formData.type.rental) {
        $scope.rental = {
           template : "templates/forms/rental.html?t=" + _version,
        };
    }
    if ($scope.formData.type.leasing) {
        $scope.leasing = {
           template : "templates/forms/leasing.html?t=" + _version,
        };
    }

    if ($scope.formData.type.chattle) {
        $scope.chattle = {
            template : "templates/forms/chattle.html?t=" + _version,
        }
    }
    
    if ($scope.formData.type.mobileCap) {
        var monthlyTotalRate = 0;
        var discount = isNaN($scope.formData.mobileCapDis) || $scope.formData.mobileCapDis == null?0:parseFloat($scope.formData.mobileCapDis);
        //console.log(discount);
        var ratePerMob = 0;

        angular.forEach($scope.mobileMegaPlans,function(item,index){
            ratePerMob = $scope.mobile_mega_plans[index].price * item;
            monthlyTotalRate += ratePerMob;
        });

        $scope.mobileCap = {
            template : "templates/forms/mobile_mega.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
        };

        // var mobCPlan = {
        //     39 : {desc:" $250 inc calls 500MB"},
        //     49 : {desc:" $2500 inc calls 2000MB"},
        //     59 : {desc:" $2500 inc calls 3000MB"}
        // }
        // $scope.mobileCap = {
        //    template : "templates/forms/mobile_mega.html",
        //    plan : mobCPlan
        // };
    }

    if ($scope.formData.type.mobileUt) {
        var monthlyTotalRate = 0;
        var discount = isNaN($scope.formData.mobileUtDis) || $scope.formData.mobileUtDis == null?0:parseFloat($scope.formData.mobileUtDis);
        //console.log(discount);
        var ratePerMob = 0;

        angular.forEach($scope.mobile4GUntimedPlans,function(item,index){
            ratePerMob = $scope.mobile_4G_Untimed_Calls[index].price * item;
            monthlyTotalRate += ratePerMob;
        });

        $scope.mobileUt = {
            template : "templates/forms/mobile_mega.html?t=" + _version,
            totalFee : monthlyTotalRate,
            discount : discount
        };

        // $scope.mobileUt = {
        //    template : "templates/forms/mobile_ut.html",
        // };
    }

    if ($scope.formData.type.mobileWireless) {
        $scope.mobileWireless = {
           template : "templates/forms/mobile_wireless.html?t=" + _version,
        };
    }

    if ($scope.formData.type.rate131300) {
        $scope.rate131300 = {
            template : "templates/forms/1300.html?t=" + _version,
        }
    }

    if ($scope.formData.type.rateDis131300) {
        $scope.rateDis131300 = {
            template : "templates/forms/1300_discounted.html?t=" + _version,
        }
    }

    $scope.oneAtATime = true;

    $scope.sign = function(id) {
       $("#testDi").dialog("open");
       $('#iAccept').attr('data-id', id);
       if (!$('#'+id).find('canvas').length) {
            $('#signatureBox').html("<span>Sign here!</span>&nbsp;&nbsp;<i class='fa fa-refresh'onclick=clearSg('signatureBox')></i>");
            $('#signatureBox').jSignature({color:"#000000",lineWidth:1,
                                    width :490, height :98,
                                    cssclass : "signature-canvas",
                                   "decor-color": 'transparent'
                                  });
            clicked = true;
        }
    }

    $scope.currentSign = 0;
    $scope.signNavigate = function(direction) {
        var signItems = angular.element(".signItem");
        var elementId = "";
        if (direction =="next") {
            if ($scope.currentSign == 0) {
               currentItem = signItems[$scope.currentSign];
               $scope.currentSign +=1;
            } else if ($scope.currentSign < signItems.length -1) {
                $scope.currentSign +=1;
                currentItem = signItems[$scope.currentSign];
            } else {
                currentItem = signItems[$scope.currentSign];
            }

        } else if (direction == "previous") {
            if ($scope.currentSign == 0) {
               currentItem = signItems[$scope.currentSign];
            } else if ($scope.currentSign > 0) {
                $scope.currentSign -=1;
                currentItem = signItems[$scope.currentSign];
            }

        }

        elementId = currentItem.getAttribute("id");

        var top = $("#"+elementId).position().top;
        $(window).scrollTop(top-300);
        $(".signItem").removeClass("selected-to-sign");
        $("#"+elementId).addClass("selected-to-sign");
       // $(document).scrollTo("#"+elementId, 500 , {offset: -$(window).height()/2});
    }

    $scope.downloadPdf = function() {
        angular.forEach($rootScope.formData.type, function(value,key) {
             var nametail = new Date().getTime();
             var rand = Math.floor((Math.random()*1000)+10);
             var fileName = key+rand+nametail+".pdf";
             $("#loading").show();
             $("#downloadPdfBn").prop('disabled',true);
             if (value == true) {
                var html =  $("#"+key).html();
                var pdf = Form.update({html:html, fileName : fileName}, function(data) {
                   // window.open("assets/files/"+fileName,'_blank');
                        $("#downloadPdfBn").prop('disabled',false);
                        $("#loadingStatus").html("Loading "+key);
                    });
             }

        });
        if($rootScope.editId) {
            var editId = $rootScope.editId;
            $rootScope.editId = null;
        }
        Form.save({data:$scope.formData, user : user.id, editId : editId});
        $state.go('forms');
        $("#loading").hide();
    }

});
 Qms.controller('ViewFormsCtrl', function($scope, $state,$filter, Scopes, Form, $rootScope, FlashMessage, $http, ngDialog,DTOptionsBuilder, DTColumnBuilder,DTColumnDefBuilder){
    $scope.vmf = {};
            $scope.vmf.dtInstance = {};   
            $scope.vmf.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('order', [3, 'desc'])
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500])
    $scope.vmr = {};
            $scope.vmr.dtInstance = {};   
            //$scope.vmr.dtColumnDefs = [DTColumnDefBuilder.newColumnDef(2).notSortable()];
            $scope.vmr.dtOptions = DTOptionsBuilder.newOptions()
                              .withOption('order', [[6, 'desc']])
                              .withOption('paging', true)
                              .withOption('searching', true)
                              .withOption('info', true)
                              .withOption('lengthMenu',[5,10,15,20,50,100,200,500])
    $scope.pages = 0;
    $scope.itemsPerPage = 10;
    $scope.count = 0;
    $scope.status = "";

    $scope.sortType = 'creator_name';
    $scope.reverse = false;

    $http.get("profile").success(function(response) {
        $scope.user = response;
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;   
        Form.show({user:$scope.user.id}, function(data) {
            $scope.loading = false;
            $scope.forms = data.formData;
            $scope.userStatus = 'all';
            if (data.userStatus == 'user') {
                $scope.userStatus = 5;
            }
        });
    });

    $scope.filterForms = function(status) {
       $scope.status = status;
    }

    $scope.edit = function(id) {
      var x=window.confirm("Do you want to Modify this?")
        if (x)
        var data = Form.show({id : id}, function(data) {
            angular.forEach(data.formData,function(name,index){
                //console.log(name.comLocation['schedule_of_goods'].length);
                if (index==data){
                    //alert(index);
                    angular.forEach(name,function(val,index){
                        if (index=='voiceEssentialChannel'){
                            //alert(angular.isArray(name));
                            if (!angular.isArray(name)){
                                var key = val;
                                delete val;
                                name.voiceEssentialChannel = [{'voiceEssentialChannel-0' : key}];
                            }
                        }
                        if (index=='voiceEssentialDID'){
                            //alert(angular.isArray(name));
                            if (!angular.isArray(val)){
                                var key = val;
                                delete val;
                                name.voiceEssentialDID = [{'voiceEssentialDID-0' : key}];
                            }
                        }
                    });
                } 
            })
            $rootScope.viewFormData = data.formData;
           //console.log($rootScope.viewFormData);
            $state.go('forms');
        });
    }

    $scope.downloadForms = function(id) {
        var forms = Form.downloadForms({id:id}, function(data) {
           if (data.success) {
               angular.forEach(data.files, function(name, index){
                    window.open("assets/files/"+name,'_blank');
               });

           }
        });
    }

    $scope.closedialog = function(){
        $scope.closeThisDialog();
    }

    $scope.view_referals = function(id) {
        var forms = Form.view_referals({id:id}, function(data) {
           if (data.success) {
               // console.log(data.data);
               $scope.data = data.data;
               $scope.template = "templates/forms/referals.html";
                ngDialog.open({
                     template: $scope.template,
                     plan: true,
                     controller : 'ViewFormsCtrl',
                     width: 900,
                     height: 500,
                     scope: $scope,
                     className: 'ngdialog-theme-default',
                     showClose: true,
                     closeByEscape: true,
                     closeByDocument: true
                     // preCloseCallback : function(value) {
                     //     var signData = $('img.imported').attr('src');
                     //     if ((!signData) && (!$rootScope.isDraftMode)) {
                     //         if (alert("You have not signed yet. Please sign to start filling out data. You can also select Draft Mode to be able to start.")) {
                     //             return true;
                     //         }
                     //         return false;
                     //         $timeout(angular.noop());
                     //     }
                     // }
                 });
           }
        });
    }

    $scope.convertToCSV = function (objArray){
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = '';

        for (var i = 0; i < array.length; i++) {
            var line = '';
            for (var index in array[i]) {
                if (line != '') line += ','

                line += array[i][index];
            }

            str += line + '\r\n';
        }

        return str;
    }

    $scope.to_csv = function() {
        $scope.csvButton = !$scope.csvButton;
        var forms = Form.to_csv({referals:$scope.referals}, function(response) {
            if(response.success){
                $scope.csvFile = response.filename;
                $('#referalCSV').click();
            }
        });

    }
    $scope.view_all = function() {
        $('body').append('<div class="loading"></div>');
        $scope.loading = true;         
        var forms = Form.view_all(function(data) {
            $scope.loading = false;         
            if (data.success) {
                $scope.loading = false; 
                $scope.referals = data.response;
                // console.log($scope.referals);
                $scope.template = "templates/forms/view-all.html";
                ngDialog.open({
                     template: $scope.template,
                     plan: true,
                     controller : 'ViewFormsCtrl',
                     width: 1080,
                     height: 500,
                     scope: $scope,
                     className: 'ngdialog-theme-default',
                     showClose: true,
                     closeByEscape: true,
                     closeByDocument: true
                     // preCloseCallback : function(value) {
                     //     var signData = $('img.imported').attr('src');
                     //     if ((!signData) && (!$rootScope.isDraftMode)) {
                     //         if (alert("You have not signed yet. Please sign to start filling out data. You can also select Draft Mode to be able to start.")) {
                     //             return true;
                     //         }
                     //         return false;
                     //         $timeout(angular.noop());
                     //     }
                     // }
                 });
            }
        });
    }

    $scope.delete = function(id) {
        $('<div></div>').appendTo('body')
        .html('<div><h6>Are you really sure to delete this item?</h6></div>')
        .dialog({
            modal: true,
            title: 'You want to delete this item?',
            zIndex: 10000,
            autoOpen: true,
            width: '315px',
            resizable: false,
            buttons: {
                Yes: function () {
                    var data = Form.delete({id : id}, function(data) {
                        if (data.success) {
                            FlashMessage.setMessage(data);
                            Form.show({user:$scope.user.id}, function(data) {
                                $scope.forms = data.formData;
                                $scope.userStatus = 'all';
                                if (data.userStatus == 'user') {
                                    $scope.userStatus = 5;
                                }
                            });
                        }
                    });
                    $(this).dialog("close");
                },
                No: function () {
                    $(this).dialog("close");
                }
            },
            close: function (event, ui) {
                $(this).remove();
            }
        });

    }

});

Qms.directive('numberFormatter', ['$filter', function ($filter) {
        var formatter = function (num) {
          num = num || 0;
          return '$' + $filter('number')(num);
        };

        var unformatter = function (str) {
          return parseFloat(str.replace(/[^0-9\.]/g, '')).toFixed(0);
        };

        return {
          restrict: 'A',
          require: 'ngModel',
          link: function (scope, element, attr, ngModel) {
            ngModel.$formatters.unshift(formatter);
            ngModel.$parsers.unshift(unformatter);

            element.bind('blur', function () {
              element.val(formatter(ngModel.$modelValue))
            });

            element.bind('focus', function () {
              element.val(ngModel.$modelValue);
            });
          }
        };
      }]);
Qms.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit('renderFinish');
                });
            }
        }
    }
});

Qms.directive('integer', function(){
    return {
        require: 'ngModel',
        link: function(scope, ele, attr, ctrl){
            ctrl.$parsers.unshift(function(viewValue){
                return parseInt(viewValue, 10);
            });
        }
    };
});


Qms.directive('rowTools', function() {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'templates/common/row_tools.html',
        link: function($scope, $element, attrs) {
            $scope.row_id = attrs['rowId'];
        }
    };
});

Qms.directive('rowToolsProp', function() {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'templates/common/row_tools_prop.html',
        link: function($scope, $element, attrs) {
            $scope.row_id = attrs['rowId'];
        }
    };
});

Qms.directive("flashMessage", function($timeout) {
    return {
        restrict: 'E',
        templateUrl: 'templates/common/msg.html?t=" + _version,',
        link: function($scope) {
           $scope.$watch(function() {
               $scope.flashMessage = $scope.$parent.flashMessage;
               if ($scope.flashMessage.header) {
                   $timeout(function() {
                      $scope.flashMessage.header= null;
                   },3000);
               }
           });
           $scope.remove = function() {
               $scope.flashMessage.header = null;
           }
        }
    }
});


Qms.directive("passwordMatch", function(FlashMessage, $timeout) {
    return {
        restrict: 'EA',
        link: function($scope, $element, $attrs) {

            $element.on("keyup", function() {
                if ($scope.user.password != $scope.user.cpassword) {
                    $element

                }
            })

        }
    }
});

// Qms.directive('currentYear',function(){
//     return function(){
//         var now = new Date();
//         console.log(now.getFullYear());
//         return now.getFullYear();
//     }
// });

Qms.filter('range',function(){
    return function(input, min, max) {
    min = parseInt(min);
    max = parseInt(max);
    for (var i=min; i<=max; i++)
      input.push(i);
    return input;
  };
});

Qms.filter('range_printer',function(){
    return function(input, min, max) {
    var min_r = parseFloat(min);
    var max_r = parseFloat(max);
    //console.log(min + '-' + max);
    var i = min_r;
    while (i<=max_r){
        var num = i.toFixed(1);
        input.push(num);
        i+=0.1;
    }
    return input;
  };
});

Qms.filter('capitalizefirstletter', function() {
    return function(input, all) {
        return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }) : '';
    }
});
Qms.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      var keys = Object.keys(props);
        
      items.forEach(function(item) {
        var itemMatches = false;

        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});

Qms.filter('removecurrent', function() {
    //console.log(1);
    //console.log($scope);
    return function(input, all) {
        return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }) : '';
    }
});


/* move to directives*/

Qms.directive('ngConfirmClick', [
    function(){
        return {
            link: function (scope, element, attr) {
                var msg = attr.ngConfirmClick || "Are you sure?";
                var clickAction = attr.confirmedClick;
                element.bind('change',function (event) {
                    if ( window.confirm(msg) ) {
                        scope.$eval(clickAction)
                        return false;
                    }
                });
            }
        };
    }
]);

Qms.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit('ngRepeatFinished');
                });
            }
        }
    }
});

Qms.directive('onlyDigits', function () {

    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined) return '';
                var transformedInput = inputValue.replace(/[^0-9]/g, '');
                if (transformedInput !== inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});

Qms.directive('stringToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseFloat(value, 10);
      });
    }
  };
});

Qms.filter('unsafe', function($sce) {

    return function(value) {

        //mod
        wordwise = true;
        max = 50;
        tail = "...";
        max = parseInt(50, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
        }

        value + (tail || ' Ã¯Â¿Â½');
        //mod

        return $sce.trustAsHtml(value);

    };

});


Qms.filter('cut', function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                    value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' Ã¯Â¿Â½');
        };
});


Qms.directive('realTimeCurrency', function ($filter, $locale) {
var decimalSep = $locale.NUMBER_FORMATS.DECIMAL_SEP;
    var toNumberRegex = new RegExp('[^0-9\\' + decimalSep + ']', 'g');
    var trailingZerosRegex = new RegExp('\\' + decimalSep + '0+$');
    /*var filterFunc = function (value) {
        return $filter('currency')(value);
    };*/
    var currencyFilter = $filter('currency');
    var formats = $locale.NUMBER_FORMATS;
    var filterFunc = function(value) {
        var value = currencyFilter(value);
        var sep = value.indexOf(formats.DECIMAL_SEP);
        if(value >= 0) {
            return value.substring(0, sep);
        }
        return value.substring(0, sep);
    };

    function getCaretPosition(input){
        if (!input) return 0;
        if (input.selectionStart !== undefined) {
            return input.selectionStart;
        } else if (document.selection) {
            // Curse you IE
            input.focus();
            var selection = document.selection.createRange();
            selection.moveStart('character', input.value ? -input.value.length : 0);
            return selection.text.length;
        }
        return 0;
    }

    function setCaretPosition(input, pos){
        if (!input) return 0;
        if (input.offsetWidth === 0 || input.offsetHeight === 0) {
            return; // Input's hidden
        }
        if (input.setSelectionRange) {
            input.focus();
            input.setSelectionRange(pos, pos);
        }
        else if (input.createTextRange) {
            // Curse you IE
            var range = input.createTextRange();
            range.collapse(true);
            range.moveEnd('character', pos);
            range.moveStart('character', pos);
            range.select();
        }
    }

    function toNumber(currencyStr) {
        return parseFloat(currencyStr.replace(toNumberRegex, ''), 10);
    }

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function postLink(scope, elem, attrs, modelCtrl) {
            modelCtrl.$formatters.push(filterFunc);
            modelCtrl.$parsers.push(function (newViewValue) {
                var oldModelValue = modelCtrl.$modelValue;
                var newModelValue = toNumber(newViewValue);
                modelCtrl.$viewValue = filterFunc(newModelValue);
                var pos = getCaretPosition(elem[0]);
                elem.val(modelCtrl.$viewValue);
                var newPos = pos + modelCtrl.$viewValue.length -
                                   newViewValue.length;
                if ((oldModelValue === undefined) || isNaN(oldModelValue)) {
                    newPos -= 3;
                }
                setCaretPosition(elem[0], newPos);
                return newModelValue;
            });
        }
    };
});

Qms.factory('Scopes', function ($rootScope) {
    var mem = {};

    return {
        store: function (key, value) {
            mem[key] = value;
        },
        get: function (key) {
            return mem[key];
        }
    };
});

//
//Qms.factory('NgRepeatSkip', function() {
//  return function(values) {
//    var list = {};
//
//    list.index = 0;
//
//    list.next = function() {
//      list.index += 1;
//    };
//
//     list.next2 = function() {
//      list.index += 2;
//    };
//
//     list.next3 = function() {
//      list.index += 3;
//    };
//
//     list.next4 = function() {
//      list.index += 4;
//    };
//
//    list.previous = function() {
//      list.index -= 1;
//    };
//
//    list.value = function() {
//      return values[list.index];
//    };
//  };
//});
//

Qms.run(function ($rootScope) {
    $rootScope.$on('scope.stored', function (event, data) {
        //console.log("scope.stored", data);
    });
});

Qms.run(function($rootScope, $templateCache) {
    $rootScope.$on('$routeChangeStart', function(event, next, current) {
        if (typeof(current) !== 'undefined'){
            //console.log($templateCache);
            //$templateCache.remove(current.templateUrl);
        }
    });
});

Qms.directive('multipleEmails', function () {
        var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+/=?^_`{|}~.-]+@[a-z0-9-]+(\.[a-z0-9-]+)*$/i;

        function validateAll(ctrl, validatorName, value) {
            var validity = ctrl.$isEmpty(value) || value.split(';').every(
                function (email) {
                    return EMAIL_REGEXP.test(email.trim());
                }
            );

            ctrl.$setValidity(validatorName, validity);
            return validity ? value : undefined;
        }

        return {
            restrict: 'A',
            require: 'ngModel',
            link: function postLink(scope, elem, attrs, modelCtrl) {
                function multipleEmailsValidator(value) {
                    return validateAll(modelCtrl, 'multipleEmails', value);
                };

                modelCtrl.$formatters.push(multipleEmailsValidator);
                modelCtrl.$parsers.push(multipleEmailsValidator);
            } 
        };
    });

Qms.directive('removeNgEmailValidation', function(){
    return {
        require : 'ngModel',
        link : function(scope, element, attrs, ngModel) {
            ngModel.$validators["email"] = function () {
                return true;
            };
        }
    }
});

Qms.directive('exportToCsv',function(){
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var el = element[0];
            element.bind('click', function(e){
                var table = $('#referalTable')[0];
                //console.log(attrs);
                var csvString = '';
                for(var i=0; i<table.rows.length;i++){
                    var rowData = table.rows[i].cells;
                    for(var j=0; j<rowData.length;j++){
                        csvString = csvString + rowData[j].innerHTML + ",";
                    }
                    csvString = csvString.substring(0,csvString.length - 1);
                    csvString = csvString + "\n";
                }
                csvString = csvString.substring(0, csvString.length - 1);
                var a = $('<a/>', {
                    style:'display:none',
                    href:'data:application/octet-stream;base64,'+btoa(csvString),
                    download: attrs.company + ' Referrals.csv'
                }).appendTo('body')
                a[0].click()
                a.remove();
            });
        }
    }
});

Qms.$inject = ['$scope', '$filter'];

Qms.directive("customSort", function() {
return {
    restrict: 'A',
    transclude: true,    
    scope: {
      order: '=',
      sort: '='
    },
    template : 
      ' <a ng-click="sort_by(order)" style="color: #555555;">'+
      '    <span ng-transclude></span>'+
      '    <i ng-class="selectedCls(order)"></i>'+
      '</a>',
    link: function(scope) {
                
    // change sorting order
    scope.sort_by = function(newSortingOrder) {       
        var sort = scope.sort;
        
        if (sort.sortingOrder == newSortingOrder){
            sort.reverse = !sort.reverse;
        }                    

        sort.sortingOrder = newSortingOrder;        
    };
    
   
    scope.selectedCls = function(column) {
        if(column == scope.sort.sortingOrder){
            return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
        }
        else{            
            return'icon-sort' 
        } 
    };      
  }// end link
}
});
Qms.directive("checkboxGroup", function() {
        return {
            restrict: "A",
            link: function(scope, elem, attrs) {
                // Determine initial checked boxes
                if (scope.array.indexOf(scope.item.id) !== -1) {
                    elem[0].checked = true;
                }

                // Update array on click
                elem.bind('click', function() {
                    var index = scope.array.indexOf(scope.item);
                    // Add if checked
                    if (elem[0].checked) {
                        if (index === -1) scope.array.push(scope.item);
                        // alert(scope.array);
                    }
                    // Remove if unchecked
                    else {
                        if (index !== -1) scope.array.splice(index, 1);
                    }
                    // Sort and update DOM display
                    scope.$apply(scope.array.sort(function(a, b) {
                        return a - b
                    }));
                });
            }
        }
    });