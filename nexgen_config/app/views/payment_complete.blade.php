<!DOCTYPE html>
<html>
	<body>
		<h1 style="text-align:center">Success</h1>
		<table style="display:block;margin:auto;width:500px">
			<tr>
				<td style="width:300px">Card Holder Name </td>
				<td style="text-align:right;width:200px"><$$ $paycorpResponse['holderName'] $$></td>
			</tr>
			<tr>
				<td style="width:300px">Credit Card Number </td>
				<td style="text-align:right;width:200px"><$$ $paycorpResponse['cardNumber'] $$></td>
			</tr>
			<tr>
				<td style="width:300px">Card Expiry Date </td>
				<td style="text-align:right;width:200px"><$$ $paycorpResponse['expiry'] $$></td>
			</tr>
			<tr>
				<td style="width:300px">Card Type </td>
				<td style="text-align:right;width:200px"><$$ $paycorpResponse['type'] $$></td>
			</tr>
			<tr>
				<td style="width:300px">Token </td>
				<td style="text-align:right;width:200px"><$$ $paycorpResponse['token'] $$></td>
			</tr>
		</table>
	</body>
</html>