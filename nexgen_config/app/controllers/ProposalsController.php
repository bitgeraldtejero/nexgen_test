<?php

class ProposalsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$proposals = Proposals::all();
		return Response::json($proposals);

	}
	// create pdf function here v
	public function createPdf()
    {
        set_time_limit(0);
        $forms = Request :: input("htmlData");
        $userDetails = Request :: input("userDetails");
        $account = Request :: input("account");
        $fromEmail = "noreply@nexgenconfigapp.xyz";
        $fromName = "Nexgen";
        $subject = 'Welcome to Nexgen';
        $toEmail = $userDetails["email"];
        $toName = $userDetails["name"];
        $accEmail = $account['email'];
        $accName = $account['name'];
        $mailData = [
            'user' => $toName,
            'userEmail' => $toEmail,
            'name' => $fromName,
            'email'=> $fromEmail
        ];

        $paths = array();

        if (Auth::user()) {
            $managerEmail = explode(";",Auth::user()->manageremail);
        }

        if (is_array($forms) || is_object($forms)) {
            foreach ($forms as $form) {
                $pdf = new mPDF('utf-8', 'A4');
                $pdf->WriteHTML($form['html']);
                $data = array("pdf" => $pdf->Output("assets/files/".$form['fileName']));
                $paths[] = "assets/files/".$form['fileName'];
            }
        }

        if (strpos($form['fileName'],'draft')!==false){
          $emailTemplate = 'emails.form.draftforms';
        } else {
          $emailTemplate = 'emails.form.forms';
        }
        
        Mail::send($emailTemplate, $mailData, function($message) use ($subject, $paths, $accEmail, $managerEmail)
        {
            $message->to($accEmail);
            if ($accEmail!='info@somelongcompany.com.au'){
              $message->cc('JamesHarb@nexgen.com.au')
                    ->cc('ElieAyoub@nexgen.com.au')
                    ->cc('developer@businesstelecom.com.au')
                    ->cc('QA@nexgen.com.au');
            }
            else {
              $message->cc('JamesHarb@nexgen.com.au')
                    ->cc('ElieAyoub@nexgen.com.au')
                    ->cc('developer@businesstelecom.com.au');
            }
            
            foreach ($managerEmail as $manager){
              $message->cc($manager);
            }

            $message->subject($subject);

            foreach ($paths as $path) {
                $message->attach($path);
            }
        });

        if (Mail::failures()) {
            Log::debug(Mail::failures());
            return Response::json(array("success" => "fail"));
        } else {
            return Response::json(array("success" => "success","managerEmail"=>$managerEmail,"message"=>$message));
        }
        // return Response::json(array("success"=>"Call to function Successful")); 
    }


	public function upload(){

		if ( !empty( $_FILES ) ) {

		    $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
		    // $uploadPath = storage_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $_FILES[ 'file' ][ 'name' ];
			$uploadPathT = 'assets' . "/" . 'files' . "/" . 'logo' . "/" . $_FILES[ 'file' ][ 'name' ];

		    move_uploaded_file( $tempPath, $uploadPathT );

		    $answer = array( 'answer' => 'File transfer completed', 'FilePath' => $uploadPathT );
		    $json = json_encode( $answer );

			//Log::error($exception);
		    echo $json;

		} else {

		    echo 'No files';
		}
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $files = Request :: input("files");
        $user = Request :: input("user");

        $proposalData = serialize($files);
        $editId = Request :: input("editId");

        if ($editId) {
            $fd = Proposals::find($editId);
        } else {
            $fd = new Proposals();
            $fd->user_id = $user;
        }

        try {
            // $fd->user_id = $user;
            $fd->proposal_data = $proposalData;
            $fd->save();

            return Response::json(array("success" => "success", "editId"=> $fd->id, "data"=> $fd->proposal_data));
        } catch (Exception $ex) {
            return Response ::json(array("success" => "false", "message" => $ex->getMessage(),"proposalData"=>$files));
        }
    }


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$proposals = Proposals::find($id);
		$proposalArray = array (
            'id' 				=>$proposals->id,
            'proposal_data'		=>unserialize($proposals->proposal_data)
		);
		return Response::json($proposalArray);

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{	
		$files = Request :: input("files");
        $companyName = Request :: input("companyName");
        $businessName = Request :: input("businessName");
        $contactNumber = Request :: input("contactNumber");
        $emailAddress = Request :: input("emailAddress");
        $schQty = Request :: input("schQty");
        $schItems = Request :: input("schItems");
        $schOption = Request :: input("schOption");
        $hTerm = Request :: input("hTerm");
        $bTerm = Request :: input("bTerm");
        $nationalCall = Request :: input("nationalCall");
        $localCall = Request :: input("localCall");
        $callNumbers = Request :: input("callNumbers");
        $directQuantity = Request :: input("directQuantity");
        $diD = Request :: input("diD");
        $selectedFeedbacks = Request :: input("selectedFeedbacks");
        $editId = Request :: input("editId");
		$fd =  Proposals::find($id);
		// $input = Input::all();

		try{
			$fd->files					= $files;
			$fd->companyName			= $companyName;
			$fd->businessName			= $businessName;
			$fd->contactNumber			= $contactNumber;
			$fd->emailAddress			= $emailAddress;
			$fd->display				= 'asasa';
			$fd->schQty					= serialize($schQty);
			$fd->schItems				= serialize($schItems);
			$fd->schOption				= serialize($schOption);
			$fd->hTerm					= $hTerm;
			$fd->bTerm					= $bTerm;
			$fd->nationalCall			= $nationalCall;
			$fd->localCall				= $localCall;
			$fd->callNumbers			= $callNumbers;
			$fd->directQuantity			= $directQuantity;
			$fd->diD					= $diD;
			$fd->selectedFeedbacks		= serialize($selectedFeedbacks);
            
                      
                                
			if($fd->save())
				return Response::json(array('success' => true,'id'=>$fd->id));
			else
				throw new \Exception("Could not save the Feedbacks");

		} catch(\Exception $e){
			return Response::json(array('success' => false,'message'=>$e->getMessage()));
		}

	}

	// {
	// 	// $input = Input::all();
	// 	// $fd     = SalesRep::find($id);

	//  //    try{

	// 	// 	$fd->name 			= $input['name'];
	// 	// 	$fd->phone 			= $input['phone'];
	// 	// 	$fd->email 			= $input['email'];

	// 	// 	$fd->updated_at 		= time();


	// 	// 	if($fd->save())
	// 	// 		return Response::json(array('success' => true,'id'=>$fd->id,"message"=>'Salesrep saved successfully'));
	// 	// 	else
	// 	// 		throw new \Exception("Couldnot save the salesrep");

	//  //   } catch (\Exception $e){
	// 	// 	return Response::json(array('success' => false,'message'=>$e->getMessage()));
	//  //   }

	// }


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		 if(Proposals::find($id)->delete())
			return Response::json(array('success' => true));
		 else
		 	return Response::json(array('success' => false));

	}


}
