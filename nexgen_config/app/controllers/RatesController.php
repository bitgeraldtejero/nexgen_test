<?php

class RatesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$rates = Rates::all();
		// print_r($rates);
		foreach ($rates as $key => $value) {
			$value['rates'] = $value['rates'];
		}
		return Response::json($rates);

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{		
		$input = Input::all();
		$fd = new Rates();
        try {
            $fd->name = $input['name'];
            $fd->term = $input['term'];
            $fd->rates = serialize($input['rate']);
            $fd->effective_at = $input['effective_at'];
            $fd->type = $input['type'];
            $fd->save();
			 return Response::json(array("success" => "success", "editId"=> $fd->id));
        } catch (Exception $ex) {
            return Response::json(array("success" => "false", "message" => $ex->getMessage()));
        }
    }


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	 public function show($id)
    {
	 	$data = Rates::find($id);
		// $data['rates'] = unserialize($data['rates']);
        $data['rates'] = $data['rates'];
        $data['type'] = $data['type'];
        $data['effective_at'] = strtotime($data['effective_at']) * 1000;
        return Response::json($data);	 	
    }


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();

		$c = Rates::find($id);

		$c->name 			= $input['name'];
		$c->term 			= $input['term'];
		$c->rates  			= serialize($input['rate']);
		$c->effective_at    = $input['effective_at'];
		$c->type    		= $input['type'];

		$c->updated_at 		= time();

		if($c->save())
			return Response::json(array('success' => true,'id'=>$c->id));
		else
			throw new \Exception("Couldnot save the category");
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	// public function deleteRates($id)
	// {	
	// 	$id = Request :: input("id");
	//  if ($id) {
 //           $data = Rates::find($id);
 //           $data->delete();
 //           return Response::json(array("success" => true, "message" => "item Deleted"));
 //       } else {
 //           return Response::json(array("success" => false, "message" => "Some Error!"));
 //       }
	// }
	public function destroy($id)
	{
	if(Rates::find($id)->delete())
		return Response::json(array('success' => true));
	else
	 	return Response::json(array('success' => false));
	}
	
	/**
	 * Get the category level.
	 *
	 * @param  int  $level
	 * @return Response
	 */


}
