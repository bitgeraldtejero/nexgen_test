<?php

class ScheduleGoodsController extends \BaseController {


	public function index()
	{
		$sched_goods = ScheduleGoods::all();
		return Response::json($sched_goods);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	public function upload(){

		if ( !empty( $_FILES ) ) {

		    $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
		    // $uploadPath = storage_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $_FILES[ 'file' ][ 'name' ];
			$uploadPathT = 'assets' . "/" . 'files' . "/" . 'uploads' . "/" . $_FILES[ 'file' ][ 'name' ];

		    move_uploaded_file( $tempPath, $uploadPathT );

		    $answer = array( 'answer' => 'File transfer completed', 'FilePath' => $uploadPathT );
		    $json = json_encode( $answer );

			//Log::error($exception);
		    echo $json;

		} else {

		    echo 'No files';
		}
	}

	public function uploads(){

		if ( !empty( $_FILES ) ) {

		    $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
		    // $uploadPath = storage_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $_FILES[ 'file' ][ 'name' ];
			$uploadPathT = 'assets' . "/" . 'files' . "/" . 'uploads' . "/" . $_FILES[ 'file' ][ 'name' ];

		    move_uploaded_file( $tempPath, $uploadPathT );

		    $answer = array( 'answer' => 'File transfer completed', 'FilePath' => $uploadPathT );
		    $json = json_encode( $answer );

			//Log::error($exception);
		    echo $json;

		} else {

		    echo 'No files';
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$input = Input::all();

		try{

			$c = new ScheduleGoods();

			$c->group				= $input['group'];
			$c->item				= $input['item'];
			$c->rrp					= $input['rrp'];
			$c->model 				= $input['model'];
			$c->category			= $input['category'];
			$c->features 			= $input['features'];
			$c->descriptions 		= $input['descriptions'];
			$c->hardware_solutions	= $input['hardware_solutions'];
			$c->img1 				= $input['img1'];
			$c->img2 				= $input['img2'];



			if($c->save())
				return Response::json(array('success' => true,'id'=>$c->id ,"message"=>'Product Saved successfully!'));
			else
				throw new \Exception("Could not save the Product");
		} catch(\Exception $e){
			return Response::json(array('success' => false,'message'=>$e->getMessage()));
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$scheduleGoods = ScheduleGoods::find($id);
		return Response::json($scheduleGoods);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$c =  ScheduleGoods::find($id);
		$input = Input::all();

		try{

			$c->group				= $input['group'];
			$c->item				= $input['item'];
			$c->rrp					= $input['rrp'];
			$c->model 				= $input['model'];
			$c->category			= $input['category'];
			$c->features 			= $input['features'];
			$c->descriptions 		= $input['descriptions'];
			$c->hardware_solutions	= $input['hardware_solutions'];
			$c->img1 				= $input['img1'];
			$c->img2 				= $input['img2'];
                                           
			if($c->save())
				return Response::json(array('success' => true,'id'=>$c->id));
			else
				throw new \Exception("Could not save the Product");

		} catch(\Exception $e){
			return Response::json(array('success' => false,'message'=>$e->getMessage()));
		}

	}

	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
	if(ScheduleGoods::find($id)->delete())
		return Response::json(array('success' => true));
	else
	 	return Response::json(array('success' => false));
	}


}
