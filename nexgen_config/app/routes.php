<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/','HomeController@index');
Route::post('/feedbacks/upload','FeedbacksController@upload');
Route::post('/schedule_goods/upload','ScheduleGoodsController@upload');
Route::post('/schedule_goods/uploads','ScheduleGoodsController@uploads');
Route::post('/proposal/upload','ProposalsController@upload');
Route::get('/category/{id}/{level}','CategoryController@show');
Route::resource('product', 'ProductController');
Route::resource('propose', 'ProposeController');
Route::resource('feedbacks', 'FeedbacksController');
Route::resource('schedule_goods','ScheduleGoodsController');
Route::resource('category', 'CategoryController');
Route::resource('rates', 'RatesController');
Route::resource('financetype', 'FinanceTypeController');
Route::resource('typet', 'TypetController');
Route::resource('company', 'CompanyController');
Route::resource('salesrep', 'SalesRepController');
Route::resource('telemarketer', 'TelemarketerController');
Route::resource('winback', 'WinBackController');
Route::resource('leadsource', 'LeadSourceController');
Route::resource('user', 'UserController');
Route::resource('config', 'ConfigController');
Route::resource('sellprice', 'SellPriceController');
Route::resource('rentalprice', 'RentalPriceController');
Route::resource('flexirent', 'FlexiRentController');
Route::resource('proposal', 'ProposalsController');
Route::get('report{id}', array('uses' => 'HomeController@showReport'));
Route::get('login', array('uses' => 'HomeController@showLogin'));
Route::post('login', array('uses' => 'HomeController@doLogin'));
Route::get('logout', array('uses' => 'HomeController@doLogout'));
Route::get('profile', array('uses' => 'HomeController@getCurrentUser'));
Route::post('/form/create-pdf', array('uses'=> 'FormController@createPdf'));
Route::post('/proposals/create-pdf', array('uses'=> 'ProposalsController@createPdf'));
Route::post('/form/save-data', array('uses'=> 'FormController@saveData'));
Route::post('/form/show-data', array('uses'=> 'FormController@showForms'));
Route::post('/form/delete-data', array('uses'=> 'FormController@deleteForm'));
Route::post('/form/download-forms', array('uses'=> 'FormController@downloadForms'));
Route::post('/form/get-schedule-goods', array('uses'=> 'FormController@getScheduleGoods'));
Route::post('/form/get-rates', array('uses'=> 'FormController@getRates'));
Route::post('/form/view-referals',array('uses'=>'FormController@viewReferals'));
Route::post('/form/view-all',array('uses'=>'FormController@viewAll'));
Route::post('/form/to-csv',array('uses'=>'FormController@toCsv'));
Route::post('/form/send-referrals',array('uses'=>'FormController@sendReferrals'));
Route::post('frontier/qualify',array('uses'=>'SQController@getServiceQualification'));
Route::post('frontier/get-addresses',array('uses'=>'SQController@getLocationIds'));
Route::post('frontier/get-advance',array('uses'=>'SQController@getLocationId'));
Route::post('frontier/get-fnn',array('uses'=>'SQController@getServiceQualification'));
Route::post('frontier/sqCsv',array('uses'=>'SQController@to_Csv'));