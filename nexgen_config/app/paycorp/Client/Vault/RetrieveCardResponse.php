<?php

class RetrieveCardResponse {
    
    private $creditCard;
    private $responseCode;
    private $responseText;
    private $clientRef;

    public function __construct() {
        
    }

    public function getResponseCode() {
        return $this->responseCode;
    }

    public function setResponseCode($responseCode) {
        $this->responseCode = $responseCode;
    }

    public function getResponseText() {
        return $this->responseText;
    }

    public function setResponseText($responseText) {
        $this->responseText = $responseText;
    }
    
    public function getClientRef(){
        return $this->clientRef;
    }
    
    public function setClientRef($clientRef){
        $this->clientRef = $clientRef;
    }
    
    public function getCreditCard(){
        return $this->creditCard;
    }
    
    public function setCreditCard($creditCard){
        $this->creditCard = $creditCard;
    }

}
